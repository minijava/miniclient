package org.miniclient.json;


// Base class of all MiniClient exceptions.
public class MiniClientJsonException extends Exception 
{
    private static final long serialVersionUID = 1L;


    public MiniClientJsonException()
    {
        super();
    }
    public MiniClientJsonException(String message)
    {
        super(message);
    }
    public MiniClientJsonException(Throwable cause)
    {
        super(cause);
    }
    public MiniClientJsonException(String message, Throwable cause)
    {
        super(message, cause);
    }
    public MiniClientJsonException(String message, Throwable cause,
            boolean enableSuppression, boolean writableStackTrace)
    {
        super(message, cause, enableSuppression, writableStackTrace);
    }


}
