package org.miniclient.json;



public interface LiteJsonStructureBuilder
{
    // Converts the object to a nested structure of Map/List, object/JsonCompatible, and JsonSerializable + primitive types.
    // Uses the default depth of the object (not necessarily 0).
    // Note that the return value (structure) is either Map<String,Object> or List<Object>.
    Object buildJsonStructure(Object jsonObj) throws MiniClientJsonException;           

    // Traverses down to the depth level (in terms of Object, Map, List).
    // 1 means this object only. No map, list, other object traversal (even it it's its own fields).
    // 0 means no introspection.
    // depth is always additive during traversal.
    // that is, if depth is 3, then object -> object -> object -> primitive only. 
    Object buildJsonStructure(Object jsonObj, int depth) throws MiniClientJsonException;
    // Map<String,Object> toJsonObject();   // Reserved for later. (e.g., JsonObject toJsonObject())

}
