package org.miniclient.json.builder;

import java.beans.IntrospectionException;
import java.lang.reflect.Array;
import java.lang.reflect.InvocationTargetException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.json.LiteJsonStructureBuilder;
import org.miniclient.json.MiniClientJsonException;
import org.miniclient.json.common.BeanIntrospectionUtil;
import org.miniclient.json.common.JsonNull;


// Note that this class is primarily used to convert an object to the map/list tree structure.
//    (The default depth is DEF_DRILL_DOWN_DEPTH.)
// Then the tree structure is fed into MiniClientJsonBuilder (which has a default depth of MAX_DRILL_DOWN_DEPTH)
//    to get the JSON string.
public final class MiniClientJsonStructureBuilder implements LiteJsonStructureBuilder
{
    private static final Logger log = Logger.getLogger(MiniClientJsonStructureBuilder.class.getName());

    // Default value.
    // private static final int DEF_DRILL_DOWN_DEPTH = 2;
    // Max value: equivalent to -1.
    private static final int MAX_DRILL_DOWN_DEPTH = (int) Byte.MAX_VALUE;  // Arbitrary.
    private static final int DEF_DRILL_DOWN_DEPTH = 3;  // For objects. Arbitrary. It should be normally >= 1.
    
    // Note:
    // It's important not to keep any "state" as class variables for this class
    //   so that a single instance can be used for multiple/concurrent build operations.
    // (Often, the recursive implementation may involve multiple objects (each as a node in an object tree),
    //   which may use the same/single instance of this builder class.)
    // ...


    public MiniClientJsonStructureBuilder()
    {
    }


    @Override
    public Object buildJsonStructure(Object obj) throws MiniClientJsonException
    {
        return buildJsonStructure(obj, DEF_DRILL_DOWN_DEPTH);
    }

    @Override
    public Object buildJsonStructure(Object jsonObj, int depth) throws MiniClientJsonException
    {
        if(depth < 0 || depth > MAX_DRILL_DOWN_DEPTH) {
            depth = MAX_DRILL_DOWN_DEPTH;
        }
        return _buildJsonStruct(jsonObj, depth);
    }


    // TBD:
    // The problem with this algo is we have no way to consistently represent null node.
    // For map value and list element, we can just use Java null.
    // But, in general, it may not be possible.....  ????    Is this true????
    // Seems to be working so far (based on the limited unit test cases...)
    @SuppressWarnings("unchecked")
    private Object _buildJsonStruct(Object obj, int depth) throws MiniClientJsonException
    {
//        if(depth < 0) {
//            return null;
//        }
        Object jsonStruct = null;
        if(obj == null || obj instanceof JsonNull) {    // ????
            // return null;
        } else {
            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> depth = " + depth);
            // if(depth == 0) {
            if(depth <= 0) {
                if(obj instanceof Boolean) {
                    jsonStruct = (Boolean) obj;
                } else if(obj instanceof Character) {
                    jsonStruct = (Character) obj;
                } else if(obj instanceof Number) {
                    jsonStruct = (Number) obj;
                } else if(obj instanceof String) {
                    // Note that this string is not a "json string" (e.g., forward slash escaped, etc.)
                    // e.g., if the string is "...\\/...", we will read it as "...\\/..." not as ".../...".
                    jsonStruct = (String) obj;
                } else {
                    // ????
                    jsonStruct = obj.toString();
                }
            } else {
                if(obj instanceof Map<?,?>) {
                    Map<String,Object> jsonMap = new LinkedHashMap<String,Object>();
    
                    Map<String,Object> map = null;
                    try {
                        map = (Map<String,Object>) ((Map<?,?>) obj);
                    } catch(Exception e) {
                        log.log(Level.WARNING, "Invalid map type.", e);
                        // What to do???
                        // Use map.toString???
                    }
                    if(map != null && !map.isEmpty()) {
                        for(String f : map.keySet()) {
                            Object val = map.get(f);
                            Object jsonVal = _buildJsonStruct(val, depth - 1);
                            if(jsonVal != null) {
                                jsonMap.put(f, jsonVal);
                            } else {
                                // ???
                                jsonMap.put(f, null);
                            }
                        }
                    }
                    
                    jsonStruct = jsonMap;
                } else if(obj instanceof List<?>) {
                    List<Object> jsonList = new ArrayList<Object>();
    
                    List<Object> list = null;
                    try {
                        list = (List<Object>) ((List<?>) obj);
                    } catch(Exception e) {
                        log.log(Level.WARNING, "Invalid list type.", e);
                        // What to do???
                        // Use list.toString???
                    }
                    if(list != null && !list.isEmpty()) {
                        for(Object v : list) {
                            Object jsonVal = _buildJsonStruct(v, depth - 1);
                            if(jsonVal != null) {
                                jsonList.add(jsonVal);
                            } else {
                                // ???
                                jsonList.add(null);
                            }
                        }
                    }
                    
                    jsonStruct = jsonList;
                } else if(obj.getClass().isArray()) {          // ???
                    List<Object> jsonList = new ArrayList<Object>();
                    Object array = obj;
                    if(array!= null && Array.getLength(array) > 0) {
                        int arrLen = Array.getLength(array);
                        // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> arrLen = " + arrLen);
                        for(int i=0; i<arrLen; i++) {
                            Object o = Array.get(array, i);
                            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> o = " + o + "; " + o.getClass());
                            Object jsonVal = _buildJsonStruct(o, depth - 1);
                            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> jsonVal = " + jsonVal + "; " + o.getClass());
                            if(jsonVal != null) {
                                jsonList.add(jsonVal);
                            } else {
                                // ???
                                jsonList.add(null);
                            }
                        }
                    }
    
                    jsonStruct = jsonList;
                } else if(obj instanceof Collection<?>) {
                    List<Object> jsonList = new ArrayList<Object>();
                    // jsonList.addAll((Collection<Object>) ((Collection<?>) obj));

                    Iterator<Object> it = ((Collection<Object>) ((Collection<?>) obj)).iterator();
                    while(it.hasNext()) {
                        Object o = it.next();
                        Object jsonVal = _buildJsonStruct(o, depth - 1);
                        if(jsonVal != null) {
                            jsonList.add(jsonVal);
                        } else {
                            // ???
                            jsonList.add(null);
                        }
                    }
                    
                    jsonStruct = jsonList;
                } else {
                    // ???
                    // This can potentially cause infinite recursion.
                    // because maybe JsonCompatible object implements toJsonStructure() using JsonBuilder.buidJsonStructure()
                    // which calls the object's toJsonStructure(), which calls JsonBuilder.buidJsonStructure(), etc.
                    // ....
                    // if(obj instanceof JsonCompatible) {
                    //     jsonStruct = ((JsonCompatible) obj).toJsonStructure(depth);
                    // } else {
                        // primitive types... ???
                        if(obj instanceof Boolean) {
                            jsonStruct = (Boolean) obj;
                        } else if(obj instanceof Character) {
                            jsonStruct = (Character) obj;
                        } else if(obj instanceof Number) {
                            jsonStruct = (Number) obj;
                        } else if(obj instanceof String) {
                            jsonStruct = (String) obj;
                        } else {

                            // Use inspection....
                            // TBD:
                            // BuilderPolicy ???
                            // ...
                            
                            Map<String, Object> mapEquivalent = null;
                            try {
                                // mapEquivalent = BeanIntrospectionUtil.introspect(obj, depth);   // depth? or depth - 1 ?
                                // Because we are just converting a bean to a map,
                                // the depth param is not used. (or, depth == 1).
                                mapEquivalent = BeanIntrospectionUtil.introspect(obj);
                            // } catch (IllegalAccessException
                            //         | IllegalArgumentException
                            //         | InvocationTargetException
                            //         | IntrospectionException e) {
                            } catch (Exception e) {
                                // Ignore.
                                if(log.isLoggable(Level.INFO)) log.log(Level.INFO, "Faild to introspect a bean.", e);
                            }
                            if(mapEquivalent != null) {
                                jsonStruct = _buildJsonStruct(mapEquivalent, depth);   // Note: We do not change the depth.
                            } else {
                                
                                // ????
                                // jsonStruct = null; ???
                                jsonStruct = obj.toString();
                                // ...
                            }
                            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> jsonStruct = " + jsonStruct);
                                

                        }
                    // }
                }
            }
        }

        return jsonStruct;
    }
   
    
}
