package org.miniclient.json.builder;

import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Date;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.json.LiteJsonBuilder;
import org.miniclient.json.LiteJsonStructureBuilder;
import org.miniclient.json.MiniClientJsonException;
import org.miniclient.json.common.CharacterUtil;
import org.miniclient.json.common.JsonNull;
import org.miniclient.json.common.Literals;
import org.miniclient.json.common.Symbols;
import org.miniclient.json.common.UnicodeUtil;


public final class MiniClientJsonBuilder implements LiteJsonBuilder
{
    private static final Logger log = Logger.getLogger(MiniClientJsonBuilder.class.getName());

    // Default value.
    // private static final int DEF_DRILL_DOWN_DEPTH = 2;
    // Max value: equivalent to -1.
    private static final int MAX_DRILL_DOWN_DEPTH = (int) Byte.MAX_VALUE;  // Arbitrary.

    // temporary
    // private static final int maxDepth = MAX_DRILL_DOWN_DEPTH;
    private static final String WS = " ";  // either to use space or not after "," and ":".
    // temporary

    
    // Note:
    // It's important not to keep any "state" as class variables for this class
    //   so that a single instance can be used for multiple/concurrent build operations.
    // (Often, the recursive implementation may involve multiple objects (each as a node in an object tree),
    //   which may use the same/single instance of this builder class.)
    // ...


    public MiniClientJsonBuilder()
    {
    }

    @Override
    public String build(Object jsonObj) throws MiniClientJsonException
    {
        // Which is better?
        
        // [1] Using StringBuilder.
        // StringBuilder sb = new StringBuilder();
        // return _build(sb, jsonObj);
    
        // Or, [2] Using StringWriter.
        String jsonStr = null;
        StringWriter writer = new StringWriter();
        try {
            // _build(writer, jsonObj, depth, useBeanIntrospection);
            // writer.flush();   // ???
            build(writer, jsonObj);
            jsonStr = writer.toString();
            // log.warning(">>>>>>>>>>>>>>>>>>>> jsonStr = " + jsonStr);
            // String jsonStr2 = writer.getBuffer().toString();
            // log.warning(">>>>>>>>>>>>>>>>>>>> jsonStr2 = " + jsonStr2);

            if(log.isLoggable(Level.FINE)) log.fine("jsonStr = " + jsonStr);
        } catch (IOException e) {
            log.log(Level.WARNING, "Failed to write jsonObj as JSON.", e);
        }
        return jsonStr;
    }

    @Override
    public void build(Writer writer, Object obj) throws MiniClientJsonException, IOException 
    {
        if( (obj == null) || (obj instanceof Boolean) || (obj instanceof Character) || (obj instanceof Number) || (obj instanceof String)) {
            throw new MiniClientJsonException("Top level element should be an object or an array/list.");
        }
        Object jsonObj = null;

        if( obj instanceof Map<?,?> || obj instanceof List<?> || obj.getClass().isArray() || obj instanceof Collection<?> ) {
            jsonObj = obj;
        } else {
            // ???
            // throw new MiniClientJsonException("Top level element should be an object or an array/list.");
            // ...
            // We support a generic "object" by converting it to a nested map/list structure (most likely, a single level map). 
            // Note that this automatic conversion is for convenience only, and
            //     we only support an object at the top-level object (which recurses down its structure, however), not [object] or {k:object}.
            // If the user wants such a support for more complex collections,
            //    then he/she needs to call MiniClientJsonStructureBuilder.buildJsonStructure(obj/[]/{}, depth) explicitly
            //    before calling MiniClientJsonBuilder.buildJson().
            LiteJsonStructureBuilder structureBuilder = new MiniClientJsonStructureBuilder();
            jsonObj = structureBuilder.buildJsonStructure(obj);
        }

        _build(writer, jsonObj, MAX_DRILL_DOWN_DEPTH);
        writer.flush();   // ???

//        String jsonStr = writer.toString();
//        if(log.isLoggable(Level.FINE)) log.fine("jsonStr = " + jsonStr);

    }


    @SuppressWarnings("unchecked")
    private void _build(Writer writer, Object obj, int depth) throws IOException
    {
        if(depth < 0) {
            return;
        }

        if(obj == null || obj instanceof JsonNull) {
            writer.append(Literals.NULL);
        } else {
            // if(depth == 0) {
            if(depth <= 0) {
                // TBD:
                // This section of code is repeated when depth==0 and and when depth>0,
                //     almost identically... (but not quite exactly the same)
                // Need to be refactored.
                // ....
                String primStr = null;
                if(obj instanceof Boolean) {
                    if(((Boolean) obj).equals(Boolean.TRUE)) {
                        primStr = Literals.TRUE;
                    } else {
                        primStr = Literals.FALSE;
                    }                        
                    writer.append(primStr);
                } else if (obj instanceof Character) {
                    // ???
                    Character strChar = (Character) obj;
                    writer.append("\"").append(strChar).append("\"");
                } else if (obj instanceof Number) {
//                        double d = ((Number) obj).doubleValue();
//                        jsonStr = Double.valueOf(d);
                    primStr = ((Number) obj).toString();
                    writer.append(primStr);
                } else if(obj instanceof String) {
                    // ????
                    // Is there a better/faster way to do this?

                    // ???
                    primStr = (String) obj;
                    // writer.append("\"").append(primStr).append("\"");

                    // ???
                    writer.append("\"");
                    _appendEscapedString(writer, primStr);
                    writer.append("\"");
                    // ???
                    
                } else {
                    
                    // TBD:
                    // java.util.Date ???
                    // and other JDK built-in class support???
                    // ..

                    if(obj instanceof Date) {
                        // TBD:
                        // Create a struct ???
                        primStr = ((Date) obj).toString();
                        // ...
                    } else if(obj instanceof String) {
                        primStr = (String) obj;
                    } else {
                        
                        // TBD:
                        // POJO/Bean support???
                        // ...
                        
                        // ????
                        primStr = obj.toString();
                    }
                    // writer.append("\"").append(primStr).append("\"");
                    writer.append("\"");
                    _appendEscapedString(writer, primStr);
                    writer.append("\"");
                }
            } else {
                if(obj instanceof Map<?,?>) {
                    Map<String,Object> map = null;
                    try {
                        map = (Map<String,Object>) ((Map<?,?>) obj);
                    } catch(Exception e) {
                        log.log(Level.INFO, "Invalid map type.", e);
                    }
                    writer.append("{");
                    if(map != null && !map.isEmpty()) {
                        Iterator<String> it = map.keySet().iterator();
                        while(it.hasNext()) {
                            String key = it.next();
                            Object val = map.get(key);
                            writer.append("\"").append(key).append("\":").append(WS);
                            _build(writer, val, depth - 1);
                            if(it.hasNext()) {
                                writer.append(",").append(WS);                                
                            }
                        }
                    }
                    writer.append("}");
                } else if(obj instanceof List<?>) {
                    List<Object> list = null;
                    try {
                        list = (List<Object>) ((List<?>) obj);
                    } catch(Exception e) {
                        log.log(Level.INFO, "Invalid list type.", e);
                    }
                    writer.append("[");
                    if(list != null && !list.isEmpty()) {
                        Iterator<Object> it = list.iterator();
                        while(it.hasNext()) {
                            Object o = it.next();
                            _build(writer, o, depth - 1);
                            if(it.hasNext()) {
                                writer.append(",").append(WS);
                            }
                        }
                    }
                    writer.append("]");
                } else if(obj.getClass().isArray()) {          // ???
                    Object array = obj;
                    writer.append("[");
                    if(array!= null && Array.getLength(array) > 0) {
                        int arrLen = Array.getLength(array);
                        // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> arrLen = " + arrLen);
                        for(int i=0; i<arrLen; i++) {
                            Object o = Array.get(array, i);
                            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> o = " + o + "; " + o.getClass());
                            _build(writer, o, depth - 1);
                            // log.warning(">>>>>>>>>>>>>>>>>>>>>>>>>>>>> jsonVal = " + jsonVal + "; " + o.getClass());
                            if(i < arrLen - 1) {
                                writer.append(",").append(WS);
                            }
                        }
                    }
                    writer.append("]");

//                    // ???
//                    // This adds weird repeated brackets ([[[[[[[ ...]]]]]]]]).
//                    _build(writer, Arrays.asList(array), depth);
                } else if(obj instanceof Collection<?>) {       // ???????
                    Collection<Object> coll = null;
                    try {
                        coll = (Collection<Object>) ((Collection<?>) obj);
                    } catch(Exception e) {
                        log.log(Level.INFO, "Invalid collection type.", e);
                    }
                    writer.append("[");
                    if(coll != null && !coll.isEmpty()) {
                        Iterator<Object> it = coll.iterator();
                        while(it.hasNext()) {
                            Object o = it.next();
                            _build(writer, o, depth - 1);
                            if(it.hasNext()) {
                                writer.append(",");
                                writer.append(WS);
                            }
                        }
                    }
                    writer.append("]");
                } else {
                    // primitive types... ???
                    // ...
                    String primStr = null;
                    if(obj instanceof Boolean) {
                        if(((Boolean) obj).equals(Boolean.TRUE)) {
                            primStr = Literals.TRUE;
                        } else {
                            primStr = Literals.FALSE;
                        }                        
                        writer.append(primStr);
                    } else if (obj instanceof Character) {
                        // ???
                        Character strChar = (Character) obj;
                        writer.append("\"").append(strChar).append("\"");
                    } else if (obj instanceof Number) {
//                            double d = ((Number) obj).doubleValue();
//                            jsonStr = Double.valueOf(d);
                        primStr = ((Number) obj).toString();
                        writer.append(primStr);
                    } else if (obj instanceof String) {
                        // ????
                        // Is there a better/faster way to do this?

                        // ???
                        primStr = (String) obj;
                        // writer.append("\"").append(primStr).append("\"");

                        // ???
                        writer.append("\"");
                        _appendEscapedString(writer, primStr);
                        writer.append("\"");
                        // ???
                        
                    } else {
                        
                        // TBD:
                        // java.util.Date ???
                        // and other JDK built-in class support???
                        // ..

                        if(obj instanceof Date) {
                            // TBD:
                            // Create a struct ???
                            primStr = ((Date) obj).toString();
                            // ...
                        } else if(obj instanceof String) {
                            primStr = (String) obj;
                        } else {
                                
                            // ????
                            primStr = obj.toString();

                        }
                        writer.append("\"");
                        _appendEscapedString(writer, primStr);
                        writer.append("\"");
                    }
                }
            }
        }
    }

    
    // private static final int escapeForwardSlash = -1; 
    private void _appendEscapedString(Writer writer, String primStr) throws IOException
    {
        if(primStr != null && !primStr.isEmpty()) {
            char[] primChars = primStr.toCharArray();
            // char prevEc = 0;
            for(char ec : primChars) {
                if(Symbols.isEscapedChar(ec)) {
                    // if(prevEc == '<' && ec == '/') {
                    //     // if(escapeForwardSlash >= 0) {
                    //     //     writer.append("\\/");
                    //     // } else {
                    //         writer.append("/");
                    //     // }
                    // } else {
                        // String str = Symbols.getEscapedCharString(ec, escapeForwardSlash > 0 ? true : false);
                        String str = Symbols.getEscapedCharString(ec, false);
                        if(str != null) {
                            writer.append(str);
                        } else {
                            // ???
                            writer.append(ec);
                        }
                    // }
                } else if(CharacterUtil.isISOControl(ec)) {
                    char[] uc = UnicodeUtil.getUnicodeHexCodeFromChar(ec);
                    writer.write(uc);
                } else { 
                    writer.append(ec);
                }
                // prevEc = ec;
            }
        }

    }
    
    
}
