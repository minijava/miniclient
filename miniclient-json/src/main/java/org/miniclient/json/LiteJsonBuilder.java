package org.miniclient.json;

import java.io.IOException;
import java.io.Writer;


public interface LiteJsonBuilder
{
    String build(Object jsonObj) throws MiniClientJsonException;
    void build(Writer writer, Object jsonObj) throws MiniClientJsonException, IOException;

}
