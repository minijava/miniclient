package org.miniclient.json;

import java.io.IOException;
import java.io.Reader;


public interface LiteJsonParser
{
    Object parse(String jsonStr) throws MiniClientJsonException;
    Object parse(Reader reader) throws MiniClientJsonException, IOException;
}
