package org.miniclient.json;

import org.miniclient.json.common.JsonToken;


public interface LiteJsonTokenizer
{
    /**
     * @return true if there is more tokens in the stream.
     * @throws MiniClientJsonException
     */
    boolean hasMore() throws MiniClientJsonException;
    
    /**
     * @return the next token.
     * @throws MiniClientJsonException
     */
    JsonToken next() throws MiniClientJsonException;
    
    /**
     * @return the next token, without removing it from the stream.
     * @throws MiniClientJsonException
     */
    JsonToken peek() throws MiniClientJsonException;

}
