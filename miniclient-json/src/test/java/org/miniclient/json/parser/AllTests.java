package org.miniclient.json.parser;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MiniClientJsonParserTest.class, MiniClientJsonTokenizerTest.class })
public class AllTests
{

}
