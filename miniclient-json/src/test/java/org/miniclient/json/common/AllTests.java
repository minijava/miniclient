package org.miniclient.json.common;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ CharQueueTest.class, UnicodeUtilTest.class })
public class AllTests
{

}
