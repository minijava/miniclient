package org.miniclient.json;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ org.miniclient.json.common.AllTests.class, org.miniclient.json.parser.AllTests.class, org.miniclient.json.builder.AllTests.class })
public class AllTests
{

}
