package org.miniclient.json.builder;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ MiniClientJsonBuilderTest.class,
        MiniClientJsonStructureBuilderTest.class })
public class AllTests
{

}
