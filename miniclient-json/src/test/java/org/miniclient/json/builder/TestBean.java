package org.miniclient.json.builder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class TestBean
{
    private int attrA = 0;
    private String attrB = null;
    private Map<String,Object> attrC = null;
    private List<Object> attrD = null;
    private TestBean attrE = null;
    private char[] attrF = null;

    
    public TestBean(int attrA, String attrB)
    {
        super();
        this.attrA = attrA;
        this.attrB = attrB;
        
        // test data

        attrC = new HashMap<String,Object>();
        attrC.put("key 1", "value 1");
        attrC.put("key 2", 33.33);
        List<String> list1 = new ArrayList<String>();
        list1.add("elem a");
        list1.add("elem b");
        attrC.put("key 3", list1);

        attrD = new ArrayList<Object>();
        attrD.add('a');
        attrD.add(55.55);
        attrD.add("element K");
        attrD.add(new Date());
        
        // ???
        // stack overflow...
        // attrE = new TestBean(11, "eleven");
        
        attrF = new char[]{'x', 'y', 'z'};
        // ...
    }

    public int getAttrA()
    {
        return attrA;
    }
    public void setAttrA(int attrA)
    {
        this.attrA = attrA;
    }
    public String getAttrB()
    {
        return attrB;
    }
    public void setAttrB(String attrB)
    {
        this.attrB = attrB;
    }
    public Map<String, Object> getAttrC()
    {
        return attrC;
    }
    public void setAttrC(Map<String, Object> attrC)
    {
        this.attrC = attrC;
    }
    public List<Object> getAttrD()
    {
        return attrD;
    }
    public void setAttrD(List<Object> attrD)
    {
        this.attrD = attrD;
    }
    public TestBean getAttrE()
    {
        return attrE;
    }
    public void setAttrE(TestBean attrE)
    {
        this.attrE = attrE;
    }

    public char[] getAttrF()
    {
        return attrF;
    }
    public void setAttrF(char[] attrF)
    {
        this.attrF = attrF;
    }

    @Override
    public String toString()
    {
        return "TestBean [attrA=" + attrA + ", attrB=" + attrB + ", attrC="
                + attrC + ", attrD=" + attrD + ", attrE=" + attrE + ", attrF="
                + Arrays.toString(attrF) + "]";
    }
  

}
