package org.miniclient.ext.twitter.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.core.ListResponseType;
import org.miniclient.ext.twitter.TwitterApiServiceClient;
import org.miniclient.ext.twitter.maker.TwitterApiServiceClientMaker;
import org.miniclient.impl.AbstractApiServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;



// temporary
// Base class for all Twitter API resources.
public class BaseTwitterApiServiceClient extends AbstractApiServiceClient implements TwitterApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public BaseTwitterApiServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.LIST);
        // ...

    }


    // Factory methods

    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return TwitterApiServiceClientMaker.getInstance();
    }

    
}
