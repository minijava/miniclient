package org.miniclient.ext.twitter.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ext.twitter.TwitterApiServiceClient;
import org.miniclient.ext.twitter.factory.TwitterApiServiceClientFactory;
import org.miniclient.ext.twitter.impl.BaseTwitterApiServiceClient;
import org.miniclient.ext.twitter.maker.TwitterApiServiceClientMaker;
import org.miniclient.factory.impl.AbstractApiServiceClientFactory;
import org.miniclient.maker.ApiServiceClientMaker;


public class BaseTwitterApiServiceClientFactory extends AbstractApiServiceClientFactory implements TwitterApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class TwitterApiServiceClientFactoryHolder
    {
        private static final BaseTwitterApiServiceClientFactory INSTANCE = new BaseTwitterApiServiceClientFactory();
    }

    // Singleton method
    public static BaseTwitterApiServiceClientFactory getInstance()
    {
        return TwitterApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiServiceClient(resourceBaseUrl);
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return TwitterApiServiceClientMaker.getInstance();
    }


    @Override
    public TwitterApiServiceClient createTwitterApiServiceClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiServiceClient(resourceBaseUrl);
    }

    
}
