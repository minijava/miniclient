package org.miniclient.ext.twitter.maker;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ext.twitter.impl.BaseTwitterApiServiceClient;
import org.miniclient.maker.ApiUserClientMaker;


// Abstract factory.
public class TwitterApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(TwitterApiUserClientMaker.class.getName());


    protected TwitterApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class TwitterApiUserClientMakerHolder
    {
        private static final TwitterApiUserClientMaker INSTANCE = new TwitterApiUserClientMaker();
    }

    // Singleton method
    public static TwitterApiUserClientMaker getInstance()
    {
        return TwitterApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiServiceClient(resourceBaseUrl);
    }

    
}
