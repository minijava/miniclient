package org.miniclient.ext.twitter.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.twitter.TwitterApiUserClient;
import org.miniclient.ext.twitter.maker.TwitterApiUserClientMaker;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;


// Base class for all Twitter API resources.
public class BaseTwitterApiUserClient extends AbstractApiUserClient implements TwitterApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseTwitterApiUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public BaseTwitterApiUserClient(String resourceBaseUrl,
            UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
    }
    
    public BaseTwitterApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseTwitterApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }


    // Factory methods

    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return TwitterApiUserClientMaker.getInstance();
    }

}
