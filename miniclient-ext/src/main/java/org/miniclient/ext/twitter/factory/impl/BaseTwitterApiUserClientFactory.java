package org.miniclient.ext.twitter.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ApiUserClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.twitter.TwitterApiUserClient;
import org.miniclient.ext.twitter.factory.TwitterApiUserClientFactory;
import org.miniclient.ext.twitter.impl.BaseTwitterApiUserClient;
import org.miniclient.ext.twitter.maker.TwitterApiUserClientMaker;
import org.miniclient.factory.impl.AbstractApiUserClientFactory;
import org.miniclient.maker.ApiUserClientMaker;


public class BaseTwitterApiUserClientFactory extends AbstractApiUserClientFactory implements TwitterApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseTwitterApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class TwitterApiUserClientFactoryHolder
    {
        private static final BaseTwitterApiUserClientFactory INSTANCE = new BaseTwitterApiUserClientFactory();
    }

    // Singleton method
    public static BaseTwitterApiUserClientFactory getInstance()
    {
        return TwitterApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new BaseTwitterApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return TwitterApiUserClientMaker.getInstance();
    }


    @Override
    public TwitterApiUserClient createTwitterApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new BaseTwitterApiUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public TwitterApiUserClient createTwitterApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        return new BaseTwitterApiUserClient(apiServiceClient, userCredential);
    }

 
}
