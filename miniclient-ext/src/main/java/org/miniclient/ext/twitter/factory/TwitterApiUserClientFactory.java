package org.miniclient.ext.twitter.factory;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.twitter.TwitterApiUserClient;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;


public interface TwitterApiUserClientFactory extends ApiUserClientFactory, ClientFactory
{
    TwitterApiUserClient createTwitterApiUserClient(String resourceBaseUrl, UserCredential userCredential);
    TwitterApiUserClient createTwitterApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential);

}
