package org.miniclient.ext.twitter.maker;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.impl.AbstractAuthRefreshPolicy;
import org.miniclient.common.impl.AbstractAutoRedirectPolicy;
import org.miniclient.common.impl.AbstractCacheControlPolicy;
import org.miniclient.common.impl.AbstractClientCachePolicy;
import org.miniclient.common.impl.AbstractCrudMethodFilter;
import org.miniclient.common.impl.AbstractRequestRetryPolicy;
import org.miniclient.impl.AbstractDataAccessClient;
import org.miniclient.impl.AbstractRestServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;


// Abstract factory.
public class TwitterApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(TwitterApiServiceClientMaker.class.getName());


    protected TwitterApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class TwitterApiServiceClientMakerHolder
    {
        private static final TwitterApiServiceClientMaker INSTANCE = new TwitterApiServiceClientMaker();
    }

    // Singleton method
    public static TwitterApiServiceClientMaker getInstance()
    {
        return TwitterApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new AbstractRequestRetryPolicy() {};
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new AbstractClientCachePolicy() {};
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new AbstractCacheControlPolicy() {};
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new AbstractAutoRedirectPolicy() {};
    }


}
