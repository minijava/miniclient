package org.miniclient.ext.twitter.factory;

import org.miniclient.ext.twitter.TwitterApiServiceClient;
import org.miniclient.factory.ApiServiceClientFactory;
import org.miniclient.factory.ClientFactory;


public interface TwitterApiServiceClientFactory extends ApiServiceClientFactory, ClientFactory
{
    TwitterApiServiceClient createTwitterApiServiceClient(String resourceBaseUrl);

}
