package org.miniclient.ext.google.mirror.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.impl.BaseGoogleApiUserClient;
import org.miniclient.ext.google.mirror.MirrorApiUserClient;
import org.miniclient.ext.google.mirror.maker.MirrorApiUserClientMaker;
import org.miniclient.maker.ApiUserClientMaker;


// Base class for all Mirror API resources.
public class BaseMirrorApiUserClient extends BaseGoogleApiUserClient implements MirrorApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseMirrorApiUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public BaseMirrorApiUserClient(String resourceBaseUrl,
            UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
    }

    public BaseMirrorApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseMirrorApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }
    

    // Factory methods

    @Override
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MirrorApiUserClientMaker.getInstance();
    }

    
    
    @Override
    public String toString()
    {
        return "BaseMirrorApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
