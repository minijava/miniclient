package org.miniclient.ext.google.tasks;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.core.ListResponseType;
import org.miniclient.ext.google.impl.BaseGoogleApiServiceClient;


// temporary
// https://developers.google.com/google-apps/tasks/v1/reference/tasklists
public class TaskListApiServiceClient extends BaseGoogleApiServiceClient
{
    private static final Logger log = Logger.getLogger(TaskListApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    public static final String TASKLIST_RESOURCE_BASE_URL = "https://www.googleapis.com/tasks/v1/users/@me/lists/";


    public TaskListApiServiceClient()
    {
        super(TASKLIST_RESOURCE_BASE_URL);
    }

    

    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.MAP_ITEMS);
        // ...

    }



    @Override
    public String toString()
    {
        return "TaskListApiServiceClient [getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceCacheControlPolicy()="
                + getRestServiceCacheControlPolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getCacheControlPolicy()="
                + getCacheControlPolicy() + "]";
    }

    
    
}
