package org.miniclient.ext.google.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleRestUserClient;
import org.miniclient.ext.google.factory.GoogleRestUserClientFactory;
import org.miniclient.ext.google.impl.BaseGoogleRestUserClient;
import org.miniclient.ext.google.maker.GoogleRestUserClientMaker;
import org.miniclient.factory.impl.AbstractRestUserClientFactory;
import org.miniclient.maker.RestUserClientMaker;


public class BaseGoogleRestUserClientFactory extends AbstractRestUserClientFactory implements GoogleRestUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleRestUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestUserClientFactoryHolder
    {
        private static final BaseGoogleRestUserClientFactory INSTANCE = new BaseGoogleRestUserClientFactory();
    }

    // Singleton method
    public static BaseGoogleRestUserClientFactory getInstance()
    {
        return GoogleRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestUserClient(resourceBaseUrl);
    }
    @Override
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return GoogleRestUserClientMaker.getInstance();
    }


    @Override
    public GoogleRestUserClient createGoogleRestUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new BaseGoogleRestUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleRestUserClient createGoogleRestUserClient(RestServiceClient restServiceClient, UserCredential userCredential)
    {
        return new BaseGoogleRestUserClient(restServiceClient, userCredential);
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestUserClient(resourceBaseUrl);
    }

    

    @Override
    public String toString()
    {
        return "BaseGoogleRestUserClientFactory []";
    }


}
