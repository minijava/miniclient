package org.miniclient.ext.google.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.RestApiException;
import org.miniclient.core.ListResponseType;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiServiceClient;
import org.miniclient.ext.google.maker.GoogleApiServiceClientMaker;
import org.miniclient.impl.AbstractApiServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;


// temporary
// Base class for all Google API resources.
public class BaseGoogleApiServiceClient extends AbstractApiServiceClient implements GoogleApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public BaseGoogleApiServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }

    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.MAP_ITEMS);
        // ...

    }


    // Factory methods

    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return GoogleApiServiceClientMaker.getInstance();
    }


    // Override methods
    //   primarily, for logging.

    @Override
    public Object get(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.get(): credential = " + credential + "; id = " + id);
        return super.get(credential, id);
    }

    @Override
    public List<Object> list(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.list(): credential = " + credential + "; params = " + params);
        return super.list(credential, params);
    }

    @Override
    public List<String> keys(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.keys(): credential = " + credential + "; params = " + params);
        return super.keys(credential, params);
    }

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData);
        return super.create(credential, inputData);
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        return super.create(credential, inputData, id);
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.update(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        return super.update(credential, inputData, id);
    }

    @Override
    public Object modify(UserCredential credential, Object partialData,
            String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.modify(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        return super.modify(credential, partialData, id);
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.delete(): credential = " + credential + "; id = " + id);
        return super.delete(credential, id);
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiServiceClient.delete(): credential = " + credential + "; params = " + params);
        return super.delete(credential, params);
    }


    @Override
    public String toString()
    {
        return "BaseGoogleApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }


}
