package org.miniclient.ext.google.mirror.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.credential.ClientCredential;
import org.miniclient.ext.google.impl.BaseGoogleServiceClient;
import org.miniclient.ext.google.mirror.MirrorServiceClient;


public class BaseMirrorServiceClient extends BaseGoogleServiceClient implements MirrorServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseMirrorServiceClient()
    {
        this(null);
    }

    public BaseMirrorServiceClient(ClientCredential clientCredential)
    {
        super(clientCredential);
    }


    
    @Override
    public String toString()
    {
        return "BaseMirrorServiceClient [getClientKey()=" + getClientKey()
                + ", getClientSecret()=" + getClientSecret()
                + ", getClientCredential()=" + getClientCredential() + "]";
    }


}
