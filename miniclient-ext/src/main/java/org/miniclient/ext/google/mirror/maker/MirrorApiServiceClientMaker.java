package org.miniclient.ext.google.mirror.maker;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.impl.AbstractCrudMethodFilter;
import org.miniclient.ext.google.common.GoogleAuthRefreshPolicy;
import org.miniclient.ext.google.common.GoogleAutoRedirectPolicy;
import org.miniclient.ext.google.common.GoogleCacheControlPolicy;
import org.miniclient.ext.google.common.GoogleClientCachePolicy;
import org.miniclient.ext.google.common.GoogleRequestRetryPolicy;
import org.miniclient.ext.google.impl.BaseGoogleRestServiceClient;
import org.miniclient.impl.AbstractDataAccessClient;
import org.miniclient.maker.ApiServiceClientMaker;


// Abstract factory.
public class MirrorApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MirrorApiServiceClientMaker.class.getName());


    protected MirrorApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class MirrorApiServiceClientMakerHolder
    {
        private static final MirrorApiServiceClientMaker INSTANCE = new MirrorApiServiceClientMaker();
    }

    // Singleton method
    public static MirrorApiServiceClientMaker getInstance()
    {
        return MirrorApiServiceClientMakerHolder.INSTANCE;
    }


    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new GoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new GoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new GoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new GoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MirrorApiServiceClientMaker []";
    }


}
