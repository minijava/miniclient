package org.miniclient.ext.google.factory;

import org.miniclient.ext.google.GoogleApiServiceClient;
import org.miniclient.factory.ApiServiceClientFactory;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;


public interface GoogleApiServiceClientFactory extends ApiServiceClientFactory, ClientFactory
{
    GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl);

    // ???
    // GoogleApiUserClientFactory createGoogleApiUserClientFactory();

}
