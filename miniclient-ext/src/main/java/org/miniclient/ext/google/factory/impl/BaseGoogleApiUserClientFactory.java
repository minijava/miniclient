package org.miniclient.ext.google.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ApiUserClient;
import org.miniclient.ResourceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiUserClient;
import org.miniclient.ext.google.factory.GoogleApiUserClientFactory;
import org.miniclient.ext.google.impl.BaseGoogleApiUserClient;
import org.miniclient.ext.google.maker.GoogleApiUserClientMaker;
import org.miniclient.factory.impl.AbstractApiUserClientFactory;
import org.miniclient.maker.ApiUserClientMaker;


public class BaseGoogleApiUserClientFactory extends AbstractApiUserClientFactory implements GoogleApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleApiUserClientFactoryHolder
    {
        private static final BaseGoogleApiUserClientFactory INSTANCE = new BaseGoogleApiUserClientFactory();
    }

    // Singleton method
    public static BaseGoogleApiUserClientFactory getInstance()
    {
        return GoogleApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiUserClient(resourceBaseUrl);
    }
    @Override
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return GoogleApiUserClientMaker.getInstance();
    }


    @Override
    public GoogleApiUserClient createGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new BaseGoogleApiUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleApiUserClient createGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        return new BaseGoogleApiUserClient(apiServiceClient, userCredential);
    }

    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiUserClient(resourceBaseUrl);
    }

    

    @Override
    public String toString()
    {
        return "BaseGoogleApiUserClientFactory []";
    }


}
