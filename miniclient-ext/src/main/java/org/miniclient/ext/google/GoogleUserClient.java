package org.miniclient.ext.google;

import org.miniclient.UserClient;
import org.miniclient.credential.UserCredential;


public interface GoogleUserClient extends UserClient, UserCredential
{

}
