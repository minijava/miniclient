package org.miniclient.ext.google.maker;

import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.common.impl.AbstractHttpMethodFilter;
import org.miniclient.ext.google.common.GoogleAuthRefreshPolicy;
import org.miniclient.ext.google.common.GoogleAutoRedirectPolicy;
import org.miniclient.ext.google.common.GoogleCacheControlPolicy;
import org.miniclient.ext.google.common.GoogleClientCachePolicy;
import org.miniclient.ext.google.common.GoogleRequestRetryPolicy;
import org.miniclient.ext.google.common.GoogleResourceUrlBuilder;
import org.miniclient.impl.AbstractDataAccessClient;
import org.miniclient.maker.RestServiceClientMaker;


// Google factory.
public class GoogleRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(GoogleRestServiceClientMaker.class.getName());


    protected GoogleRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleRestServiceClientMakerHolder
    {
        private static final GoogleRestServiceClientMaker INSTANCE = new GoogleRestServiceClientMaker();
    }

    // Singleton method
    public static GoogleRestServiceClientMaker getInstance()
    {
        return GoogleRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new GoogleResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new AbstractHttpMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new GoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new GoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new GoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new GoogleAutoRedirectPolicy();
    }


    
    @Override
    public String toString()
    {
        return "GoogleRestServiceClientMaker []";
    }


}
