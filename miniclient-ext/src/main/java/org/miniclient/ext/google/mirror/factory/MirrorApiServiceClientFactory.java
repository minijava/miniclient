package org.miniclient.ext.google.mirror.factory;

import org.miniclient.ext.google.factory.GoogleApiServiceClientFactory;
import org.miniclient.ext.google.mirror.MirrorApiServiceClient;


public interface MirrorApiServiceClientFactory extends GoogleApiServiceClientFactory
{
    MirrorApiServiceClient createMirrorContactApiServiceClient();
    MirrorApiServiceClient createMirrorLocationApiServiceClient();
    MirrorApiServiceClient createMirrorSubscriptionApiServiceClient();
    MirrorApiServiceClient createMirrorTimelineApiServiceClient();
    MirrorApiServiceClient createMirrorTimelineAttachmentApiServiceClient(String timelineItemId);

    // ???
    // MirrorApiUserClientFactory createMirrorApiUserClientFactory();
}
