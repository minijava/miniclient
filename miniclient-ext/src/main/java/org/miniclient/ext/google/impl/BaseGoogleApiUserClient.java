package org.miniclient.ext.google.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.RestApiException;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiUserClient;
import org.miniclient.ext.google.maker.GoogleApiUserClientMaker;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;


// Base class for all Google API resources.
public class BaseGoogleApiUserClient extends AbstractApiUserClient implements GoogleApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseGoogleApiUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public BaseGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
    }

    public BaseGoogleApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }
    

    // Factory methods

    @Override
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return GoogleApiUserClientMaker.getInstance();
    }


    // Override methods
    //   primarily, for logging.

    @Override
    public Object get(String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.get(): id = " + id);
        return super.get(id);
    }
    @Override
    public List<Object> list(Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.list(): params = " + params);
        return super.list(params);
    }
    @Override
    public List<String> keys(Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.keys(): params = " + params);
        return super.keys(params);
    }
    @Override
    public Object create(Object inputData) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.create(): inputData = " + inputData);
        return super.create(inputData);
    }
    @Override
    public Object create(Object inputData, String id) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.create(): inputData = " + inputData + "; id = " + id);
        return super.create(inputData, id);
    }
    @Override
    public Object update(Object inputData, String id) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.update(): inputData = " + inputData + "; id = " + id);
        return super.update(inputData, id);
    }
    @Override
    public Object modify(Object partialData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.modify(): partialData = " + partialData + "; id = " + id);
        return super.modify(partialData, id);
    }
    @Override
    public boolean delete(String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.delete(): id = " + id);
        return super.delete(id);
    }
    @Override
    public int delete(Map<String, Object> params) throws RestApiException,
            IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleApiUserClient.delete(): params = " + params);
        return super.delete(params);
    }

    
    @Override
    public String toString()
    {
        return "BaseGoogleApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }
    

}
