package org.miniclient.ext.google.proxy;

import org.miniclient.ext.google.GoogleApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedGoogleApiServiceClient extends GoogleApiServiceClient
{
    // No need for API to return the decorated client.
    // GoogleApiServiceClient getDecoratedClient();
}
