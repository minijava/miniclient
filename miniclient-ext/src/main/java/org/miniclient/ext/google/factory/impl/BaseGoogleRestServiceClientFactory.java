package org.miniclient.ext.google.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.ext.google.GoogleRestServiceClient;
import org.miniclient.ext.google.factory.GoogleApiUserClientFactory;
import org.miniclient.ext.google.factory.GoogleRestServiceClientFactory;
import org.miniclient.ext.google.factory.GoogleRestUserClientFactory;
import org.miniclient.ext.google.impl.BaseGoogleRestServiceClient;
import org.miniclient.ext.google.maker.GoogleRestServiceClientMaker;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.factory.impl.AbstractApiUserClientFactory;
import org.miniclient.factory.impl.AbstractRestServiceClientFactory;
import org.miniclient.factory.impl.AbstractRestUserClientFactory;
import org.miniclient.maker.RestServiceClientMaker;


public class BaseGoogleRestServiceClientFactory extends AbstractRestServiceClientFactory implements GoogleRestServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleRestServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestServiceClientFactoryHolder
    {
        private static final BaseGoogleRestServiceClientFactory INSTANCE = new BaseGoogleRestServiceClientFactory();
    }

    // Singleton method
    public static BaseGoogleRestServiceClientFactory getInstance()
    {
        return GoogleRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }
    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return GoogleRestServiceClientMaker.getInstance();
    }
    @Override
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return makeGoogleRestUserClientFactory();
    }
    protected GoogleRestUserClientFactory makeGoogleRestUserClientFactory()
    {
        return BaseGoogleRestUserClientFactory.getInstance();
    }

 
    @Override
    public GoogleRestServiceClient createGoogleRestServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleRestServiceClient(resourceBaseUrl);
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestServiceClient(resourceBaseUrl);
    }



//    @Override
//    public GoogleRestUserClientFactory createGoogleRestUserClientFactory()
//    {
//        return makeGoogleRestUserClientFactory();
//    }



    @Override
    public String toString()
    {
        return "BaseGoogleRestServiceClientFactory []";
    }


}
