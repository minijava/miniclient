package org.miniclient.ext.google.tasks;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.impl.BaseGoogleApiUserClient;


public class TaskListApiUserClient extends BaseGoogleApiUserClient
{
    private static final Logger log = Logger.getLogger(TaskListApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public TaskListApiUserClient()
    {
        this((UserCredential) null);
    }
    public TaskListApiUserClient(UserCredential userCredential)
    {
        super(TaskListApiServiceClient.TASKLIST_RESOURCE_BASE_URL, userCredential);
    }

    public TaskListApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public TaskListApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }


    @Override
    public String toString()
    {
        return "TaskListApiUserClient [getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getCacheControlPolicy()=" + getCacheControlPolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
