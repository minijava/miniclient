package org.miniclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractResourceUrlBuilder;
import org.miniclient.ext.google.mirror.MirrorApiServiceClient;
import org.miniclient.ext.google.mirror.impl.BaseMirrorApiServiceClient;


// temporary
public class BaseMirrorTimelineAttachmentApiServiceClient extends BaseMirrorApiServiceClient implements MirrorApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorTimelineAttachmentApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public static final String TIMELINE_PARENTRESOURCE_BASE_URL = "https://www.googleapis.com/mirror/v1/timeline/";
    public static final String ATTACHMENT_RESOURCENAME = "attachments";

    
    public BaseMirrorTimelineAttachmentApiServiceClient(String timelineItemId)
    {
        super(AbstractResourceUrlBuilder.buildBaseUrl(TIMELINE_PARENTRESOURCE_BASE_URL, timelineItemId, ATTACHMENT_RESOURCENAME));
    }



    @Override
    public String toString()
    {
        return "BaseMirrorTimelineAttachmentApiServiceClient [getRestServiceClient()="
                + getRestServiceClient()
                + ", getCrudMethodFilter()="
                + getCrudMethodFilter()
                + ", getListResponseType()="
                + getListResponseType()
                + ", getResourceBaseUrl()="
                + getResourceBaseUrl()
                + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getRequiredScopes()="
                + getRequiredScopes()
                + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy()
                + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }

    
}
