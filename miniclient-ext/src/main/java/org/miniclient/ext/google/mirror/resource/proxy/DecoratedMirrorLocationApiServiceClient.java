package org.miniclient.ext.google.mirror.resource.proxy;

import org.miniclient.ext.google.mirror.resource.MirrorLocationApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorLocationApiServiceClient extends MirrorLocationApiServiceClient
{
    // No need for API to return the decorated client.
    // MirrorLocationApiServiceClient getDecoratedClient();
}
