package org.miniclient.ext.google.mirror.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiUserClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.factory.impl.BaseGoogleApiUserClientFactory;
import org.miniclient.ext.google.mirror.MirrorApiServiceClient;
import org.miniclient.ext.google.mirror.MirrorApiUserClient;
import org.miniclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import org.miniclient.ext.google.mirror.impl.BaseMirrorApiUserClient;
import org.miniclient.ext.google.mirror.maker.MirrorApiUserClientMaker;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorContactApiUserClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorLocationApiUserClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorSubscriptionApiUserClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorTimelineApiUserClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorTimelineAttachmentApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;


public class BaseMirrorApiUserClientFactory extends BaseGoogleApiUserClientFactory implements MirrorApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(BaseMirrorApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class MirrorApiUserClientFactoryHolder
    {
        private static final BaseMirrorApiUserClientFactory INSTANCE = new BaseMirrorApiUserClientFactory();
    }

    // Singleton method
    public static BaseMirrorApiUserClientFactory getInstance()
    {
        return MirrorApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new BaseMirrorApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MirrorApiUserClientMaker.getInstance();
    }

    
    // Resource create methods

    @Override
    public MirrorApiUserClient createMirrorContactApiUserClient(UserCredential userCredential)
    {
        return new BaseMirrorContactApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorLocationApiUserClient(UserCredential userCredential)
    {
        return new BaseMirrorLocationApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorSubscriptionApiUserClient(UserCredential userCredential)
    {
        return new BaseMirrorSubscriptionApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineApiUserClient(UserCredential userCredential)
    {
        return new BaseMirrorTimelineApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(String timelineItemId, UserCredential userCredential)
    {
        return new BaseMirrorTimelineAttachmentApiUserClient(timelineItemId, userCredential);
    }


    @Override
    public MirrorApiUserClient createMirrorContactApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new BaseMirrorContactApiUserClient(serviceClient, userCredential);
    }


    @Override
    public MirrorApiUserClient createMirrorLocationApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new BaseMirrorLocationApiUserClient(serviceClient, userCredential);
    }


    @Override
    public MirrorApiUserClient createMirrorSubscriptionApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new BaseMirrorSubscriptionApiUserClient(serviceClient, userCredential);
    }


    @Override
    public MirrorApiUserClient createMirrorTimelineApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new BaseMirrorTimelineApiUserClient(serviceClient, userCredential);
    }


    @Override
    public MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new BaseMirrorTimelineAttachmentApiUserClient(serviceClient, userCredential);
    }


    @Override
    public String toString()
    {
        return "BaseMirrorApiUserClientFactory []";
    }


}
