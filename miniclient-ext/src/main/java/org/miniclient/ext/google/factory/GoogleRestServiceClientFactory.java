package org.miniclient.ext.google.factory;

import org.miniclient.ext.google.GoogleRestServiceClient;
import org.miniclient.factory.ClientFactory;
import org.miniclient.factory.RestServiceClientFactory;
import org.miniclient.factory.RestUserClientFactory;


public interface GoogleRestServiceClientFactory extends RestServiceClientFactory, ClientFactory
{
    GoogleRestServiceClient createGoogleRestServiceClient(String resourceBaseUrl);

    // ???
    // GoogleRestUserClientFactory createGoogleRestUserClientFactory();

}
