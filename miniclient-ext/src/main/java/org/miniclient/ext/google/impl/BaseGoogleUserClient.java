package org.miniclient.ext.google.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleUserClient;
import org.miniclient.ext.google.common.GoogleAuthRefreshPolicy;
import org.miniclient.impl.AbstractUserClient;


public class BaseGoogleUserClient extends AbstractUserClient implements GoogleUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    public BaseGoogleUserClient()
    {
        this(null);
    }

    public BaseGoogleUserClient(UserCredential userCredential)
    {
        super(userCredential);
    }

    
    // Factory method.
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }


    
    @Override
    public String toString()
    {
        return "BaseGoogleUserClient [getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", isAuthRequired()="
                + isAuthRequired() + ", getUser()=" + getUser()
                + ", getUserId()=" + getUserId() + ", getAuthMethod()="
                + getAuthMethod() + ", getAuthToken()=" + getAuthToken()
                + ", getAuthSecret()=" + getAuthSecret() + ", getDataScopes()="
                + getDataScopes() + ", getExpirationTime()="
                + getExpirationTime() + ", getRefreshedTime()="
                + getRefreshedTime() + ", getUserCredential()="
                + getUserCredential() + "]";
    }


}
