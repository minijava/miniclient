package org.miniclient.ext.google.factory;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiUserClient;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;


public interface GoogleApiUserClientFactory extends ApiUserClientFactory, ClientFactory
{
    GoogleApiUserClient createGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential);
    GoogleApiUserClient createGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential);
    
}
