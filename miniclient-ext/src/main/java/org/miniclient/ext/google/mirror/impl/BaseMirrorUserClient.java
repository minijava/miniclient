package org.miniclient.ext.google.mirror.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.common.GoogleAuthRefreshPolicy;
import org.miniclient.ext.google.impl.BaseGoogleUserClient;
import org.miniclient.ext.google.mirror.MirrorUserClient;


public class BaseMirrorUserClient extends BaseGoogleUserClient implements MirrorUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    public BaseMirrorUserClient()
    {
        this(null);
    }

    public BaseMirrorUserClient(UserCredential userCredential)
    {
        super(userCredential);
    }

    
    // Factory method.

    @Override
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new GoogleAuthRefreshPolicy();
    }


    
    @Override
    public String toString()
    {
        return "BaseMirrorUserClient [getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", isAuthRequired()="
                + isAuthRequired() + ", getUser()=" + getUser()
                + ", getUserId()=" + getUserId() + ", getAuthMethod()="
                + getAuthMethod() + ", getAuthToken()=" + getAuthToken()
                + ", getAuthSecret()=" + getAuthSecret() + ", getDataScopes()="
                + getDataScopes() + ", getExpirationTime()="
                + getExpirationTime() + ", getRefreshedTime()="
                + getRefreshedTime() + ", getUserCredential()="
                + getUserCredential() + "]";
    }

    
}
