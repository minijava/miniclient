package org.miniclient.ext.google;

import org.miniclient.RestServiceClient;
import org.miniclient.common.ResourceUrlBuilder;


public interface GoogleRestServiceClient extends RestServiceClient, ResourceUrlBuilder
{

}
