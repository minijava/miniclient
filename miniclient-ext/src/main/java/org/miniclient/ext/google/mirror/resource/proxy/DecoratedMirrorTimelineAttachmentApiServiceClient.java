package org.miniclient.ext.google.mirror.resource.proxy;

import org.miniclient.ext.google.mirror.resource.MirrorTimelineAttachmentApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorTimelineAttachmentApiServiceClient extends MirrorTimelineAttachmentApiServiceClient
{
    // No need for API to return the decorated client.
    // MirrorTimelineAttachmentApiServiceClient getDecoratedClient();
}
