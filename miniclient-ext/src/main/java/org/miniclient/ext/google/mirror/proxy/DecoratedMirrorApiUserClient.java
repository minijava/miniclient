package org.miniclient.ext.google.mirror.proxy;

import org.miniclient.ext.google.mirror.MirrorApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorApiUserClient extends MirrorApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorApiUserClient getDecoratedClient();
}
