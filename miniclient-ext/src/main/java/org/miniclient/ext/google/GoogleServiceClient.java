package org.miniclient.ext.google;

import org.miniclient.ServiceClient;
import org.miniclient.credential.ClientCredential;


public interface GoogleServiceClient extends ServiceClient, ClientCredential
{

}
