package org.miniclient.ext.google.mirror.resource.proxy;

import org.miniclient.ext.google.mirror.resource.MirrorContactApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorContactApiUserClient extends MirrorContactApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorContactApiUserClient getDecoratedClient();
}
