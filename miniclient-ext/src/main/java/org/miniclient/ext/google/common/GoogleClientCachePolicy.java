package org.miniclient.ext.google.common;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractClientCachePolicy;


public class GoogleClientCachePolicy extends AbstractClientCachePolicy
{
    private static final Logger log = Logger.getLogger(GoogleClientCachePolicy.class.getName());


    
    @Override
    public String toString()
    {
        return "GoogleClientCachePolicy [isCacheEnabled()=" + isCacheEnabled()
                + ", getCacheLifetime()=" + getCacheLifetime() + "]";
    }


    
}
