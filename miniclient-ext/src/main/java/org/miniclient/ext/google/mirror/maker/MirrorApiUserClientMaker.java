package org.miniclient.ext.google.mirror.maker;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ext.google.mirror.impl.BaseMirrorApiServiceClient;
import org.miniclient.maker.ApiUserClientMaker;


// Abstract factory.
public class MirrorApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(MirrorApiUserClientMaker.class.getName());


    protected MirrorApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class MirrorApiUserClientMakerHolder
    {
        private static final MirrorApiUserClientMaker INSTANCE = new MirrorApiUserClientMaker();
    }

    // Singleton method
    public static MirrorApiUserClientMaker getInstance()
    {
        return MirrorApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new BaseMirrorApiServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MirrorApiUserClientMaker []";
    }

    
}
