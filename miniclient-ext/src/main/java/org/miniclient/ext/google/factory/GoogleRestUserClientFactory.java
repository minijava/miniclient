package org.miniclient.ext.google.factory;

import org.miniclient.RestServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleRestUserClient;
import org.miniclient.factory.ClientFactory;
import org.miniclient.factory.RestUserClientFactory;


public interface GoogleRestUserClientFactory extends RestUserClientFactory, ClientFactory
{
    GoogleRestUserClient createGoogleRestUserClient(String resourceBaseUrl, UserCredential userCredential);
    GoogleRestUserClient createGoogleRestUserClient(RestServiceClient restServiceClient, UserCredential userCredential);
    
}
