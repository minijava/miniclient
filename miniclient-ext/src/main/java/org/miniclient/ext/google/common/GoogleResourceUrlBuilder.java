package org.miniclient.ext.google.common;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractResourceUrlBuilder;


// Google implementation.
public class GoogleResourceUrlBuilder extends AbstractResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(GoogleResourceUrlBuilder.class.getName());

    public GoogleResourceUrlBuilder(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


    
    @Override
    public String toString()
    {
        return "GoogleResourceUrlBuilder [getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getParentResourceBaseUrl()="
                + getParentResourceBaseUrl() + ", getParentResourceId()="
                + getParentResourceId() + ", getResourceName()="
                + getResourceName() + ", getSuffix()=" + getSuffix()
                + ", getResourcePostUrl()=" + getResourcePostUrl() + "]";
    }


}
