package org.miniclient.ext.google.factory.manager;

import java.util.logging.Logger;

import org.miniclient.ext.google.factory.GoogleRestServiceClientFactory;
import org.miniclient.ext.google.factory.GoogleRestUserClientFactory;
import org.miniclient.ext.google.factory.impl.BaseGoogleRestServiceClientFactory;


/**
 * This manager class uses the "Abstract Factory" pattern.
 * Client can inherit from this class to return a proper concrete factory(s).
 */
public abstract class AbstractGoogleRestClientFactoryManager
{
    private static final Logger log = Logger.getLogger(AbstractGoogleRestClientFactoryManager.class.getName());

    private GoogleRestServiceClientFactory googleRestServiceClientFactory;
    private GoogleRestUserClientFactory googleRestUserClientFactory;
    
    // FactoryManager can be a singleton.
    protected AbstractGoogleRestClientFactoryManager() 
    {
        googleRestServiceClientFactory = BaseGoogleRestServiceClientFactory.getInstance();
        // googleRestUserClientFactory = BaseGoogleRestUserClientFactory.getInstance();
        googleRestUserClientFactory = (GoogleRestUserClientFactory) googleRestServiceClientFactory.createRestUserClientFactory();
    }


    // Initialization-on-demand holder.
    private static final class AbstractGoogleRestClientFactoryManagerHolder
    {
        private static final AbstractGoogleRestClientFactoryManager INSTANCE = new AbstractGoogleRestClientFactoryManager() {};
    }

    // Singleton method
    public static AbstractGoogleRestClientFactoryManager getInstance()
    {
        return AbstractGoogleRestClientFactoryManagerHolder.INSTANCE;
    }


    // Note:
    // Client should override these methods, or the constructor, to return appropriate concrete factories.
    
    // Returns a service client factory.
    public GoogleRestServiceClientFactory getGoogleRestServiceClientFactory()
    {
        return googleRestServiceClientFactory;
    }

    // Returns a user client factory.
    public GoogleRestUserClientFactory getGoogleRestUserClientFactory()
    {
        return googleRestUserClientFactory;
    }


    // TBD:
    // Setters to inject factories, if needed.
    // ....
    
    public void setGoogleRestClientFactories(GoogleRestServiceClientFactory googleRestServiceClientFactory)
    {
        setGoogleRestClientFactories(googleRestServiceClientFactory, null);
    }
    public void setGoogleRestClientFactories(GoogleRestServiceClientFactory googleRestServiceClientFactory, GoogleRestUserClientFactory googleRestUserClientFactory)
    {
        if(googleRestServiceClientFactory != null) {
            this.googleRestServiceClientFactory = googleRestServiceClientFactory;
            if(googleRestUserClientFactory != null) {
                this.googleRestUserClientFactory = googleRestUserClientFactory;
            } else {
                this.googleRestUserClientFactory = (GoogleRestUserClientFactory) googleRestServiceClientFactory.createRestUserClientFactory();
            }
        } else {
            // ???
            log.info("Input googleRestServiceClientFactory is null. Both googleRestServiceClientFactory and googleRestUserClientFactory will be ignored.");
        }

    }
    

}
