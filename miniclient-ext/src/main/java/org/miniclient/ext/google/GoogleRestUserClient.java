package org.miniclient.ext.google;

import org.miniclient.RestUserClient;
import org.miniclient.common.ResourceUrlBuilder;


public interface GoogleRestUserClient extends RestUserClient, ResourceUrlBuilder
{

}
