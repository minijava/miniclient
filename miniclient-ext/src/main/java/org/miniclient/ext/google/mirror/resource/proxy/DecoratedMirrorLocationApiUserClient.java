package org.miniclient.ext.google.mirror.resource.proxy;

import org.miniclient.ext.google.mirror.resource.MirrorLocationApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedMirrorLocationApiUserClient extends MirrorLocationApiUserClient
{
    // No need for API to return the decorated client.
    // MirrorLocationApiUserClient getDecoratedClient();
}
