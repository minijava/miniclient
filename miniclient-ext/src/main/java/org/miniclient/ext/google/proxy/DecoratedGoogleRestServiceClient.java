package org.miniclient.ext.google.proxy;

import org.miniclient.ext.google.GoogleRestServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedGoogleRestServiceClient extends GoogleRestServiceClient
{
    // No need for API to return the decorated client.
    // GoogleRestServiceClient getDecoratedClient();
}
