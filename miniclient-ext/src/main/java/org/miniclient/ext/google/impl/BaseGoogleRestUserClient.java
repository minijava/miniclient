package org.miniclient.ext.google.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleRestUserClient;
import org.miniclient.ext.google.maker.GoogleRestUserClientMaker;
import org.miniclient.impl.AbstractRestUserClient;
import org.miniclient.maker.RestUserClientMaker;


public class BaseGoogleRestUserClient extends AbstractRestUserClient implements GoogleRestUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseGoogleRestUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public BaseGoogleRestUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public BaseGoogleRestUserClient(String resourceBaseUrl,  UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
    }

    public BaseGoogleRestUserClient(RestServiceClient restServiceClient)
    {
        this(restServiceClient, null);
    }
    public BaseGoogleRestUserClient(RestServiceClient restServiceClient,
            UserCredential userCredential)
    {
        super(restServiceClient, userCredential);
    }


    // Factory methods

    @Override
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return GoogleRestUserClientMaker.getInstance();
    }


    // Override methods
    //   primarily, for logging.

    @Override
    public Map<String, Object> get(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestUserClient.get(): id = " + id + "; params = " + params);
        return super.get(id, params);
    }

    @Override
    public Map<String, Object> post(Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestUserClient.post(): inputData = " + inputData);
        return super.post(inputData);
    }

    @Override
    public Map<String, Object> put(Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestUserClient.put(): inputData = " + inputData + "; id = " + id);
        return super.put(inputData, id);
    }

    @Override
    public Map<String, Object> patch(Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestUserClient.patch(): partialData = " + partialData + "; id = " + id);
        return super.patch(partialData, id);
    }

    @Override
    public Map<String, Object> delete(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("BaseGoogleRestUserClient.delete(): id = " + id + "; params = " + params);
        return super.delete(id, params);
    }

    
    @Override
    public String toString()
    {
        return "BaseGoogleRestUserClient [getRestServiceClient()="
                + getRestServiceClient() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getResourcePostUrl()="
                + getResourcePostUrl() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + "]";
    }


}
