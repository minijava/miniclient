package org.miniclient.ext.google.mirror.resource.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.mirror.MirrorApiUserClient;
import org.miniclient.ext.google.mirror.impl.BaseMirrorApiUserClient;


public class BaseMirrorContactApiUserClient extends BaseMirrorApiUserClient implements MirrorApiUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorContactApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public BaseMirrorContactApiUserClient()
    {
        this((UserCredential) null);
    }
    public BaseMirrorContactApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorContactApiServiceClient.CONTACT_RESOURCE_BASE_URL, userCredential);
    }

    public BaseMirrorContactApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public BaseMirrorContactApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "BaseMirrorContactApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
