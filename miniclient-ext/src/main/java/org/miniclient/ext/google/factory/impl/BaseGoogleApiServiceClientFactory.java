package org.miniclient.ext.google.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ResourceClient;
import org.miniclient.ext.google.GoogleApiServiceClient;
import org.miniclient.ext.google.factory.GoogleApiServiceClientFactory;
import org.miniclient.ext.google.factory.GoogleApiUserClientFactory;
import org.miniclient.ext.google.impl.BaseGoogleApiServiceClient;
import org.miniclient.ext.google.maker.GoogleApiServiceClientMaker;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.impl.AbstractApiServiceClientFactory;
import org.miniclient.maker.ApiServiceClientMaker;


public class BaseGoogleApiServiceClientFactory extends AbstractApiServiceClientFactory implements GoogleApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(BaseGoogleApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleApiServiceClientFactoryHolder
    {
        private static final BaseGoogleApiServiceClientFactory INSTANCE = new BaseGoogleApiServiceClientFactory();
    }

    // Singleton method
    public static BaseGoogleApiServiceClientFactory getInstance()
    {
        return GoogleApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiServiceClient(resourceBaseUrl);
    }
    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return GoogleApiServiceClientMaker.getInstance();
    }
    @Override
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return makeGoogleApiUserClientFactory();
    }
    protected GoogleApiUserClientFactory makeGoogleApiUserClientFactory()
    {
        return BaseGoogleApiUserClientFactory.getInstance();
    }


    @Override
    public GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl)
    {
        return new BaseGoogleApiServiceClient(resourceBaseUrl);
    }

    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiServiceClient(resourceBaseUrl);
    }



//    @Override
//    public GoogleApiUserClientFactory createGoogleApiUserClientFactory()
//    {
//        return makeGoogleApiUserClientFactory();
//    }



    @Override
    public String toString()
    {
        return "BaseGoogleApiServiceClientFactory []";
    }


}
