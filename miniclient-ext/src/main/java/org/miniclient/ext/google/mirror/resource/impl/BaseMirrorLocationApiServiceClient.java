package org.miniclient.ext.google.mirror.resource.impl;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.RestApiException;
import org.miniclient.credential.UserCredential;
import org.miniclient.exception.MethodNotAllowedRaException;
import org.miniclient.ext.google.mirror.MirrorApiServiceClient;
import org.miniclient.ext.google.mirror.impl.BaseMirrorApiServiceClient;


// temporary
public class BaseMirrorLocationApiServiceClient extends BaseMirrorApiServiceClient implements MirrorApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(BaseMirrorLocationApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public static final String LOCATION_RESOURCE_BASE_URL = "https://www.googleapis.com/mirror/v1/locations";

    public BaseMirrorLocationApiServiceClient()
    {
        super(LOCATION_RESOURCE_BASE_URL);
    }

    
    // temporary

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        throw new MethodNotAllowedRaException();
    }


    @Override
    public String toString()
    {
        return "BaseMirrorLocationApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }

    
}
