package org.miniclient.ext.google.tasks;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ TaskApiUserClientTest.class, TaskListApiUserClientTest.class })
public class AllTests
{

}
