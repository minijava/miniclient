package org.miniclient.ext.google.tasks;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.miniclient.RestApiException;
import org.miniclient.core.AuthMethod;
import org.miniclient.credential.UserCredential;
import org.miniclient.credential.impl.AbstractUserCredential;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope: https://www.googleapis.com/auth/tasks
// https://developers.google.com/google-apps/tasks/v1/reference/tasks
public class TaskApiUserClientTest
{
    private static final Logger log = Logger.getLogger(TaskApiUserClientTest.class.getName());


    private TaskApiUserClient taskApiUserClient;
    
    
    @Before
    public void setUp() throws Exception
    {
        String parentResourceId = "@default";    // ???
        // String parentResourceId = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6NDQ0NTMwMTUyOjA";
        taskApiUserClient = new TaskApiUserClient(parentResourceId);
        
        String user = "ab";
        String userId = "123";
        String authToken = "ya29.AHES6ZQNJ1TWlg40_qrIJ92CLzgG8oB4IDXacVSPrahBYehJ0rcfCZ5M";
        Set<String> dataScopes = null;
        Long expirationTime = null;
        UserCredential authCredential = new AbstractUserCredential(true, user, userId, AuthMethod.BEARER, authToken, null, dataScopes, expirationTime, null) {};
        taskApiUserClient.setUserCredential(authCredential);
        
    }

    @After
    public void tearDown() throws Exception
    {
        taskApiUserClient = null;
    }


    @Test
    public void testGet()
    {
        Object task = null;
        String id = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6MDoxMjg2MTU1MjM4";
        try {
            task = taskApiUserClient.get(id);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("task = " + task);
    }

    @Test
    public void testList()
    {
        List<Object> tasks = null;
        try {
            Map<String,Object> params = null;
            tasks = taskApiUserClient.list(params);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("tasks = " + tasks);
    }

    @Test
    public void testKeys()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObject()
    {
        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("kind", "tasks#task");
        inputData.put("title", "test task 1");
        // etc...
        
        Object task = null;
        try {
            task = taskApiUserClient.create(inputData);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("task = " + task);
    }

    @Test
    public void testCreateObjectString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testUpdate()
    {
        String id = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6MDoxMjg2MTU1MjM4";

        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("kind", "tasks#task");
        inputData.put("id", id);
        inputData.put("title", "test task 1 modified - " + System.currentTimeMillis());
        // etc...
        
        Object task = null;
        try {
            task = taskApiUserClient.update(inputData, id);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("task = " + task);
    }

    @Test
    public void testDeleteString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
        // fail("Not yet implemented");
    }

}
