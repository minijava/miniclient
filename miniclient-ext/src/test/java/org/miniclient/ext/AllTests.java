package org.miniclient.ext;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ org.miniclient.ext.google.tasks.AllTests.class })
public class AllTests
{

}
