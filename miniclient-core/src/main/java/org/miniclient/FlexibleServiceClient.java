package org.miniclient;

import org.miniclient.credential.ClientCredential;


/**
 * Service specific client object instance.
 */
public interface FlexibleServiceClient extends ServiceClient
{
    void setClientCredential(ClientCredential clientCredential);
}
