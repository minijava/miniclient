package org.miniclient;



/**
 * User specific client object instance.
 */
public interface ResourceUserClient extends ResourceClient, UserClient
{

}
