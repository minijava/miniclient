package org.miniclient;

import org.miniclient.credential.UserCredential;


/**
 * User specific client object instance.
 * The instances of UserClient are specific to particular users,
 *    and they cannot be shared.
 */
public interface UserClient
{
    /**
     * Returns the UserCredential associated with this UserClient.
     * @return UserCredential
     */
    UserCredential getUserCredential();
}
