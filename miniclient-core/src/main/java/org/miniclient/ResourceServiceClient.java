package org.miniclient;

import org.miniclient.common.DataAccessClient;



/**
 * Service specific client object instance.
 */
public interface ResourceServiceClient extends ResourceClient, ServiceClient, DataAccessClient
{
  
}
