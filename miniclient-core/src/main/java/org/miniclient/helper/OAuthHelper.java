package org.miniclient.helper;

import java.util.UUID;
import java.util.logging.Level;
import java.util.logging.Logger;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;

import org.miniclient.core.OAuthConstants;
import org.miniclient.util.HashUtil;


/**
 * Note that OAuth means OAuth v1.
 * (The newer version of OAuth is specifically referred as OAuth2.)
 */
public class OAuthHelper
{
    private static final Logger log = Logger.getLogger(OAuthHelper.class.getName());

    // Hard coded. Always 1.0.
    public static final String OAUTH_VERSION = "1.0";
    
    // temporary
    private static final String DEFAULT_SIGNATURE_METHOD = "HMAC-SHA1";
    
    
    // Initialization-on-demand holder.
    private static final class DebugHelperHolder
    {
        private static final OAuthHelper INSTANCE = new OAuthHelper();
    }

    // Singleton method
    public static OAuthHelper getInstance()
    {
        return DebugHelperHolder.INSTANCE;
    }

    // The only customizable var.
    private String signatureMethod;

    
    private OAuthHelper()
    {
        signatureMethod = DEFAULT_SIGNATURE_METHOD;
    }
    public String getSignatureMethod()
    {
        return signatureMethod;
    }


    public void setSignatureMethod(String signatureMethod)
    {
        this.signatureMethod = signatureMethod;
    }


    public String generateNonce()
    {
        // temporary
        String nonce = HashUtil.generateSha1Hash(UUID.randomUUID().toString());
        return nonce;
    }

    // Cf. https://dev.twitter.com/docs/auth/creating-signature
    // http://jersey.java.net/nonav/apidocs/1.1.1-ea/contribs/jersey-oauth/oauth-signature/com/sun/jersey/oauth/signature/OAuthSignature.html
    // https://code.google.com/p/oauth-signpost/
    public String generateOAuthSiganture(String content)
    {
        if(content == null) {
            return null;
        }
        
        
        
//        Mac mac = Mac.getInstance("HmacSHA1");
//        SecretKeySpec secret = new SecretKeySpec(key.getBytes(), mac.getAlgorithm());
//        mac.init(secret);
//        byte[] digest = mac.doFinal(baseString.getBytes());
//        // byte[] result=Base64.encodeBase64(digest);
        
        
        return null;
    }

    public String generateOAuthHeader(String consumerKey, String accessToken, String content)
    {
        StringBuilder sb = new StringBuilder();

        sb.append(OAuthConstants.OAUTH_CONSUMER_KEY).append("=").append(consumerKey);
        String nonce = generateNonce();
        sb.append(OAuthConstants.OAUTH_NONCE).append("=").append(nonce);
        String signature = generateOAuthSiganture(content);
        sb.append(OAuthConstants.OAUTH_SIGNATURE).append("=").append(signature);
        sb.append(OAuthConstants.OAUTH_SIGNATURE_METHOD).append("=").append(signatureMethod);
        long now = System.currentTimeMillis();
        sb.append(OAuthConstants.OAUTH_TIMESTAMP).append("=").append(now);
        sb.append(OAuthConstants.OAUTH_ACCESS_TOKEN).append("=").append(accessToken);
        sb.append(OAuthConstants.OAUTH_VERSION).append("=").append(OAUTH_VERSION);

        String header = sb.toString();
        if(log.isLoggable(Level.FINE)) log.fine("header = " + header);

        return header;
    }


}
