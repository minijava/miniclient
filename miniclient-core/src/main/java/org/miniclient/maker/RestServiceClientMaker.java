package org.miniclient.maker;

import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.ResourceUrlBuilder;


public interface RestServiceClientMaker extends ClientPolicyMaker
{
    // Factory methods

    ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl);
    // ResourceUrlBuilder makeResourceUrlBuilder(String parentResourceBaseUrl, String parentResourceId, String resourceName);

    HttpMethodFilter makeHttpMethodFilter();
    DataAccessClient makeDataAccessClient();

}
