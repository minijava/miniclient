package org.miniclient.maker;

import org.miniclient.ApiServiceClient;
import org.miniclient.ApiUserClient;
import org.miniclient.RestUserClient;


public interface ApiUserClientMaker
{
    // Factory methods
//    RestUserClient makeRestClient(String resourceBaseUrl);
    ApiServiceClient makeServiceClient(String resourceBaseUrl);
    // ApiServiceClient makeServiceClient(String parentResourceBaseUrl, String parentResourceId, String resourceName);

}
