package org.miniclient.maker;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.RequestRetryPolicy;


public interface ClientPolicyMaker
{
    // Factory methods

    AuthRefreshPolicy makeAuthRefreshPolicy();
    RequestRetryPolicy makeRequestRetryPolicy();
    ClientCachePolicy makeClientCachePolicy();
    CacheControlPolicy makeCacheControlPolicy();
    AutoRedirectPolicy makeAutoRedirectPolicy();

}
