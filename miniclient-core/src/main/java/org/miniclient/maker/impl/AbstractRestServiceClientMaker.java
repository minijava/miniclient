package org.miniclient.maker.impl;

import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.common.impl.AbstractAuthRefreshPolicy;
import org.miniclient.common.impl.AbstractAutoRedirectPolicy;
import org.miniclient.common.impl.AbstractCacheControlPolicy;
import org.miniclient.common.impl.AbstractClientCachePolicy;
import org.miniclient.common.impl.AbstractHttpMethodFilter;
import org.miniclient.common.impl.AbstractRequestRetryPolicy;
import org.miniclient.common.impl.AbstractResourceUrlBuilder;
import org.miniclient.impl.AbstractDataAccessClient;
import org.miniclient.maker.RestServiceClientMaker;


// Abstract factory.
public abstract class AbstractRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractRestServiceClientMaker.class.getName());


    protected AbstractRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestServiceClientMakerHolder
    {
        private static final AbstractRestServiceClientMaker INSTANCE = new AbstractRestServiceClientMaker() {};
    }

    // Singleton method
    public static AbstractRestServiceClientMaker getInstance()
    {
        return AbstractRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new AbstractResourceUrlBuilder(resourceBaseUrl) {};
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new AbstractHttpMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new AbstractRequestRetryPolicy() {};
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new AbstractClientCachePolicy() {};
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new AbstractCacheControlPolicy() {};
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new AbstractAutoRedirectPolicy() {};
    }


    @Override
    public String toString()
    {
        return "AbstractRestServiceClientMaker []";
    }



}
