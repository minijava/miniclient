package org.miniclient.maker.impl.base;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.impl.base.DefaultApiServiceClient;
import org.miniclient.maker.ApiUserClientMaker;


// Default factory.
public final class DefaultApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultApiUserClientMaker.class.getName());


    protected DefaultApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class DefaultApiUserClientMakerHolder
    {
        private static final DefaultApiUserClientMaker INSTANCE = new DefaultApiUserClientMaker();
    }

    // Singleton method
    public static DefaultApiUserClientMaker getInstance()
    {
        return DefaultApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new DefaultApiServiceClient(resourceBaseUrl);
    }

    
}
