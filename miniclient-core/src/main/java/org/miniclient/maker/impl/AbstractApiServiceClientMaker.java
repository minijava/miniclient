package org.miniclient.maker.impl;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.impl.AbstractAuthRefreshPolicy;
import org.miniclient.common.impl.AbstractAutoRedirectPolicy;
import org.miniclient.common.impl.AbstractCacheControlPolicy;
import org.miniclient.common.impl.AbstractClientCachePolicy;
import org.miniclient.common.impl.AbstractCrudMethodFilter;
import org.miniclient.common.impl.AbstractRequestRetryPolicy;
import org.miniclient.impl.AbstractDataAccessClient;
import org.miniclient.impl.AbstractRestServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;


// Abstract factory.
public abstract class AbstractApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractApiServiceClientMaker.class.getName());


    protected AbstractApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class AbstractApiServiceClientMakerHolder
    {
        private static final AbstractApiServiceClientMaker INSTANCE = new AbstractApiServiceClientMaker() {};
    }

    // Singleton method
    public static AbstractApiServiceClientMaker getInstance()
    {
        return AbstractApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new AbstractCrudMethodFilter() {};
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new AbstractDataAccessClient() {};
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new AbstractAuthRefreshPolicy() {};
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new AbstractRequestRetryPolicy() {};
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new AbstractClientCachePolicy() {};
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new AbstractCacheControlPolicy() {};
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new AbstractAutoRedirectPolicy() {};
    }


    @Override
    public String toString()
    {
        return "AbstractApiServiceClientMaker []";
    }


}
