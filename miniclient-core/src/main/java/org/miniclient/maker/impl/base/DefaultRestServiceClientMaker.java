package org.miniclient.maker.impl.base;

import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.common.impl.base.DefaultAuthRefreshPolicy;
import org.miniclient.common.impl.base.DefaultAutoRedirectPolicy;
import org.miniclient.common.impl.base.DefaultCacheControlPolicy;
import org.miniclient.common.impl.base.DefaultClientCachePolicy;
import org.miniclient.common.impl.base.DefaultHttpMethodFilter;
import org.miniclient.common.impl.base.DefaultRequestRetryPolicy;
import org.miniclient.common.impl.base.DefaultResourceUrlBuilder;
import org.miniclient.impl.base.DefaultDataAccessClient;
import org.miniclient.maker.RestServiceClientMaker;


// Default factory.
public final class DefaultRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultRestServiceClientMaker.class.getName());


    protected DefaultRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class DefaultRestServiceClientMakerHolder
    {
        private static final DefaultRestServiceClientMaker INSTANCE = new DefaultRestServiceClientMaker();
    }

    // Singleton method
    public static DefaultRestServiceClientMaker getInstance()
    {
        return DefaultRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new DefaultResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new DefaultHttpMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new DefaultDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new DefaultAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new DefaultRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new DefaultClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new DefaultCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new DefaultAutoRedirectPolicy();
    }


}
