package org.miniclient.maker.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.impl.AbstractApiServiceClient;
import org.miniclient.maker.ApiUserClientMaker;


// Abstract factory.
public abstract class AbstractApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(AbstractApiUserClientMaker.class.getName());


    protected AbstractApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class AbstractApiUserClientMakerHolder
    {
        private static final AbstractApiUserClientMaker INSTANCE = new AbstractApiUserClientMaker() {};
    }

    // Singleton method
    public static AbstractApiUserClientMaker getInstance()
    {
        return AbstractApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new AbstractApiServiceClient(resourceBaseUrl) {};
    }


    @Override
    public String toString()
    {
        return "AbstractApiUserClientMaker []";
    }


}
