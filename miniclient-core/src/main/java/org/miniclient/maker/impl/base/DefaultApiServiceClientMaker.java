package org.miniclient.maker.impl.base;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.impl.base.DefaultAuthRefreshPolicy;
import org.miniclient.common.impl.base.DefaultAutoRedirectPolicy;
import org.miniclient.common.impl.base.DefaultCacheControlPolicy;
import org.miniclient.common.impl.base.DefaultClientCachePolicy;
import org.miniclient.common.impl.base.DefaultCrudMethodFilter;
import org.miniclient.common.impl.base.DefaultRequestRetryPolicy;
import org.miniclient.impl.base.DefaultDataAccessClient;
import org.miniclient.impl.base.DefaultRestServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;


// Default factory.
public final class DefaultApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultApiServiceClientMaker.class.getName());


    protected DefaultApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class DefaultApiServiceClientMakerHolder
    {
        private static final DefaultApiServiceClientMaker INSTANCE = new DefaultApiServiceClientMaker();
    }

    // Singleton method
    public static DefaultApiServiceClientMaker getInstance()
    {
        return DefaultApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new DefaultRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new DefaultCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new DefaultDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new DefaultAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new DefaultRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new DefaultClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new DefaultCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new DefaultAutoRedirectPolicy();
    }

}
