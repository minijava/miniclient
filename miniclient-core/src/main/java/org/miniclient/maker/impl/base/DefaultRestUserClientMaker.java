package org.miniclient.maker.impl.base;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.impl.base.DefaultRestServiceClient;
import org.miniclient.maker.RestUserClientMaker;


// Default factory.
public final class DefaultRestUserClientMaker implements RestUserClientMaker
{
    private static final Logger log = Logger.getLogger(DefaultRestUserClientMaker.class.getName());

    
    protected DefaultRestUserClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class DefaultRestUserClientMakerHolder
    {
        private static final DefaultRestUserClientMaker INSTANCE = new DefaultRestUserClientMaker();
    }

    // Singleton method
    public static DefaultRestUserClientMaker getInstance()
    {
        return DefaultRestUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new DefaultRestServiceClient(resourceBaseUrl);
    }

}
