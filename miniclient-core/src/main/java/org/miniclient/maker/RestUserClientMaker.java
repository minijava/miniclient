package org.miniclient.maker;

import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;


public interface RestUserClientMaker
{
    // Factory method
    RestServiceClient makeServiceClient(String resourceBaseUrl);
    // RestServiceClient makeServiceClient(String parentResourceBaseUrl, String parentResourceId, String resourceName);

}
