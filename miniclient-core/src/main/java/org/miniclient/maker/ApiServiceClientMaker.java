package org.miniclient.maker;

import org.miniclient.RestServiceClient;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;


public interface ApiServiceClientMaker extends ClientPolicyMaker
{
    // Factory methods

    RestServiceClient makeRestClient(String resourceBaseUrl);
    // RestServiceClient makeRestClient(String parentResourceBaseUrl, String parentResourceId, String resourceName);

    CrudMethodFilter makeCrudMethodFilter();
    DataAccessClient makeDataAccessClient();

}
