package org.miniclient;


// This has this notorious "diamond" shape inheritance.
// Note that we use the "api" version of the implementation over that of "rest" version.
public interface RestApiServiceClient extends RestServiceClient, ApiServiceClient
{

}
