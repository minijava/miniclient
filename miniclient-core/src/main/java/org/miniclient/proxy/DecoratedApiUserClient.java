package org.miniclient.proxy;

import org.miniclient.ApiUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedApiUserClient extends ApiUserClient
{
    // No need for API to return the decorated client.
    // ApiUserClient getDecoratedClient();
}
