package org.miniclient.proxy;

import org.miniclient.RestApiServiceClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedRestApiServiceClient extends RestApiServiceClient
{
    // No need for API to return the decorated client.
    // RestApiServiceClient getDecoratedClient();
}
