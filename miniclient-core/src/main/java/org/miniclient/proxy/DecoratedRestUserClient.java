package org.miniclient.proxy;

import org.miniclient.RestUserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedRestUserClient extends RestUserClient
{
    // No need for API to return the decorated client.
    // RestUserClient getDecoratedClient();
}
