// Copyright (c) 2013 Harry Y.
// Permission is hereby granted to any person obtaining a copy of this software 
// to deal in the software without restriction subject to the following conditions: 
// "The software shall be used for good, not evil."
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. 

/**
 * MiniClient classes adopting the "decorator pattern".
 * These classes can be used as a simple wrapper around more complex implmentation classes.
 */
package org.miniclient.proxy;
