package org.miniclient.proxy;

import org.miniclient.UserClient;


// "Marker interface" for "proxy" or "mock" clients. 
public interface DecoratedUserClient extends UserClient
{
    // No need for API to return the decorated client.
    // UserClient getDecoratedClient();
}
