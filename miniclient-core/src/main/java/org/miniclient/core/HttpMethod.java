package org.miniclient.core;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


// temporary
public final class HttpMethod
{
    private static final Logger log = Logger.getLogger(HttpMethod.class.getName());

    // Note the all caps.
    public static final String GET = "GET";
    public static final String POST = "POST";
    public static final String PUT = "PUT";
    public static final String PATCH = "PATCH";
    public static final String DELETE = "DELETE";
    public static final String HEAD = "HEAD";
    public static final String OPTIONS = "OPTIONS";
    // etc...

    private static Set<String> methodSet;
    static {
        methodSet = new HashSet<String>();
        methodSet.add(HttpMethod.GET);
        methodSet.add(HttpMethod.POST);
        methodSet.add(HttpMethod.PUT);
        methodSet.add(HttpMethod.DELETE);
    }

    private HttpMethod() {}


    // Returns true if methodName is a valid HTTP method name.
    public static boolean isValid(String methodName)
    {
        if(methodName == null) {
            return false;
        }
        // ??? Should we "normalize" (e.g., capitalize) the name ??
        return methodSet.contains(methodName.toUpperCase());
    }

    
}
