package org.miniclient.core;

import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;


// temporary
public final class CrudMethod
{
    private static final Logger log = Logger.getLogger(CrudMethod.class.getName());

    // Note the all lower cases.
    public static final String GET_ITEM = "get.item";
    public static final String GET_LIST = "get.list";
    public static final String GET_KEYS = "get.keys";
    public static final String CREATE_ITEM = "create.item";
    public static final String UPDATE_ITEM = "update.item";
    public static final String MODIFY_ITEM = "modify.item";   // Partial update
    public static final String DELETE_ITEM = "delete.item";
    public static final String DELETE_LIST = "delete.list";
    // etc...

    private static Set<String> methodSet;
    static {
        methodSet = new HashSet<String>();
        methodSet.add(CrudMethod.GET_ITEM);
        methodSet.add(CrudMethod.GET_LIST);
        methodSet.add(CrudMethod.GET_KEYS);
        methodSet.add(CrudMethod.CREATE_ITEM);
        methodSet.add(CrudMethod.UPDATE_ITEM);
        methodSet.add(CrudMethod.MODIFY_ITEM);
        methodSet.add(CrudMethod.DELETE_ITEM);
        methodSet.add(CrudMethod.DELETE_LIST);
    }

    private CrudMethod() {}


    // Returns true if methodName is a valid HTTP method name.
    public static boolean isValid(String methodName)
    {
        if(methodName == null) {
            return false;
        }
        // ??? Should we "normalize" (e.g., to lowever case) the name ??
        return methodSet.contains(methodName.toLowerCase());
    }


    
}
