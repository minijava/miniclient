package org.miniclient.core;

import java.util.logging.Logger;


/**
 * Note that "OAuth" throughout this library always means OAuth v1 (version 1.0a).
 * (The newer version of OAuth is specifically referred as OAuth2.)
 */
public final class AuthMethod
{
    private static final Logger log = Logger.getLogger(AuthMethod.class.getName());

    // ???
    public static final String NONE = "None";
    public static final String BASIC = "Basic";
    public static final String DIGEST = "Digest";
    public static final String OAUTH = "OAuth";   // OAuth1
    // public static final String OAUTH2 = "OAuth2";   // Use "OAuth2" or "Bearer"?
    public static final String BEARER = "Bearer";   // OAuth2 token type ???
    // ..
    public static final String TWOLEGGED = "TwoLegged";   // OAuth, client key based. (aka 2-LO, or application-auth, etc.)
    // etc...

    private AuthMethod() {}

    
}
