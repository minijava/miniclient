package org.miniclient.core;

import java.util.logging.Logger;



// Different Web services return a "list" output in a number of different ways:
// (1) As a list.
// (2) As an object/hash, with the list as a field value (with the field name, "items", "list", etc.)
public final class ListResponseType
{
    private static final Logger log = Logger.getLogger(ListResponseType.class.getName());

    // Only two types, for now.
    public static final String TYPE_LIST = "list";
    public static final String TYPE_MAP = "map";
    
    // ListResponseType is "Immutable".
    public static final ListResponseType LIST = new ListResponseType(TYPE_LIST);
    public static final ListResponseType MAP_ITEMS = new ListResponseType(TYPE_MAP, "items");
    public static final ListResponseType MAP_LIST = new ListResponseType(TYPE_MAP, "list");
    // etc..    

    // "list" or "map"
    private final String type;
    
    // This is used only if type=="map"
    private final String listKey;
    

    public ListResponseType()
    {
        this(TYPE_LIST);  // ???
    }
    public ListResponseType(String type)
    {
        this(type, null);  // ???
    }
    public ListResponseType(String type, String listKey)
    {
        this.type = type;
        this.listKey = listKey;
    }

    public String getType()
    {
        return type;
    }
    public String getListKey()
    {
        return listKey;
    }

    public static ListResponseType getDefaultListResponseType()
    {
        return LIST;
    }

    @Override
    public int hashCode()
    {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((listKey == null) ? 0 : listKey.hashCode());
        result = prime * result + ((type == null) ? 0 : type.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj)
    {
        if (this == obj)
            return true;
        if (obj == null)
            return false;
        if (getClass() != obj.getClass())
            return false;
        ListResponseType other = (ListResponseType) obj;
        if (listKey == null) {
            if (other.listKey != null)
                return false;
        } else if (!listKey.equals(other.listKey))
            return false;
        if (type == null) {
            if (other.type != null)
                return false;
        } else if (!type.equals(other.type))
            return false;
        return true;
    }

    
}
