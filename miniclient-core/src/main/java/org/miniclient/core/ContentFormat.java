package org.miniclient.core;

import java.util.logging.Level;
import java.util.logging.Logger;


// Mime type definitions.
public final class ContentFormat
{
    private static final Logger log = Logger.getLogger(ContentFormat.class.getName());

    // ???
    // Note: All caps.
    // Some are applicable to both request and response.
    // And, some are only applicable to either request or response, but not both.
    public static final String TEXT = "TEXT";     // Normally, for response only.
    public static final String HTML = "HTML";     // Normally, for response only.
    public static final String XML = "XML";
    public static final String JSON = "JSON";
    public static final String QUERY = "QUERY";   // Urlencoded query string. For request only.
    public static final String FORM = "FORM";     // multipart form data. For request only.
    // etc...
    
    // temporary
    public static final String DEFAULT_CHARSET = "utf-8";
    
    private ContentFormat() {}

    
    public static boolean isValid(String format)
    {
        
        return false;
    }
    
    // Returns the default mime type for the given "format".
    public static String getCotentType(String format)
    {
        return getCotentType(format, true);
    }
    public static String getCotentType(String format, boolean includeCharset)
    {
        if(includeCharset) {
            return getCotentType(format, DEFAULT_CHARSET);
        } else {
            return getCotentType(format, null);
        }
    }
    public static String getCotentType(String format, String charset)
    {
        String contentType = null;
        // switch(format) {
        // case TEXT:
        if(format.equals(TEXT)) {
            contentType = "text/plain";
        //     break;
        // case HTML:
        } else if(format.equals(HTML)) {
            contentType = "text/html";
        //     break;
        // case XML:
        } else if(format.equals(XML)) {
            contentType = "application/xml";
        //     break;
        // case JSON:
        } else if(format.equals(JSON)) {
            contentType = "application/json";
        //     break;
        // case QUERY:
        } else if(format.equals(QUERY)) {
            contentType = "application/x-www-form-urlencoded";
        //     break;
        // case FORM:
        } else if(format.equals(FORM)) {
            contentType = "multipart/form-data";
        //     break;
        // default:
        } else {
            // ???
        }
        if(contentType != null && charset != null) {
            contentType += "; charset=" + charset;
        }
        return contentType;
    }
    
    
    // temporary
    // The main purpose of this function is to get the "ContentFormat" (our string constant)
    //      from the response contentType header.
    public static String getContentFormat(String contentType)
    {
        if(contentType == null || contentType.isEmpty()) {
            // return contentType;   // ???
            return null;
        }
        if(contentType.startsWith("application/json")) {
            return JSON;
        } else if(contentType.startsWith("application/xml")) {
            return XML;
        } else if(contentType.startsWith("text/plain")) {
            return TEXT;
        } else if(contentType.startsWith("text/html")) {
            return HTML;
        } else {
            // ???
            if(log.isLoggable(Level.INFO)) log.info("Unrecognized contentType = " + contentType);
            return null;   // ???
        }
    }
    
    
}
