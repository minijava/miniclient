package org.miniclient;


/**
 * "Top most" interface in the REST/API - Service/User client class hierarchy.
 * The "resource" refers to the web resource (of a Web service).
 */
public interface ResourceClient extends WebClient
{
    /**
     * Return the ResourceBaseUrl of this "resource".
     * @return ResourceBaseUrl
     */
    String getResourceBaseUrl();
}
