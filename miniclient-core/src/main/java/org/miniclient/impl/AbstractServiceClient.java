package org.miniclient.impl;

import java.util.logging.Logger;

import org.miniclient.FlexibleServiceClient;
import org.miniclient.credential.ClientCredential;


public abstract class AbstractServiceClient implements FlexibleServiceClient, ClientCredential
{
    private static final Logger log = Logger.getLogger(AbstractServiceClient.class.getName());

    
    // Client auth credential.
    // This is primarily used for OAuth. (consumer key, and consumer secret)
    private ClientCredential clientCredential;

    
    public AbstractServiceClient()
    {
        this(null);
    }
    public AbstractServiceClient(ClientCredential clientCredential)
    {
        super();
        this.clientCredential = clientCredential;
    }


    @Override
    public String getClientKey()
    {
        return getClientCredential().getClientKey();
    }
    @Override
    public String getClientSecret()
    {
        return getClientCredential().getClientSecret();
    }


    @Override
    public ClientCredential getClientCredential()
    {
        return clientCredential;
    }
    @Override
    public void setClientCredential(ClientCredential clientCredential)
    {
        this.clientCredential = clientCredential;
    }

    
    @Override
    public String toString()
    {
        return "AbstractServiceClient [clientCredential=" + clientCredential
                + "]";
    }


}
