package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.impl.base.DefaultAuthRefreshPolicy;
import org.miniclient.credential.UserCredential;
import org.miniclient.impl.AbstractUserClient;


public final class DefaultUserClient extends AbstractUserClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultUserClient()
    {
        this(null);
    }

    public DefaultUserClient(UserCredential userCredential)
    {
        super(userCredential);
    }

    
    // Factory method.
    protected AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new DefaultAuthRefreshPolicy();
    }

}
