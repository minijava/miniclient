package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.core.ContentFormat;
import org.miniclient.credential.impl.AbstractUserCredential;
import org.miniclient.impl.AbstractRestServiceClient;
import org.miniclient.maker.RestServiceClientMaker;
import org.miniclient.maker.impl.AbstractRestServiceClientMaker;
import org.miniclient.maker.impl.base.DefaultRestServiceClientMaker;


// This is a "final" class.
// Use AbstractRestServiceClient as a base class if you need to subclass this.
public final class DefaultRestServiceClient extends AbstractRestServiceClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultRestServiceClient.class.getName());
    private static final long serialVersionUID = 1L;


    public DefaultRestServiceClient(String resourceBaseUrl)
    {
        super(resourceBaseUrl);        
    }
//    public DefaultRestServiceClient(ResourceUrlBuilder resourceUrlBuilder)
//    {
//        super(resourceUrlBuilder);
//    }

    @Override
    protected void init()
    {
        super.init();

        // Set the default values.
        setAuthCredentialRequired(false);  // ???
        super.setDefaultAuthCredential(new AbstractUserCredential() {});
        setRequestFormat(ContentFormat.JSON);
        setResponseFormat(ContentFormat.JSON);
        setTimeoutSeconds(10);   // ???
//        setFollowRedirect(false);
//        setMaxFollow(0);
        // ...
    }


    // Factory methods.

    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return DefaultRestServiceClientMaker.getInstance();
    }

    
    

}
