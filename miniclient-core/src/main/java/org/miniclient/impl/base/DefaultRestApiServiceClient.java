package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.credential.ClientCredential;
import org.miniclient.credential.UserCredential;
import org.miniclient.impl.AbstractRestApiServiceClient;


public final class DefaultRestApiServiceClient extends AbstractRestApiServiceClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultRestApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultRestApiServiceClient()
    {
        // TODO Auto-generated constructor stub
    }

}
