package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.impl.AbstractDataAccessClient;


public final class DefaultDataAccessClient extends AbstractDataAccessClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultDataAccessClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public DefaultDataAccessClient()
    {
        super();
    }

    
}
