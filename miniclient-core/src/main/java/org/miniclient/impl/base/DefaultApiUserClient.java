package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;
import org.miniclient.maker.impl.base.DefaultApiUserClientMaker;


public final class DefaultApiUserClient extends AbstractApiUserClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public DefaultApiUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public DefaultApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
    }

    public DefaultApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public DefaultApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }
    

    // Factory methods

    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return DefaultApiUserClientMaker.getInstance();
    }



}
