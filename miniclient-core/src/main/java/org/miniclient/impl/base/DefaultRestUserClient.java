package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.impl.AbstractRestUserClient;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.maker.impl.base.DefaultRestUserClientMaker;


public final class DefaultRestUserClient extends AbstractRestUserClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultRestUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public DefaultRestUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public DefaultRestUserClient(String resourceBaseUrl,  UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
    }

    public DefaultRestUserClient(RestServiceClient restServiceClient)
    {
        this(restServiceClient, null);
    }
    public DefaultRestUserClient(RestServiceClient restServiceClient,
            UserCredential userCredential)
    {
        super(restServiceClient, userCredential);
    }

    // Factory methods

    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return DefaultRestUserClientMaker.getInstance();
    }

}
