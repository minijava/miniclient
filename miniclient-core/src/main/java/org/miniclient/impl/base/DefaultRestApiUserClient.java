package org.miniclient.impl.base;

import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.RestApiServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.credential.UserCredential;
import org.miniclient.impl.AbstractRestApiUserClient;


public final class DefaultRestApiUserClient extends AbstractRestApiUserClient implements Serializable
{
    private static final Logger log = Logger.getLogger(DefaultRestApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public DefaultRestApiUserClient(RestApiServiceClient restApiServiceClient)
    {
        super(restApiServiceClient);
        // TODO Auto-generated constructor stub
    }

 

}
