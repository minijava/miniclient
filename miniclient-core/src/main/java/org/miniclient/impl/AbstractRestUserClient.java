package org.miniclient.impl;

import java.io.IOException;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.FlexibleResourceClient;
import org.miniclient.FlexibleUserClient;
import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.credential.UserCredential;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.maker.impl.AbstractRestUserClientMaker;


// User-specific REST Client.
// Actual implementation is delegated to service-specific REST client.
public abstract class AbstractRestUserClient implements RestUserClient, FlexibleResourceClient, FlexibleUserClient, ResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(AbstractRestUserClient.class.getName());

    // Embedded REST Client.
    private final RestServiceClient restServiceClient;
    
    // Abstract factory.
    private RestUserClientMaker restUserClientMaker;

    // AuthCredential contains userId as well as the user's auth token, etc.
    private UserCredential userCredential;

    
    public AbstractRestUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public AbstractRestUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super();

        // Abstract factory.
        restUserClientMaker = makeRestUserClientMaker();

        // TBD: Validation?
        // restServiceClient cannot be null.
        this.restServiceClient = makeServiceClient(resourceBaseUrl);

        // TBD:
        // Check required scopes vs. userCredential.dataScopes ???
        // ....
        this.userCredential = userCredential;

        // TBD:
        init();
    }

    public AbstractRestUserClient(RestServiceClient restServiceClient)
    {
        this(restServiceClient, null);
    }
    public AbstractRestUserClient(RestServiceClient restServiceClient, UserCredential userCredential)
    {
        super();

        // Abstract factory.
        restUserClientMaker = makeRestUserClientMaker();

        // TBD: Validation?
        // restServiceClient cannot be null.
        this.restServiceClient = restServiceClient;
        
        this.userCredential = userCredential;

        // TBD:
        init();
    }

    
//    public AbstractRestUserClient(String resourceBaseUrl)
//    {
//        this(new AbstractResourceUrlBuilder(resourceBaseUrl) {});
//    }
//    public AbstractRestUserClient(ResourceUrlBuilder resourceUrlBuilder) 
//    {
//        this((RestServiceClient) new AbstractRestServiceClient(resourceUrlBuilder) {});
//    }
//    public AbstractRestUserClient(RestServiceClient restServiceClient)
//    {
//        this(restServiceClient, null);
//    }
//    public AbstractRestUserClient(RestServiceClient restServiceClient,  UserCredential userCredential)
//    {
//        super();
//
//        // TBD: Validation?
//        // restServiceClient cannot be null.
//        this.restServiceClient = restServiceClient;
//        this.userCredential = userCredential;
//
//        // TBD:
//        init();
//    }
    
    protected void init()
    {
        // Place holder
    }


    // We use delegation.
    protected RestServiceClient getRestServiceClient()
    {
        return restServiceClient;
    }
    protected FlexibleResourceClient getFlexibleRestServiceClient()
    {
        return (FlexibleResourceClient) restServiceClient;
    }

    
    // Factory methods...

    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return createRestUserClientMaker();
    }
    private static RestUserClientMaker createRestUserClientMaker()
    {
        return AbstractRestUserClientMaker.getInstance();
    }

    protected RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return restUserClientMaker.makeServiceClient(resourceBaseUrl);
    }
    

    @Override
    public boolean isMethodSupported(String methodName)
    {
        return getRestServiceClient().isMethodSupported(methodName);
    }

    @Override
    public String getResourceBaseUrl()
    {
        return getRestServiceClient().getResourceBaseUrl();
    }
//    @Override
//    public void setResourceBaseUrl(String resourceBaseUrl)
//    {
//        getRestServiceClient().setResourceBaseUrl(resourceBaseUrl);        
//    }
    
    @Override
    public String getResourceGetUrl(String id, Map<String, Object> params)
    {
        return ((ResourceUrlBuilder) getRestServiceClient()).getResourceGetUrl(id, params);
    }
    @Override
    public String getResourcePostUrl()
    {
        return ((ResourceUrlBuilder) getRestServiceClient()).getResourcePostUrl();
    }
    @Override
    public String getResourcePutUrl(String id)
    {
        return ((ResourceUrlBuilder) getRestServiceClient()).getResourcePutUrl(id);
    }
    @Override
    public String getResourcePatchUrl(String id)
    {
        return ((ResourceUrlBuilder) getRestServiceClient()).getResourcePatchUrl(id);
    }
    @Override
    public String getResourceDeleteUrl(String id, Map<String, Object> params)
    {
        return ((ResourceUrlBuilder) getRestServiceClient()).getResourceDeleteUrl(id, params);
    }

    
    @Override
    public AuthRefreshPolicy getAuthRefreshPolicy()
    {
        return getRestServiceClient().getAuthRefreshPolicy();
    }
    @Override
    public void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy)
    {
        getFlexibleRestServiceClient().setAuthRefreshPolicy(authRefreshPolicy);
    }

    @Override
    public RequestRetryPolicy getRequestRetryPolicy()
    {
        return getRestServiceClient().getRequestRetryPolicy();
    }
    @Override
    public void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy)
    {
        getFlexibleRestServiceClient().setRequestRetryPolicy(requestRetryPolicy);
    }

    @Override
    public ClientCachePolicy getClientCachePolicy()
    {
        return getRestServiceClient().getClientCachePolicy();
    }
    @Override
    public void setClientCachePolicy(ClientCachePolicy clientCachePolicy)
    {
        getFlexibleRestServiceClient().setClientCachePolicy(clientCachePolicy);
    }

    @Override
    public CacheControlPolicy getCacheControlPolicy()
    {
        return getRestServiceClient().getCacheControlPolicy();
    }
    @Override
    public void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy)
    {
        getFlexibleRestServiceClient().setCacheControlPolicy(cacheControlPolicy);
    }


    @Override
    public AutoRedirectPolicy getAutoRedirectPolicy()
    {
        return getRestServiceClient().getAutoRedirectPolicy();
    }
    @Override
    public void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy)
    {
        getFlexibleRestServiceClient().setAutoRedirectPolicy(autoRedirectPolicy);
    }

    @Override
    public UserCredential getUserCredential()
    {
        return userCredential;
    }
    @Override
    public void setUserCredential(UserCredential userCredential)
    {
        // TBD:
        // Check required scopes vs. userCredential.dataScopes ???
        // ....
        this.userCredential = userCredential;
    }

    // temporary
    public boolean isAccessAllowed()
    {
        return getRestServiceClient().isAccessAllowed(userCredential);
    }
    // ...    
    
    
    @Override
    public Map<String, Object> get(String id, Map<String, Object> params) throws IOException
    {
        return getRestServiceClient().get(getUserCredential(), id, params);
    }

    @Override
    public Map<String, Object> post(Object inputData) throws IOException
    {
        return getRestServiceClient().post(getUserCredential(), inputData);
    }

    @Override
    public Map<String, Object> put(Object inputData, String id) throws IOException
    {
        return getRestServiceClient().put(getUserCredential(), inputData, id);
    }

    @Override
    public Map<String, Object> patch(Object partialData, String id) throws IOException
    {
        return getRestServiceClient().patch(getUserCredential(), partialData, id);
    }

    @Override
    public Map<String, Object> delete(String id, Map<String, Object> params) throws IOException
    {
        return getRestServiceClient().delete(getUserCredential(), id, params);
    }

    
    @Override
    public String toString()
    {
        return "AbstractRestUserClient [restServiceClient=" + restServiceClient
                + ", restUserClientMaker=" + restUserClientMaker
                + ", userCredential=" + userCredential + "]";
    }
    
}
