// Copyright (c) 2013 Harry Y.
// Permission is hereby granted to any person obtaining a copy of this software 
// to deal in the software without restriction subject to the following conditions: 
// "The software shall be used for good, not evil."
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. 

/**
 * MiniClient base class implementations.
 * These classes are all abstract, 
 * and they can be used as base classes for more specific implementations.
 */
package org.miniclient.impl;
