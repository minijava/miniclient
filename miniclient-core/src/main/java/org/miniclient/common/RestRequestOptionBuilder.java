package org.miniclient.common;


// Not being used.
public interface RestRequestOptionBuilder
{
    RestRequestOptionBuilder setMethod(String method);
    RestRequestOptionBuilder setConnectTimeout(int seconds);
    RestRequestOptionBuilder setHeader(String key, String value);
    RestRequestOptionBuilder removeHeader(String key);
    // etc...
    // ...
    RestRequestOption build();
}
