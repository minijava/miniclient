package org.miniclient.common;


// For "HTTP Cache Control".
public interface CacheControlPolicy extends WebClientPolicy
{
    // Cache-Control header.
    String getCachControl();
    
    // TBD:
    // etag, etc...
    // ...

}
