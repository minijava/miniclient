package org.miniclient.common;


public interface RequestRetryPolicy extends WebClientPolicy
{
    boolean isRetryIfFails();
    int getMaxRetryCount();
    
    
}
