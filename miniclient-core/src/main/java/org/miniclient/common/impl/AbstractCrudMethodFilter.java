package org.miniclient.common.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.core.CrudMethod;
import org.miniclient.core.HttpMethod;


public abstract class AbstractCrudMethodFilter extends AbstractMethodFilter implements CrudMethodFilter, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractCrudMethodFilter.class.getName());
    private static final long serialVersionUID = 1L;

    // We use either httpMethodFilter or super.methodSet, but not both.
    private final HttpMethodFilter httpMethodFilter;

    public AbstractCrudMethodFilter()
    {
        super();

        getMethodSet().add(CrudMethod.GET_ITEM);
        getMethodSet().add(CrudMethod.GET_LIST);
        getMethodSet().add(CrudMethod.GET_KEYS);
        getMethodSet().add(CrudMethod.CREATE_ITEM);
        getMethodSet().add(CrudMethod.UPDATE_ITEM);
        getMethodSet().add(CrudMethod.DELETE_ITEM);
        getMethodSet().add(CrudMethod.DELETE_LIST);

        this.httpMethodFilter = null;
    }

    public AbstractCrudMethodFilter(HttpMethodFilter httpMethodFilter)
    {
        // The httpMethodFilter arg cannot be null.
        this.httpMethodFilter = (httpMethodFilter != null) ? httpMethodFilter : new AbstractHttpMethodFilter() {};  // ???
    }

    @Override
    public boolean isMethodSupported(String methodName)
    {
        if(httpMethodFilter == null) {
            return super.isMethodSupported(methodName);            
        } else {
            // switch(methodName) {
            // case CrudMethod.GET_ITEM:
            // case CrudMethod.GET_LIST:
            // case CrudMethod.GET_KEYS:
            if(methodName.equals(CrudMethod.GET_ITEM) 
                || methodName.equals(CrudMethod.GET_LIST) 
                || methodName.equals(CrudMethod.GET_KEYS)) {
                return httpMethodFilter.isMethodSupported(HttpMethod.GET);
            // case CrudMethod.CREATE_ITEM:
            } else if(methodName.equals(CrudMethod.CREATE_ITEM) ) {
                return (httpMethodFilter.isMethodSupported(HttpMethod.POST) || httpMethodFilter.isMethodSupported(HttpMethod.PUT));
            // case CrudMethod.UPDATE_ITEM:
            } else if(methodName.equals(CrudMethod.UPDATE_ITEM) ) {
                return httpMethodFilter.isMethodSupported(HttpMethod.PUT);
            // case CrudMethod.DELETE_ITEM:
            // case CrudMethod.DELETE_LIST:
            } else if(methodName.equals(CrudMethod.DELETE_ITEM) 
                || methodName.equals(CrudMethod.DELETE_LIST)) {
                return httpMethodFilter.isMethodSupported(HttpMethod.DELETE);
            // default:
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean setMethodSupported(String methodName)
    {
        if(httpMethodFilter == null) {
            return super.setMethodSupported(methodName);
        } else {
            // switch(methodName) {
            // case CrudMethod.GET_ITEM:
            // case CrudMethod.GET_LIST:
            // case CrudMethod.GET_KEYS:
            if(methodName.equals(CrudMethod.GET_ITEM) 
                || methodName.equals(CrudMethod.GET_LIST) 
                || methodName.equals(CrudMethod.GET_KEYS)) {
                return ((AbstractHttpMethodFilter) httpMethodFilter).setMethodSupported(HttpMethod.GET);
            // case CrudMethod.CREATE_ITEM:
            } else if(methodName.equals(CrudMethod.CREATE_ITEM)) {
                return (((AbstractHttpMethodFilter) httpMethodFilter).setMethodSupported(HttpMethod.POST) || ((AbstractHttpMethodFilter) httpMethodFilter).setMethodSupported(HttpMethod.PUT));
            // case CrudMethod.UPDATE_ITEM:
            } else if(methodName.equals(CrudMethod.UPDATE_ITEM)) {
                return ((AbstractHttpMethodFilter) httpMethodFilter).setMethodSupported(HttpMethod.PUT);
            // case CrudMethod.DELETE_ITEM:
            // case CrudMethod.DELETE_LIST:
            } else if(methodName.equals(CrudMethod.DELETE_ITEM)) {
                return ((AbstractHttpMethodFilter) httpMethodFilter).setMethodSupported(HttpMethod.DELETE);
            // default:
            } else {
                return false;
            }
        }
    }

    @Override
    public boolean setMethodUnsupported(String methodName)
    {
        if(httpMethodFilter == null) {
            return super.setMethodUnsupported(methodName);
        } else {
            // switch(methodName) {
            // case CrudMethod.GET_ITEM:
            // case CrudMethod.GET_LIST:
            // case CrudMethod.GET_KEYS:
            if(methodName.equals(CrudMethod.GET_ITEM) 
                || methodName.equals(CrudMethod.GET_LIST) 
                || methodName.equals(CrudMethod.GET_KEYS)) {
                return ((AbstractHttpMethodFilter) httpMethodFilter).setMethodUnsupported(HttpMethod.GET);
            // case CrudMethod.CREATE_ITEM:
            } else if(methodName.equals(CrudMethod.CREATE_ITEM)) {
                return (((AbstractHttpMethodFilter) httpMethodFilter).setMethodUnsupported(HttpMethod.POST) || ((AbstractHttpMethodFilter) httpMethodFilter).setMethodUnsupported(HttpMethod.PUT));
            // case CrudMethod.UPDATE_ITEM:
            } else if(methodName.equals(CrudMethod.UPDATE_ITEM)) {
                return ((AbstractHttpMethodFilter) httpMethodFilter).setMethodUnsupported(HttpMethod.PUT);
            // case CrudMethod.DELETE_ITEM:
            // case CrudMethod.DELETE_LIST:
            } else if(methodName.equals(CrudMethod.DELETE_ITEM)) { 
                return ((AbstractHttpMethodFilter) httpMethodFilter).setMethodUnsupported(HttpMethod.DELETE);
            // default:
            } else {
                return false;
            }
        }
    }


    @Override
    public String toString()
    {
        return "AbstractCrudMethodFilter [httpMethodFilter=" + httpMethodFilter
                + "]";
    }

    
}
