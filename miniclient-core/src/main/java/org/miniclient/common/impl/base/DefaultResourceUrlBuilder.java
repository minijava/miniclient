package org.miniclient.common.impl.base;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractResourceUrlBuilder;


// Default implementation.
public final class DefaultResourceUrlBuilder extends AbstractResourceUrlBuilder
{
    private static final Logger log = Logger.getLogger(DefaultResourceUrlBuilder.class.getName());

    public DefaultResourceUrlBuilder(String resourceBaseUrl)
    {
        super(resourceBaseUrl);
    }


}
