package org.miniclient.common.impl.base;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractUserAuthRefreshHandler;


public final class DefaultUserAuthRefreshHandler extends AbstractUserAuthRefreshHandler
{
    private static final Logger log = Logger.getLogger(DefaultUserAuthRefreshHandler.class.getName());
    private static final long serialVersionUID = 1L;


    public DefaultUserAuthRefreshHandler()
    {
    }

    
}
