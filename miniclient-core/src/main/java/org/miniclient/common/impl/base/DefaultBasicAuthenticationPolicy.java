package org.miniclient.common.impl.base;

import java.net.URI;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractAuthenticationtPolicy;
import org.miniclient.credential.AuthCredential;


public final class DefaultBasicAuthenticationPolicy extends AbstractAuthenticationtPolicy
{
    private static final Logger log = Logger.getLogger(DefaultBasicAuthenticationPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultBasicAuthenticationPolicy(String authMethod, String authTransmissionType)
    {
        super(authMethod, authTransmissionType);
    }

    @Override
    public String generateAuthorizationString(AuthCredential authCredential, String httpMethod, URI baseURI, Map<String, String[]> requestParams)
    {
        // TBD:
        return null;
    }

    

}
