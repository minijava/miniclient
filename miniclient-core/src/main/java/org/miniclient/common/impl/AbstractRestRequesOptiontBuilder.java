package org.miniclient.common.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.common.RestRequestOption;
import org.miniclient.common.RestRequestOptionBuilder;


// Base implementation.
public abstract class AbstractRestRequesOptiontBuilder implements RestRequestOptionBuilder, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractRestRequesOptiontBuilder.class.getName());
    private static final long serialVersionUID = 1L;

    private String method;
    private int connectTimeout;
    private Map<String,String> headerMap;
    
    public AbstractRestRequesOptiontBuilder()
    {
        headerMap = new HashMap<String,String>();

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }

    @Override
    public RestRequestOptionBuilder setMethod(String method)
    {
        this.method = method;
        return this;
    }

    @Override
    public RestRequestOptionBuilder setConnectTimeout(int seconds)
    {
        this.connectTimeout = seconds;
        return this;
    }

    @Override
    public RestRequestOptionBuilder setHeader(String key, String value)
    {
        headerMap.put(key, value);
        return this;
    }

    @Override
    public RestRequestOptionBuilder removeHeader(String key)
    {
        headerMap.remove(key);
        return this;
    }

    @Override
    public RestRequestOption build()
    {
        // TBD:
        return null;
    }


    @Override
    public String toString()
    {
        return "AbstractRestRequesOptiontBuilder [method=" + method
                + ", connectTimeout=" + connectTimeout + ", headerMap="
                + headerMap + "]";
    }


}
