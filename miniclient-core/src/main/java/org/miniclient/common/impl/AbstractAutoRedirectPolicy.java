package org.miniclient.common.impl;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Logger;

import org.miniclient.common.AutoRedirectPolicy;


public abstract class AbstractAutoRedirectPolicy implements AutoRedirectPolicy, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    private boolean autoFollowPrimaryChoice;
    private boolean autoRedirect;
    private Map<Integer,Boolean> autoFollowRedirectionMap;
    
    public AbstractAutoRedirectPolicy()
    {
        autoFollowPrimaryChoice = false;
        autoRedirect = false;
        autoFollowRedirectionMap = new HashMap<Integer,Boolean>();

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }

    @Override
    public boolean isAutoFollowPrimaryChoice()
    {
        return autoFollowPrimaryChoice;
    }

    @Override
    public boolean isAutoRedirect()
    {
        return autoRedirect;
    }

    @Override
    public boolean isAutoFollowRedirection(int statusCode)
    {
        if(autoFollowRedirectionMap.containsKey(statusCode)) {
            return autoFollowRedirectionMap.get(statusCode);
        }
        return false;
    }


    @Override
    public String toString()
    {
        return "AbstractAutoRedirectPolicy [autoFollowPrimaryChoice="
                + autoFollowPrimaryChoice + ", autoRedirect=" + autoRedirect
                + ", autoFollowRedirectionMap=" + autoFollowRedirectionMap
                + "]";
    }

}
