package org.miniclient.common.impl;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.common.RequestRetryPolicy;


// Web services call retry policy
public  class AbstractRequestRetryPolicy implements RequestRetryPolicy, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractRequestRetryPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    private boolean retryIfFails;
    private int maxRetryCount;


    public AbstractRequestRetryPolicy() 
    {
        

        // TBD:
        init();
    }
    
    protected void init()
    {
        // Place holder
    }


    @Override
    public boolean isRetryIfFails()
    {
        return retryIfFails;
    }
    public void setRetryIfFails(boolean retryIfFails)
    {
        this.retryIfFails = retryIfFails;
    }

    @Override
    public int getMaxRetryCount()
    {
        return maxRetryCount;
    }
    public void setMaxRetryCount(int maxRetryCount)
    {
        this.maxRetryCount = maxRetryCount;
    }

    
    @Override
    public String toString()
    {
        return "AbstractRequestRetryPolicy [retryIfFails=" + retryIfFails
                + ", maxRetryCount=" + maxRetryCount + "]";
    }    

    
}
