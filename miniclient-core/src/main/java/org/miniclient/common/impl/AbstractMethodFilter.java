package org.miniclient.common.impl;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Logger;

import org.miniclient.common.MethodFilter;
import org.miniclient.core.CrudMethod;


public abstract class AbstractMethodFilter implements MethodFilter, Serializable
{
    private static final Logger log = Logger.getLogger(AbstractMethodFilter.class.getName());
    private static final long serialVersionUID = 1L;

    private Set<String> methodSet;

    public AbstractMethodFilter()
    {
        methodSet = new HashSet<String>();
    }

    protected Set<String> getMethodSet()
    {
        if(methodSet == null) {
            methodSet = new HashSet<String>();
        }
        return methodSet;
    }

    @Override
    public boolean isMethodSupported(String methodName)
    {
        return methodSet.contains(methodName);
    }

    public boolean setMethodSupported(String methodName)
    {
        // TBD: Validate methodName?
        return methodSet.add(methodName);
    }

    public boolean setMethodUnsupported(String methodName)
    {
        // TBD: Validate methodName?
        return methodSet.remove(methodName);
    }


    @Override
    public String toString()
    {
        return "AbstractMethodFilter [methodSet=" + methodSet + "]";
    }

}
