package org.miniclient.common;

import java.util.Map;


/**
 * Defines a "rule" to create a set of URLs (for GET, POST, PUT, DELETE).
 * 
 * TBD: Some Web service require a "suffix" to indicate the desired response format.
 *      (e.g., http://twitter/status/update/123.json)
 *      How to support this without making the API overly complex???
 */
public interface ResourceUrlBuilder
{
    // ???
//    String getParentResourceBaseUrl();
//    String getParentResourceId();
//    String getResourceName();

    // ???
    // String getSuffix();


    /**
     * Returns the Web service URL corresponding to the given resource.
     * @return Resource base URL.
     */
    String getResourceBaseUrl();
    
    // ???
    // See the note above...
    // String getResponseFormat();
    // Or,
    // String getUrlSuffix();
    

    // This is better. But, some programming languages do not support function overloading.
    // String getResourceGetUrl(String id);
    // String getResourceGetUrl(Map<String,Object> params);
    /**
     * Both id and params cannot be null.
     * If id is specified, params is ignored. It returns the URL corresponding to the resource specified by id.
     * If params is specified. It returns the URL corresponding to the collection of resources specified by the params.
     * If neither id nor params is specified, then it's an error.
     * @param id Resource id. 
     * @param params URL query parameters to select a collection.
     * @return Returns URL for a single resource or a collection.
     */
    String getResourceGetUrl(String id, Map<String,Object> params);

    /**
     * Returns the URL for POST'ing to the resource.
     * @return the resource URL for posting.
     */
    String getResourcePostUrl();

    /**
     * Returns the URL for PUT'ing to the resource specified by the id.
     * @param id Resource id. 
     * @return the resource URL for PUT'ing.
     */
    String getResourcePutUrl(String id);

    /**
     * Returns the URL for PUT'ing to the resource specified by the id.
     * @param id Resource id. 
     * @return the resource URL for PUT'ing.
     */
    String getResourcePatchUrl(String id);

    // This is better. But, some programming languages do not support function overloading.
    // String getResourceDeleteUrl(String id);
    // String getResourceDeleteUrl(Map<String,Object> params);
    /**
     * Both id and params cannot be null.
     * If id is specified, params is ignored. It returns the URL for deleting the resource specified by id.
     * If params is specified. It returns the URL for deleting the collection of resources specified by the params.
     * If neither id nor params is specified, then it's an error.
     * @param id Resource id. 
     * @param params URL query parameters to select a collection.
     * @return Returns URL for deleting a single resource or a collection or resources.
     */
    String getResourceDeleteUrl(String id, Map<String,Object> params);
}
