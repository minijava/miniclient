package org.miniclient.common;

import java.util.Set;

import org.miniclient.credential.DataAccessCredential;


public interface DataAccessClient
{
    // TBD:
    Set<String> getRequiredScopes();
    boolean requiresScope(String scope);
    boolean isAccessAllowed(DataAccessCredential dataAccessCredential);
    // ....
}
