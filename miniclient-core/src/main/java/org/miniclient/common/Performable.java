package org.miniclient.common;

import org.miniclient.RestApiException;


// TBD:
// The intention is to make a "policy" type a "strategy" (as in strategy pattern). 
public interface Performable
{
    /**
     * Returns false if perform() is not implemented or otherwise it cannot be called.
     * Instead of calling perform() and catching exception,
     *     the client can call this method first to see if perform() can be called.
     * 
     * @return
     */
    boolean isPerformable();

    /*
     * Perform the target action (whatever that is) according to the policy.
     */
    // boolean perform();
    // ???
    Object perform(Object... args) throws RestApiException;

}
