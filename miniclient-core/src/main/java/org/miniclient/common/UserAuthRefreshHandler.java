package org.miniclient.common;

import org.miniclient.credential.UserCredential;


public interface UserAuthRefreshHandler extends AuthRefreshHandler   // , Performable ???
{
    // Returns true if the refresh handler is "real".
    boolean isImeplemented();
    
    // returns the updated authCredential.
    // TBD: how to return suc/fail status of the refresh ????
    // --> Use the AuthCredential.refreshedTime before and after the call.
    UserCredential refreshAuthToken(UserCredential userCredential);

}
