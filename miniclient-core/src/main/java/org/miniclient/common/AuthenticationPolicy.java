package org.miniclient.common;

import java.net.URI;
import java.util.Map;

import org.miniclient.credential.AuthCredential;


// ???
// Not being used...
public interface AuthenticationPolicy extends WebClientPolicy    // Performable
{
    // Basic, Digest, OAuth, 2LO, OAuth2, etc... 
    String getAuthMethod();
    
    // header, url-encoded single part form, or query string. 
    // For now, just use header ????
    String getAuthTransmissionType();
    
    // The auth string after "Authorization: ".
    //  or, part of the query string, url-encoded form string, depending on the auth transmission type.
    // request params contain all auth params (regardless of where (header, query, form) they are)
    //    as well as all query params (and single part url-encoded form params?).
    // requestParams should contain necessary auth credentials as well (relevant to given auth method)????
    //    --> Or, just use AuthCredential
    String generateAuthorizationString(AuthCredential authCredential, String httpMethod, URI baseURI, Map<String,String[]> requestParams);

}
