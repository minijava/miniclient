package org.miniclient.common;


// This is different from HTTP Cache Control.
public interface ClientCachePolicy extends WebClientPolicy
{
    
    boolean isCacheEnabled();
    int getCacheLifetime();
    // etc...
    
    Object get(String id);
    boolean put(String id, Object object);
    boolean clear(String id);

}
