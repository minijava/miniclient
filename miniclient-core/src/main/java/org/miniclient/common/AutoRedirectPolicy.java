package org.miniclient.common;


public interface AutoRedirectPolicy extends WebClientPolicy // , Performable  ???
{
    // Applicable only if the response is 300 and it contains location field.
    boolean isAutoFollowPrimaryChoice();

    // Applicable only if the request is GET,
    //    and if the response is 302.
    boolean isAutoRedirect();

    // for 301, 303, and 307, and as well as for 302 (if not get)
    boolean isAutoFollowRedirection(int statusCode);
    
}
