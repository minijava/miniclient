package org.miniclient.util;

import java.io.UnsupportedEncodingException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Formatter;
import java.util.Random;
import java.util.logging.Logger;
import java.util.logging.Level;

 
public class CommonUtil 
{
    private static final Logger log = Logger.getLogger(CommonUtil.class.getName()); 
    private static Random sRNG = null;
    private static Random getRandom()
    {
        if(sRNG == null) {
            sRNG = new Random();
        }
        return sRNG;
    }

    private CommonUtil() {}

    private static String convertToHex(byte[] data)
    {
        StringBuffer buf = new StringBuffer();
        for (int i = 0; i < data.length; i++) {
            int halfbyte = (data[i] >>> 4) & 0x0F;
            int two_halfs = 0;
            do {
                if ((0 <= halfbyte) && (halfbyte <= 9)) {
                    buf.append((char) ('0' + halfbyte));
                } else {
                    buf.append((char) ('a' + (halfbyte - 10)));
                }
                halfbyte = data[i] & 0x0F;
            } while(two_halfs++ < 1);
        }
        return buf.toString();
    }

    public static String MD2(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        return messageDigest(text, "MD2");
    }
    public static String MD5(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        return messageDigest(text, "MD5");
    }
    public static String SHA1(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        return messageDigest(text, "SHA-1");
    }
    public static String SHA256(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        return messageDigest(text, "SHA-256");
    }
    public static String SHA384(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        return messageDigest(text, "SHA-384");
    }
    public static String SHA512(String text) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        return messageDigest(text, "SHA-512");
    }

    public static String messageDigest(String text, String algorithm) 
        throws NoSuchAlgorithmException, UnsupportedEncodingException  
    {
        MessageDigest md = MessageDigest.getInstance(algorithm);
        // md.update(text.getBytes("iso-8859-1"), 0, text.length());
        md.update(text.getBytes("UTF-8"), 0, text.length());
//        // SHA-512 --> 64 bytes / 128 hex numbers.
//        byte[] sha1hash = new byte[64];
//        sha1hash = md.digest();
        byte[] sha1hash = md.digest();
        return convertToHex(sha1hash);
    }
  
}
