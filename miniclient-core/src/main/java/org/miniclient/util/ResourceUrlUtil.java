package org.miniclient.util;

import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;


public class ResourceUrlUtil
{
    private static final Logger log = Logger.getLogger(ResourceUrlUtil.class.getName());

    // Static methods only.
    private ResourceUrlUtil() {}


    // The logic building url is somewhat convoluted, but a few notes.
    // We do not insist that the url should or should not end with "/" 
    //     (although it may make more sense to end a collection URL with "/" and an entity URL without "/")
    // but, if there is a suffix, then we remove the trailing "/", if any, before the suffix. 
    public static String buildUrl(String baseUrl, String id)
    {
        return buildUrl(baseUrl, id, null);
    }
    public static String buildUrl(String baseUrl, String id, String suffix)
    {
        return buildUrl(baseUrl, id, suffix, null);
    }
    public static String buildUrl(String baseUrl, String id, String suffix, Map<String, Object> params)
    {
        String resourceUrl = null;
        if(baseUrl == null) {
            //return null;
            resourceUrl = "";  // Use relative URL????
        } else {
            resourceUrl = baseUrl;
        }

        if(id != null && !id.isEmpty()) {
            String idSeg = null;
            if(resourceUrl.endsWith("/")) {
                if(id.startsWith("/")) {
                    if(id.length() > 1) {
                        idSeg = id.substring(1);
                    }
                } else {
                    idSeg = id;
                }
            } else {
                if(id.startsWith("/")) {
                    idSeg = id;
                } else {
                    idSeg = "/" + id;
                }
            }
            if(idSeg != null && !idSeg.isEmpty()) {
                resourceUrl += idSeg;
            }
        }
        if(suffix != null && !suffix.isEmpty()) {
            String suffixSeg = null;
            if(suffix.startsWith(".")) {
                if(suffix.length() > 1) {
                    suffixSeg = suffix.substring(1);
                }
            } else {
                suffixSeg = suffix;
            }
            if(suffixSeg != null && !suffixSeg.isEmpty()) {
                if(resourceUrl.endsWith("/")) {
                    // ????
                    resourceUrl = resourceUrl.substring(0, resourceUrl.length());
                }
                resourceUrl += "." + suffixSeg;
            }
        }

        String url = buildUrl(resourceUrl, params);
        return url;
    }

    public static String buildUrl(String resourceUrl, Map<String, Object> params)
    {
        if(resourceUrl == null) {
            log.info("resourceUrl is null. Using an empty base.");
            resourceUrl = "";  // ???
        }
        if(params != null && !params.isEmpty()) {
            resourceUrl += "?";
            resourceUrl += buildQueryString(params);
        }
        return resourceUrl;
    }

    // We (currently) only support two types of params
    // (1) Map<String,Object> where Object is a String or "primitive" types
    // (2) Map<String,List<String>).
    public static String buildQueryString(Map<String, Object> params)
    {
        if(params == null) {
            return null;
        } else if(params.isEmpty()) {
            return "";  // ???
        } else {
            StringBuilder sb = new StringBuilder();
            for(String key : params.keySet()) {
                String encodedKey = null;
                try {
                    encodedKey = URLEncoder.encode(key, "UTF-8");
                } catch (UnsupportedEncodingException e) {
                    if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to URL encode the param key = " + key, e);
                }
                if(encodedKey != null) {
                    sb.append(encodedKey);
                    Object val = params.get(key);
                    if(val != null) {
                        String encodedVal = null;
                        if(val instanceof Boolean
                                || val instanceof Byte
                                || val instanceof Short 
                                || val instanceof Integer
                                || val instanceof Long
                                || val instanceof Float 
                                || val instanceof Double 
                                ) {
                            // Just use the numeric value.
                            if(val instanceof Boolean) {
                                encodedVal = Boolean.toString((Boolean) val);
                            } else if(val instanceof Byte) {
                                encodedVal = Byte.toString((Byte) val);
                            } else if(val instanceof Short) {
                                encodedVal = Short.toString((Short) val);
                            } else if(val instanceof Integer) {
                                encodedVal = Integer.toString((Integer) val);
                            } else if(val instanceof Long) {
                                encodedVal = Long.toString((Long) val);
                            } else if(val instanceof Float) {
                                encodedVal = Float.toString((Float) val);
                            } else if(val instanceof Double) {
                                encodedVal = Double.toString((Double) val);
                            }
                            // ...
                        } else if(val instanceof Character) {
                            Character charVal = (Character) val;
                            String strVal = Character.toString(charVal);
                            try {
                                encodedVal = URLEncoder.encode(strVal, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to URL encode the param value = " + strVal, e);
                            } 
                        } else if(val instanceof String) {
                            String strVal = (String) val;
                            try {
                                encodedVal = URLEncoder.encode(strVal, "UTF-8");
                            } catch (UnsupportedEncodingException e) {
                                if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to URL encode the param value = " + strVal, e);
                            } 
                        } else if(val instanceof List<?>) {
                            @SuppressWarnings("unchecked")
                            List<String> listVal = (List<String>) val;
                            if(listVal != null && !listVal.isEmpty()) {
                                //encodedVal = StringUtil.join(listVal, ",");
                                List<String> encodedList = new ArrayList<String>();
                                for(String v : listVal) {                                    
                                    try {
                                        encodedList.add(URLEncoder.encode(v, "UTF-8"));
                                    } catch (UnsupportedEncodingException e) {
                                        if(log.isLoggable(Level.WARNING)) log.log(Level.WARNING, "Failed to URL encode the param value = " + v, e);
                                    }
                                }
                                encodedVal = StringUtil.join(encodedList, ",");
                            }
                        } else {
                            // ????
                            if(log.isLoggable(Level.WARNING)) log.warning("Invalid params: val = " + val);
                        }
                        if(encodedVal != null) {
                            sb.append("=");
                            sb.append(encodedVal);
                        }
                    }
                    sb.append("&");
                }
            }
            String queryStr = sb.toString();
            if(queryStr.endsWith("&")) {
                queryStr = queryStr.substring(0, queryStr.length()-1);
            }
            return queryStr;
        }
    }

}
