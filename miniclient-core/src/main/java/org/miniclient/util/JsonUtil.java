package org.miniclient.util;

import java.io.IOException;
import java.io.Reader;
import java.io.Writer;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.RestApiException;
import org.miniclient.json.LiteJsonBuilder;
import org.miniclient.json.LiteJsonParser;
import org.miniclient.json.MiniClientJsonException;
import org.miniclient.json.builder.MiniClientJsonBuilder;
import org.miniclient.json.parser.MiniClientJsonParser;


/**
 * Utility functions for serializing/deserializing json object/string.
 */
public final class JsonUtil
{
    private static final Logger log = Logger.getLogger(JsonUtil.class.getName());

    // Note that this is the only place 
    //      where we use JSON parsing/building across all packages of MiniClient.
    // We can replace the internal JSON parser/builder 
    //      with any third-party parsers/builders, including MiniJson, here.
    // ...

    private JsonUtil() {}
    
    // TBD:
    // Is it safe to reuse these???
    private static LiteJsonParser sJsonParser = new MiniClientJsonParser();
    private static LiteJsonBuilder sJsonBuilder = new MiniClientJsonBuilder();
//    private static LiteJsonParser jsonParser = null;
//    private static LiteJsonBuilder jsonBuilder = null;
//    private static LiteJsonParser getJsonParser()
//    {
//        if(jsonParser == null) {
//            jsonParser = new MiniClientJsonParser();
//        }
//        return jsonParser;
//    }
//    private static LiteJsonBuilder getJsonBuilder()
//    {
//        if(jsonBuilder == null) {
//            jsonBuilder = new MiniClientJsonBuilder();
//        }
//        return jsonBuilder;
//    }


    /**
     * Parse the json string into a structure comprising only three types: a map, a list, and a scalar value.
     * 1) Map of {String -> Object}. The value type can be a map, a list, or a scalar value.
     * 2) List of {Object}. Ditto.
     * 3) Any other objects, primitive type, Object, a collection other than list and map, are treated as scalar,
     *    and they are treated as leaf nodes. (Even if an object has an internal structure, we do not have the type information.)
     *    Note that a leaf node object cannot be instantiated as an object (it's only a string). 
     * 
     * @param jsonStr Input JSON string representing a map, a list, or an object/primitive type.
     * @return The object deserialized from the jsonStr.
     * @throws RestApiException if JSON parsing fails.
     */
    public static Object parseJson(String jsonStr) throws RestApiException
    {
        if(log.isLoggable(Level.FINER)) log.finer("jsonStr = " + jsonStr);

        Object obj = null;
        try {
            // obj = getJsonParser().parse(jsonStr);
            obj = sJsonParser.parse(jsonStr);
        } catch (MiniClientJsonException e) {
            log.log(Level.WARNING, "Failed to parse the Json string.", e);
            throw new RestApiException("Failed to parse the Json string.", e);
        }
        
        if(log.isLoggable(Level.FINE)) log.fine("obj = " + obj);
        return obj;
    }
    
    /**
     * Parse the json string into a structure comprising only three types: a map, a list, and a scalar value.
     * 1) Map of {String -> Object}. The value type can be a map, a list, or a scalar value.
     * 2) List of {Object}. Ditto.
     * 3) Any other objects, primitive type, Object, a collection other than list and map, are treated as scalar,
     *    and they are treated as leaf nodes. (Even if an object has an internal structure, we do not have the type information.)
     *    Note that a leaf node object cannot be instantiated as an object (it's only a string). 
     * 
     * @param reader Reader from which the input JSON string is to be read representing a map, a list, or an object/primitive type. 
     * @return The object deserialized from the jsonStr.
     * @throws RestApiException if JSON parsing fails.
     * @throws IOException if error occurs while reading from the reader.
     */
    public static Object parseJson(Reader reader) throws RestApiException, IOException
    {
        Object obj = null;
        try {
            // obj = getJsonParser().parse(reader);
            obj = sJsonParser.parse(reader);
        } catch (MiniClientJsonException e) {
            log.log(Level.WARNING, "Failed to parse the Json string from the reader.", e);
            throw new RestApiException("Failed to parse the Json string from the reader.", e);
        }
        
        if(log.isLoggable(Level.FINE)) log.fine("obj = " + obj);
        return obj;
    }


    /**
     * Returns a JSON string for the given object.
     * The input can be a map of {String -> Object}, a list of {Object}, or an Object/primitive type.
     * It recursively traverses each element as long as they are a list or a map.
     * All other elements (primitive, object, a collection type other than list and map) are treated as leaf nodes.
     * Objects are converted to a string using toString().
     * (Note: Even if an object has an internal structure, we do not have the type information.)
     * 
     * @param jsonObj An object that is to be converted to JSON string.
     * @return The json string representation of jsonObj.
     * @throws RestApiException if JSON build fails.
     */
    public static String buildJson(Object jsonObj) throws RestApiException
    {
        if(log.isLoggable(Level.FINER)) log.finer("jsonObj = " + jsonObj);

        String jsonStr = null;
        try {
            // jsonStr = getJsonBuilder().build(jsonObj);
            jsonStr = sJsonBuilder.build(jsonObj);
        } catch (MiniClientJsonException e) {
            log.log(Level.WARNING, "Failed to build a Json string.", e);
            throw new RestApiException("Failed to build a Json string.", e);
        }

        if(log.isLoggable(Level.FINE)) log.fine("jsonStr = " + jsonStr);
        return jsonStr;
    }

    /**
     * Returns a JSON string for the given object.
     * The input can be a map of {String -> Object}, a list of {Object}, or an Object/primitive type.
     * It recursively traverses each element as long as they are a list or a map.
     * All other elements (primitive, object, a collection type other than list and map) are treated as leaf nodes.
     * Objects are converted to a string using toString().
     * (Note: Even if an object has an internal structure, we do not have the type information.)
     * 
     * @param writer Writer to which the generated JSON string is to be written. Cannot be null. 
     * @param jsonObj An object that is to be converted to JSON string.
     * @throws RestApiException if JSON build fails.
     * @throws IOException if error occurs while writing to writer.
     */
    public static void buildJson(Writer writer, Object jsonObj) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINER)) log.finer("jsonObj = " + jsonObj);

        try {
            // getJsonBuilder().build(writer, jsonObj);
            sJsonBuilder.build(writer, jsonObj);
        } catch (MiniClientJsonException e) {
            log.log(Level.WARNING, "Failed to build a Json string.", e);
            throw new RestApiException("Failed to build a Json string to writer.", e);
        }
    }

    
}
