package org.miniclient;


// Base exception to be used through REST Web services methods.
public class RestApiException extends Exception
{
    private static final long serialVersionUID = 1L;
    
    // Resource URL (or, URL path segment).
    private final String resource;       // From request
    // HTTP status code.
    private final int responseCode;      // From response.statusCode
    // In case of 3xx response
    private final String redirectLocation;     // Redirect location response field.


    public RestApiException() 
    {
        this((String) null);
    }
    public RestApiException(String message) 
    {
        this(message, (String) null);
    }
    public RestApiException(String message, String resource) 
    {
        this(message, resource, 0);
    }
    public RestApiException(String message, String resource, int responseCode) 
    {
        this(message, resource, responseCode, null);
    }
    public RestApiException(String message, String resource, int responseCode, String redirectLocation) 
    {
        super(message);
        this.resource = resource;
        this.responseCode = responseCode;
        this.redirectLocation = redirectLocation;
    }
    public RestApiException(String message, Throwable cause) 
    {
        this(message, cause, (String) null);
    }
    public RestApiException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, 0);
    }
    public RestApiException(String message, Throwable cause, String resource, int responseCode) 
    {
        this(message, cause, resource, responseCode, null);
    }
    public RestApiException(String message, Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(message, cause);
        this.resource = resource;
        this.responseCode = responseCode;
        this.redirectLocation = redirectLocation;
    }
    public RestApiException(Throwable cause) 
    {
        this(cause, (String) null);
    }
    public RestApiException(Throwable cause, String resource) 
    {
        this(cause, resource, 0);
    }
    public RestApiException(Throwable cause, String resource, int responseCode) 
    {
        this(cause, resource, responseCode, null);
    }
    public RestApiException(Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(cause);
        this.resource = resource;
        this.responseCode = responseCode;
        this.redirectLocation = redirectLocation;
    }


    // Getters only.

    public String getResource()
    {
        return this.resource;
    }

    public int getResponseCode()
    {
        return responseCode;
    }

    public String getRedirectLocation()
    {
        return redirectLocation;
    }

}
