package org.miniclient;

import org.miniclient.credential.UserCredential;


/**
 * User specific client object instance.
 */
public interface FlexibleUserClient extends UserClient
{
    void setUserCredential(UserCredential userCredential);
}
