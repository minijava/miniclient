package org.miniclient;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.RequestRetryPolicy;


/**
 * Base interface for "http" based client.
 */
public interface WebClient
{
    // Using the "strategy" pattern...
    AuthRefreshPolicy getAuthRefreshPolicy();
    RequestRetryPolicy getRequestRetryPolicy();
    ClientCachePolicy getClientCachePolicy();
    CacheControlPolicy getCacheControlPolicy();
    AutoRedirectPolicy getAutoRedirectPolicy();
    // etc...
    
}
