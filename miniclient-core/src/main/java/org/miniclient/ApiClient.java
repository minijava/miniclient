package org.miniclient;


/**
 * "Marker" interface for "API clients".
 * Note that MiniClient provides two different API sets:
 * "API clients": High level. Uses CRUD action verbs.
 * "REST clients": Low level. Uses HTTP method verbs.
 */
public interface ApiClient
{

}
