package org.miniclient;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.RequestRetryPolicy;


// Base interface for "http" based client.
public interface FlexibleWebClient extends WebClient
{
    void setAuthRefreshPolicy(AuthRefreshPolicy authRefreshPolicy);
    void setRequestRetryPolicy(RequestRetryPolicy requestRetryPolicy);
    void setClientCachePolicy(ClientCachePolicy clientCachePolicy);
    void setCacheControlPolicy(CacheControlPolicy cacheControlPolicy);
    void setAutoRedirectPolicy(AutoRedirectPolicy autoRedirectPolicy);
    // etc...
    
}
