package org.miniclient.credential;

import java.util.Set;


public interface DataAccessCredential
{
    // Auth token may be associated with a set of "data scopes"
    //    e.g., in the case of Google OAuth2, Facebook Connect, etc..
    Set<String> getDataScopes();
    boolean containsScope(String scope);
    // boolean containsAllScopes(Set<String> scopeSet);
    // boolean containsAnyScope(Set<String> scopeSet);

}
