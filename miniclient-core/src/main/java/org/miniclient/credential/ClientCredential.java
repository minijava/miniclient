package org.miniclient.credential;



public interface ClientCredential extends AuthCredential
{
    String getClientKey();
    String getClientSecret();
}
