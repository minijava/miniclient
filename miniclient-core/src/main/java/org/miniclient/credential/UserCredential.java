package org.miniclient.credential;

import java.util.Set;


// TBD:
// Is this generic/broad enough to cover different auth methods????
public interface UserCredential extends AuthCredential, DataAccessCredential
{
    // Auth data is read only if this value is true...
    // TBD: Is this necessary???
    // (Default value true???)
    boolean isAuthRequired();

    // (1) User ID fields.

    String getUser();              // User.guid. (from the client system)
    String getUserId();            // User id or username (for the server)
    // String getUsername();
    // String getUserEmail();

    // (2) Auth fields.

    // authMethod is optional.
    // If a valid value is set, it overwrites the client's default value.
    String getAuthMethod();

    // Different auth methods require different set of auth credential
    // authToken and authSecret are interpreted according to the auth method.
    // Basic: authToken=username, authSecret=password
    // Digest: ???
    // OAuth 2-legged: authToken,authSecret: not used. Use ClientCredential.clientKey, clientSecret.
    // OAuth 1.0a: authToken=accessToken, authSecrent=tokenSecret
    // OAuth2 Bearer Token: authToken=accessToken, authSecret: not used.
    // String getAuthKey();
    String getAuthToken();      // access token.
    String getAuthSecret();     // token secret

    // User's access token expiration time.
    // Null means "non-expiring" or "unknown".
    Long getExpirationTime();   // Unix epoch time in milli seconds.
    
    // This is needed to support auth token refresh.
    // Returns the last refreshed time, if known.
    Long getRefreshedTime();
}
