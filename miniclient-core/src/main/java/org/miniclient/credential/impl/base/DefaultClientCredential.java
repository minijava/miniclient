package org.miniclient.credential.impl.base;

import java.util.logging.Logger;

import org.miniclient.credential.impl.AbstractClientCredential;


public final class DefaultClientCredential extends AbstractClientCredential
{
    private static final Logger log = Logger.getLogger(DefaultClientCredential.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultClientCredential(String clientKey, String clientSecret)
    {
        super(clientKey, clientSecret);
    }

}
