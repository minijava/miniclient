package org.miniclient.credential.impl.base;

import java.util.Set;
import java.util.logging.Logger;

import org.miniclient.credential.impl.AbstractUserCredential;


public final class DefaultUserCredential extends AbstractUserCredential
{
    private static final Logger log = Logger.getLogger(DefaultUserCredential.class.getName());
    private static final long serialVersionUID = 1L;

    public DefaultUserCredential()
    {
        super();
    }

    public DefaultUserCredential(boolean authRequred)
    {
        super(authRequred);
    }

    public DefaultUserCredential(boolean authRequred, String user, String userId)
    {
        super(authRequred, user, userId);
    }
    public DefaultUserCredential(boolean authRequired, String user,
            String userId, String authMethod)
    {
        super(authRequired, user, userId, authMethod);
    }
    public DefaultUserCredential(boolean authRequired, String user,
            String userId, String authMethod, String authToken,
            String authSecret)
    {
        super(authRequired, user, userId, authMethod, authToken, authSecret);
    }
    public DefaultUserCredential(boolean authRequired, String user,
            String userId, String authMethod, String authToken,
            String authSecret, Set<String> dataScopes)
    {
        super(authRequired, user, userId, authMethod, authToken, authSecret, dataScopes);
    }
    public DefaultUserCredential(boolean authRequired, String user,
            String userId, String authMethod, String authToken,
            String authSecret, Set<String> dataScopes, Long expirationTime,
            Long refreshedTime)
    {
        super(authRequired, user, userId, authMethod, authToken, authSecret,
                dataScopes, expirationTime, refreshedTime);
    }


}
