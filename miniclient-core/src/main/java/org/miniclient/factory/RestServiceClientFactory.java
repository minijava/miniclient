package org.miniclient.factory;

import org.miniclient.maker.RestServiceClientMaker;



public interface RestServiceClientFactory extends ClientFactory, RestServiceClientMaker
{
    // Factory method
    // RestServiceClient createClient();

    // ???
    RestUserClientFactory createRestUserClientFactory();

}
