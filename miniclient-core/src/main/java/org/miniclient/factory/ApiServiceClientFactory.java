package org.miniclient.factory;

import org.miniclient.maker.ApiServiceClientMaker;


public interface ApiServiceClientFactory extends ClientFactory, ApiServiceClientMaker
{
    // ????
    // ApiServiceClient createClient();

    // ???
    ApiUserClientFactory createApiUserClientFactory();

}
