package org.miniclient.factory;

import org.miniclient.maker.RestUserClientMaker;


public interface RestUserClientFactory extends ClientFactory, RestUserClientMaker
{
    // ????
    // RestUserClient createClient();

    // ???
    // RestServiceClientFactory createRestServiceClientFactory();
    
}
