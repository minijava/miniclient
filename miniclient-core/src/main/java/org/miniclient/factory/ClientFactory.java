package org.miniclient.factory;

import org.miniclient.ResourceClient;


public interface ClientFactory
{
    // All "factory" can create a client of a concrete type.
    // Follows the "factory method" design pattern.
    ResourceClient createClient(String resourceBaseUrl);
    
}
