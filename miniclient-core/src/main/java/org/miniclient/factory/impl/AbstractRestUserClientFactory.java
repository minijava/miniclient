package org.miniclient.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;
import org.miniclient.factory.ClientFactory;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.impl.AbstractRestUserClient;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.maker.impl.AbstractRestUserClientMaker;


public abstract class AbstractRestUserClientFactory implements RestUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractRestUserClientFactory.class.getName());


    // Abstract factory.
    private RestUserClientMaker restUserClientMaker;

    protected AbstractRestUserClientFactory()
    {
        restUserClientMaker = makeRestUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestUserClientFactoryHolder
    {
        private static final AbstractRestUserClientFactory INSTANCE = new AbstractRestUserClientFactory() {};
    }

    // Singleton method
    public static AbstractRestUserClientFactory getInstance()
    {
        return AbstractRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new AbstractRestUserClient(resourceBaseUrl) {};
    }
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return AbstractRestUserClientMaker.getInstance();
    }


    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestUserClient(resourceBaseUrl);
    }

    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return restUserClientMaker.makeServiceClient(resourceBaseUrl);
    }


    @Override
    public String toString()
    {
        return "AbstractRestUserClientFactory [restUserClientMaker="
                + restUserClientMaker + "]";
    }

 
}
