package org.miniclient.factory.impl.base;

import java.util.logging.Logger;

import org.miniclient.RestUserClient;
import org.miniclient.factory.impl.AbstractRestUserClientFactory;
import org.miniclient.impl.base.DefaultRestUserClient;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.maker.impl.base.DefaultRestUserClientMaker;


public final class DefaultRestUserClientFactory extends
        AbstractRestUserClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultRestUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultRestUserClientFactoryHolder
    {
        private static final DefaultRestUserClientFactory INSTANCE = new DefaultRestUserClientFactory();
    }

    // Singleton method
    public static DefaultRestUserClientFactory getInstance()
    {
        return DefaultRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new DefaultRestUserClient(resourceBaseUrl);
    }
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return DefaultRestUserClientMaker.getInstance();
    }

    
}
