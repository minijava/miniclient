package org.miniclient.factory.impl.base;

import java.util.logging.Logger;

import org.miniclient.ApiUserClient;
import org.miniclient.factory.impl.AbstractApiUserClientFactory;
import org.miniclient.impl.base.DefaultApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;
import org.miniclient.maker.impl.base.DefaultApiUserClientMaker;


public final class DefaultApiUserClientFactory extends
        AbstractApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultApiUserClientFactoryHolder
    {
        private static final DefaultApiUserClientFactory INSTANCE = new DefaultApiUserClientFactory();
    }

    // Singleton method
    public static DefaultApiUserClientFactory getInstance()
    {
        return DefaultApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new DefaultApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return DefaultApiUserClientMaker.getInstance();
    }

    
}
