package org.miniclient.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.factory.ClientFactory;
import org.miniclient.factory.RestServiceClientFactory;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.impl.AbstractRestServiceClient;
import org.miniclient.maker.RestServiceClientMaker;
import org.miniclient.maker.impl.AbstractRestServiceClientMaker;


public abstract class AbstractRestServiceClientFactory implements RestServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractRestServiceClientFactory.class.getName());

    // Abstract factory.
    private RestServiceClientMaker restServiceClientMaker;

    protected AbstractRestServiceClientFactory()
    {
        restServiceClientMaker = makeRestServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractRestServiceClientFactoryHolder
    {
        private static final AbstractRestServiceClientFactory INSTANCE = new AbstractRestServiceClientFactory() {};
    }

    // Singleton method
    public static AbstractRestServiceClientFactory getInstance()
    {
        return AbstractRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new AbstractRestServiceClient(resourceBaseUrl) {};
    }
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return AbstractRestServiceClientMaker.getInstance();
    }
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return AbstractRestUserClientFactory.getInstance();
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestServiceClient(resourceBaseUrl);
    }

    @Override
    public RestUserClientFactory createRestUserClientFactory()
    {
        return makeRestUserClientFactory();
    }


    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return restServiceClientMaker.makeResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return restServiceClientMaker.makeHttpMethodFilter();
    }


    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return restServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return restServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return restServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return restServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return restServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return restServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "AbstractRestServiceClientFactory [restServiceClientMaker="
                + restServiceClientMaker + "]";
    }


}
