package org.miniclient.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ApiUserClient;
import org.miniclient.ResourceClient;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;
import org.miniclient.impl.AbstractApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;
import org.miniclient.maker.impl.AbstractApiUserClientMaker;


public abstract class AbstractApiUserClientFactory implements ApiUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractApiUserClientFactory.class.getName());


    // Abstract factory.
    private ApiUserClientMaker apiUserClientMaker;

    protected AbstractApiUserClientFactory()
    {
        apiUserClientMaker = makeApiUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractApiUserClientFactoryHolder
    {
        private static final AbstractApiUserClientFactory INSTANCE = new AbstractApiUserClientFactory() {};
    }

    // Singleton method
    public static AbstractApiUserClientFactory getInstance()
    {
        return AbstractApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new AbstractApiUserClient(resourceBaseUrl) {};
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return AbstractApiUserClientMaker.getInstance();
    }



    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiUserClient(resourceBaseUrl);
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return apiUserClientMaker.makeServiceClient(resourceBaseUrl);
    }


    @Override
    public String toString()
    {
        return "AbstractApiUserClientFactory [apiUserClientMaker="
                + apiUserClientMaker + "]";
    }


}
