package org.miniclient.factory.impl;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.factory.ApiServiceClientFactory;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;
import org.miniclient.impl.AbstractApiServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;
import org.miniclient.maker.impl.AbstractApiServiceClientMaker;


public abstract class AbstractApiServiceClientFactory implements ApiServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(AbstractApiServiceClientFactory.class.getName());

    // Abstract factory.
    private ApiServiceClientMaker apiServiceClientMaker;

    protected AbstractApiServiceClientFactory()
    {
        apiServiceClientMaker = makeApiServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class AbstractApiServiceClientFactoryHolder
    {
        private static final AbstractApiServiceClientFactory INSTANCE = new AbstractApiServiceClientFactory() {};
    }

    // Singleton method
    public static AbstractApiServiceClientFactory getInstance()
    {
        return AbstractApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new AbstractApiServiceClient(resourceBaseUrl) {};
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return AbstractApiServiceClientMaker.getInstance();
    }
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return AbstractApiUserClientFactory.getInstance();
    }


    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiServiceClient(resourceBaseUrl);
    }

    @Override
    public ApiUserClientFactory createApiUserClientFactory()
    {
        return makeApiUserClientFactory();
    }


    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return apiServiceClientMaker.makeRestClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return apiServiceClientMaker.makeCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return apiServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return apiServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return apiServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return apiServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return apiServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return apiServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "AbstractApiServiceClientFactory [apiServiceClientMaker="
                + apiServiceClientMaker + "]";
    }


}
