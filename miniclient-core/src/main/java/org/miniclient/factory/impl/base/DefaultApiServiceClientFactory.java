package org.miniclient.factory.impl.base;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.factory.impl.AbstractApiServiceClientFactory;
import org.miniclient.impl.base.DefaultApiServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;
import org.miniclient.maker.impl.base.DefaultApiServiceClientMaker;


public final class DefaultApiServiceClientFactory extends
        AbstractApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(DefaultApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class DefaultApiServiceClientFactoryHolder
    {
        private static final DefaultApiServiceClientFactory INSTANCE = new DefaultApiServiceClientFactory();
    }

    // Singleton method
    public static DefaultApiServiceClientFactory getInstance()
    {
        return DefaultApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new DefaultApiServiceClient(resourceBaseUrl);
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return DefaultApiServiceClientMaker.getInstance();
    }

    
}
