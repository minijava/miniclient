package org.miniclient.factory.manager;

import java.util.logging.Logger;

import org.miniclient.factory.ApiServiceClientFactory;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.impl.base.DefaultApiServiceClientFactory;
import org.miniclient.factory.impl.base.DefaultApiUserClientFactory;


/**
 * This manager class uses the "Abstract Factory" pattern.
 * Client can inherit from this class to return a proper concrete factory(s).
 */
public abstract class AbstractApiClientFactoryManager
{
    private static final Logger log = Logger.getLogger(AbstractApiClientFactoryManager.class.getName());

    private ApiServiceClientFactory apiServiceClientFactory;
    private ApiUserClientFactory apiUserClientFactory;
    
    protected AbstractApiClientFactoryManager() 
    {
        apiServiceClientFactory = DefaultApiServiceClientFactory.getInstance();
        // apiUserClientFactory = DefaultApiUserClientFactory.getInstance();
        apiUserClientFactory = apiServiceClientFactory.createApiUserClientFactory();
    }

    // Initialization-on-demand holder.
    private static final class AbstractApiClientFactoryManagerHolder
    {
        private static final AbstractApiClientFactoryManager INSTANCE = new AbstractApiClientFactoryManager() {};
    }

    // Singleton method
    public static AbstractApiClientFactoryManager getInstance()
    {
        return AbstractApiClientFactoryManagerHolder.INSTANCE;
    }

    // Note:
    // Client should override these methods, or the constructor, to return appropriate concrete factories.
    
    // Returns a service client factory.
    public ApiServiceClientFactory getApiServiceClientFactory()
    {
        return apiServiceClientFactory;
    }
    
    // Returns a user client factory.
    public ApiUserClientFactory getApiUserClientFactory()
    {
        return apiUserClientFactory;
    }


    // TBD:
    // Setters to inject factories, if needed.
    // ....
    
    public void setApiClientFactories(ApiServiceClientFactory apiServiceClientFactory)
    {
        setApiClientFactories(apiServiceClientFactory, null);
    }
    public void setApiClientFactories(ApiServiceClientFactory apiServiceClientFactory, ApiUserClientFactory apiUserClientFactory)
    {
        if(apiServiceClientFactory != null) {
            this.apiServiceClientFactory = apiServiceClientFactory;
            if(apiUserClientFactory != null) {
                this.apiUserClientFactory = apiUserClientFactory;
            } else {
                this.apiUserClientFactory = apiServiceClientFactory.createApiUserClientFactory();
            }
        } else {
            // ???
            log.info("Input apiServiceClientFactory is null. Both apiServiceClientFactory and apiUserClientFactory will be ignored.");
        }

    }


}
