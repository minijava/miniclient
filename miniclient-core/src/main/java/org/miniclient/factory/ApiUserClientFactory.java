package org.miniclient.factory;

import org.miniclient.maker.ApiUserClientMaker;


public interface ApiUserClientFactory extends ClientFactory, ApiUserClientMaker
{
    // ????
    // ApiUserClient createClient();

    // ???
    // ApiServiceClientFactory createApiServiceClientFactory();

}
