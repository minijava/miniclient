package org.miniclient;


public interface ResourceUserClientPool extends ResourceClient
{
    ResourceUserClient getUserClient(String userId);
    // ...
}
