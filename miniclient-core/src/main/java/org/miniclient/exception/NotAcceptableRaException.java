package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class NotAcceptableRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public NotAcceptableRaException() 
    {
        this((String) null);
    }
    public NotAcceptableRaException(String message) 
    {
        this(message, (String) null);
    }
    public NotAcceptableRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.NOT_ACCEPTABLE);
    }
    public NotAcceptableRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public NotAcceptableRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public NotAcceptableRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.NOT_ACCEPTABLE);
    }
    public NotAcceptableRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public NotAcceptableRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public NotAcceptableRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.NOT_ACCEPTABLE);
    }
    public NotAcceptableRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
