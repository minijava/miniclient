package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class TemporaryRedirectRaException extends RestApiRedirectException
{
    private static final long serialVersionUID = 1L;

    public TemporaryRedirectRaException() 
    {
        this((String) null);
    }
    public TemporaryRedirectRaException(String message) 
    {
        this(message, (String) null);
    }
    public TemporaryRedirectRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.TEMPORARY_REDIRECT);
    }
    public TemporaryRedirectRaException(String message, String resource, int responseCode) 
    {
        this(message, resource, responseCode, null);
    }
    public TemporaryRedirectRaException(String message, String resource, int responseCode, String redirectLocation) 
    {
        super(message, resource, responseCode, redirectLocation);
    }
    public TemporaryRedirectRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public TemporaryRedirectRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.TEMPORARY_REDIRECT);
    }
    public TemporaryRedirectRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        this(message, cause, resource, responseCode, null);
    }
    public TemporaryRedirectRaException(String message, Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(message, cause, resource, responseCode, redirectLocation);
    }
    public TemporaryRedirectRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public TemporaryRedirectRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.TEMPORARY_REDIRECT);
    }
    public TemporaryRedirectRaException(Throwable cause, String resource, int responseCode) 
    {
        this(cause, resource, responseCode, null);
    }
    public TemporaryRedirectRaException(Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(cause, resource, responseCode, redirectLocation);
    }

}
