package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class MultipleChoiceRaException extends RestApiRedirectException
{
    private static final long serialVersionUID = 1L;

    public MultipleChoiceRaException() 
    {
        this((String) null);
    }
    public MultipleChoiceRaException(String message) 
    {
        this(message, (String) null);
    }
    public MultipleChoiceRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.MULTIPLE_CHOICES);
    }
    public MultipleChoiceRaException(String message, String resource, int responseCode) 
    {
        this(message, resource, responseCode, null);
    }
    public MultipleChoiceRaException(String message, String resource, int responseCode, String redirectLocation) 
    {
        super(message, resource, responseCode, redirectLocation);
    }
    public MultipleChoiceRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public MultipleChoiceRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.MULTIPLE_CHOICES);
    }
    public MultipleChoiceRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        this(message, cause, resource, responseCode, null);
    }
    public MultipleChoiceRaException(String message, Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(message, cause, resource, responseCode, redirectLocation);
    }
    public MultipleChoiceRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public MultipleChoiceRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.MULTIPLE_CHOICES);
    }
    public MultipleChoiceRaException(Throwable cause, String resource, int responseCode) 
    {
        this(cause, resource, responseCode, null);
    }
    public MultipleChoiceRaException(Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(cause, resource, responseCode, redirectLocation);
    }

}
