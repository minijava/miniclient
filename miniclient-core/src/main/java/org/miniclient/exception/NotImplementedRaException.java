package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class NotImplementedRaException extends RestApiServerException
{
    private static final long serialVersionUID = 1L;

    public NotImplementedRaException() 
    {
        this((String) null);
    }
    public NotImplementedRaException(String message) 
    {
        this(message, (String) null);
    }
    public NotImplementedRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.NOT_IMPLEMENTED);
    }
    public NotImplementedRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public NotImplementedRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public NotImplementedRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.NOT_IMPLEMENTED);
    }
    public NotImplementedRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public NotImplementedRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public NotImplementedRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.NOT_IMPLEMENTED);
    }
    public NotImplementedRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
