package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class RequestForbiddenRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public RequestForbiddenRaException() 
    {
        this((String) null);
    }
    public RequestForbiddenRaException(String message) 
    {
        this(message, (String) null);
    }
    public RequestForbiddenRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.FORBIDDEN);
    }
    public RequestForbiddenRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public RequestForbiddenRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public RequestForbiddenRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.FORBIDDEN);
    }
    public RequestForbiddenRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public RequestForbiddenRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public RequestForbiddenRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.FORBIDDEN);
    }
    public RequestForbiddenRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
