package org.miniclient.exception;

import java.util.logging.Logger;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD:
public final class StatusExceptionMapper
{
    private static final Logger log = Logger.getLogger(StatusExceptionMapper.class.getName());

    private StatusExceptionMapper() {}

    
    public static RestApiException buildException(String message, Throwable cause, String resource, int statusCode, String location)
    {
        RestApiException exception = null;
        
        if(StatusCode.isRedirection(statusCode)) {
            if(statusCode == StatusCode.MULTIPLE_CHOICES) {
                exception = new MultipleChoiceRaException(message, cause, resource, statusCode, location);
            } else if(statusCode == StatusCode.MOVED_PERMANENTLY) {
                exception = new MovedPermanentlyRaException(message, cause, resource, statusCode, location);
            } else if(statusCode == StatusCode.FOUND) {
                exception = new FoundElsewhereRaException(message, cause, resource, statusCode, location);
            } else if(statusCode == StatusCode.TEMPORARY_REDIRECT) {
                exception = new TemporaryRedirectRaException(message, cause, resource, statusCode, location);
            } else {
                // ???
                exception = new RestApiRedirectException(message, cause, resource, statusCode, location);
            }
        } else if(StatusCode.isError(statusCode)) {
            if(StatusCode.isClientError(statusCode)) {
                if(statusCode == StatusCode.BAD_REQUEST) {
                    exception = new BadRequestRaException(message, cause, resource, statusCode);                
                } else if(statusCode == StatusCode.UNAUTHORIZED) {
                    exception = new UnauthorizedRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.FORBIDDEN) {
                    exception = new RequestForbiddenRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.NOT_FOUND) {
                    exception = new ResourceNotFoundRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.METHOD_NOT_ALLOWED) {
                    exception = new MethodNotAllowedRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.NOT_ACCEPTABLE) {
                    exception = new NotAcceptableRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.REQUEST_TIMEOUT) {
                    exception = new RequestTimeoutRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.CONFLICT) {
                    // ???
                    // exception = new ResourceAlreadyPresentRaException(message, cause, resource, statusCode);
                    exception = new RequestConflictRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.GONE) {
                    exception = new ResourceGoneRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.UNSUPPORTED_MEDIA_TYPE) {
                    exception = new UnsupportedMediaTypeRaException(message, cause, resource, statusCode);
                } else {
                    // ???
                    // exception = new BadRequestRaException(message, cause, resource, statusCode);                
                    exception = new RestApiClientException(message, cause, resource, statusCode);
                }
            } else {
                if(statusCode == StatusCode.INTERNAL_SERVER_ERROR) {
                    exception = new InternalServerErrorRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.NOT_IMPLEMENTED) {
                    exception = new NotImplementedRaException(message, cause, resource, statusCode);
                } else if(statusCode == StatusCode.SERVICE_UNAVAILABLE) {
                    exception = new ServiceUnavailableRaException(message, cause, resource, statusCode);
                } else {
                    // ???
                    // exception = new InternalServerErrorRaException(message, cause, resource, statusCode);
                    exception = new RestApiServerException(message, cause, resource, statusCode);
                }
            }

        } else {
            // ???
        }
        
        return exception;
    }
    

}
