package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class RestApiRedirectException extends RestApiException
{
    private static final long serialVersionUID = 1L;

    public RestApiRedirectException() 
    {
        this((String) null);
    }
    public RestApiRedirectException(String message) 
    {
        this(message, (String) null);
    }
    public RestApiRedirectException(String message, String resource) 
    {
        this(message, resource, StatusCode.MULTIPLE_CHOICES);
    }
    public RestApiRedirectException(String message, String resource, int responseCode) 
    {
        this(message, resource, responseCode, null);
    }
    public RestApiRedirectException(String message, String resource, int responseCode, String redirectLocation) 
    {
        super(message, resource, responseCode, redirectLocation);
    }
    public RestApiRedirectException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public RestApiRedirectException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.MULTIPLE_CHOICES);
    }
    public RestApiRedirectException(String message, Throwable cause, String resource, int responseCode) 
    {
        this(message, cause, resource, responseCode, null);
    }
    public RestApiRedirectException(String message, Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(message, cause, resource, responseCode, redirectLocation);
    }
    public RestApiRedirectException(Throwable cause) 
    {
        this(cause, null);
    }
    public RestApiRedirectException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.MULTIPLE_CHOICES);
    }
    public RestApiRedirectException(Throwable cause, String resource, int responseCode) 
    {
        this(cause, resource, responseCode, null);
    }
    public RestApiRedirectException(Throwable cause, String resource, int responseCode, String redirectLocation) 
    {
        super(cause, resource, responseCode, redirectLocation);
    }

}
