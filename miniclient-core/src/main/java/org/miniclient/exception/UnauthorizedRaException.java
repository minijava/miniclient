package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class UnauthorizedRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public UnauthorizedRaException() 
    {
        this((String) null);
    }
    public UnauthorizedRaException(String message) 
    {
        this(message, (String) null);
    }
    public UnauthorizedRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.UNAUTHORIZED);
    }
    public UnauthorizedRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public UnauthorizedRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public UnauthorizedRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.UNAUTHORIZED);
    }
    public UnauthorizedRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public UnauthorizedRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public UnauthorizedRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.UNAUTHORIZED);
    }
    public UnauthorizedRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
