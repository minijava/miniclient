package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class RestApiClientException extends RestApiException
{
    private static final long serialVersionUID = 1L;

    public RestApiClientException() 
    {
        this((String) null);
    }
    public RestApiClientException(String message) 
    {
        this(message, (String) null);
    }
    public RestApiClientException(String message, String resource) 
    {
        this(message, resource, StatusCode.BAD_REQUEST);
    }
    public RestApiClientException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public RestApiClientException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public RestApiClientException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.BAD_REQUEST);
    }
    public RestApiClientException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public RestApiClientException(Throwable cause) 
    {
        this(cause, null);
    }
    public RestApiClientException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.BAD_REQUEST);
    }
    public RestApiClientException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
