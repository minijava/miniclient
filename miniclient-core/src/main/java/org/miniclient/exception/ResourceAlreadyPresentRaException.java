package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class ResourceAlreadyPresentRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public ResourceAlreadyPresentRaException() 
    {
        this((String) null);
    }
    public ResourceAlreadyPresentRaException(String message) 
    {
        this(message, (String) null);
    }
    public ResourceAlreadyPresentRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.CONFLICT);   // ???
    }
    public ResourceAlreadyPresentRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public ResourceAlreadyPresentRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public ResourceAlreadyPresentRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.CONFLICT);   // ???
    }
    public ResourceAlreadyPresentRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public ResourceAlreadyPresentRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public ResourceAlreadyPresentRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.CONFLICT);   // ???
    }
    public ResourceAlreadyPresentRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
