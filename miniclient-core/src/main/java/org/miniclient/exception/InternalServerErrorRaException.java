package org.miniclient.exception;

import org.miniclient.core.StatusCode;


// TBD.
public class InternalServerErrorRaException extends RestApiServerException
{
    private static final long serialVersionUID = 1L;

    public InternalServerErrorRaException() 
    {
        this((String) null);
    }
    public InternalServerErrorRaException(String message) 
    {
        this(message, (String) null);
    }
    public InternalServerErrorRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.INTERNAL_SERVER_ERROR);
    }
    public InternalServerErrorRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public InternalServerErrorRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public InternalServerErrorRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.INTERNAL_SERVER_ERROR);
    }
    public InternalServerErrorRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public InternalServerErrorRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public InternalServerErrorRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.INTERNAL_SERVER_ERROR);
    }
    public InternalServerErrorRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
