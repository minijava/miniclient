package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class RequestTimeoutRaException extends RestApiClientException
{
    private static final long serialVersionUID = 1L;

    public RequestTimeoutRaException() 
    {
        this((String) null);
    }
    public RequestTimeoutRaException(String message) 
    {
        this(message, (String) null);
    }
    public RequestTimeoutRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.REQUEST_TIMEOUT);
    }
    public RequestTimeoutRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public RequestTimeoutRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public RequestTimeoutRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.REQUEST_TIMEOUT);
    }
    public RequestTimeoutRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public RequestTimeoutRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public RequestTimeoutRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.REQUEST_TIMEOUT);
    }
    public RequestTimeoutRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
