package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class ServiceUnavailableRaException extends RestApiServerException
{
    private static final long serialVersionUID = 1L;

    public ServiceUnavailableRaException() 
    {
        this((String) null);
    }
    public ServiceUnavailableRaException(String message) 
    {
        this(message, (String) null);
    }
    public ServiceUnavailableRaException(String message, String resource) 
    {
        this(message, resource, StatusCode.SERVICE_UNAVAILABLE);
    }
    public ServiceUnavailableRaException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public ServiceUnavailableRaException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public ServiceUnavailableRaException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.SERVICE_UNAVAILABLE);
    }
    public ServiceUnavailableRaException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public ServiceUnavailableRaException(Throwable cause) 
    {
        this(cause, null);
    }
    public ServiceUnavailableRaException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.SERVICE_UNAVAILABLE);
    }
    public ServiceUnavailableRaException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
