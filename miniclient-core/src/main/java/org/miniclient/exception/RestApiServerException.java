package org.miniclient.exception;

import org.miniclient.RestApiException;
import org.miniclient.core.StatusCode;


// TBD.
public class RestApiServerException extends RestApiException
{
    private static final long serialVersionUID = 1L;

    public RestApiServerException() 
    {
        this((String) null);
    }
    public RestApiServerException(String message) 
    {
        this(message, (String) null);
    }
    public RestApiServerException(String message, String resource) 
    {
        this(message, resource, StatusCode.SERVICE_UNAVAILABLE);
    }
    public RestApiServerException(String message, String resource, int responseCode) 
    {
        super(message, resource, responseCode);
    }
    public RestApiServerException(String message, Throwable cause) 
    {
        this(message, cause, null);
    }
    public RestApiServerException(String message, Throwable cause, String resource) 
    {
        this(message, cause, resource, StatusCode.SERVICE_UNAVAILABLE);
    }
    public RestApiServerException(String message, Throwable cause, String resource, int responseCode) 
    {
        super(message, cause, resource, responseCode);
    }
    public RestApiServerException(Throwable cause) 
    {
        this(cause, null);
    }
    public RestApiServerException(Throwable cause, String resource) 
    {
        this(cause, resource, StatusCode.SERVICE_UNAVAILABLE);
    }
    public RestApiServerException(Throwable cause, String resource, int responseCode) 
    {
        super(cause, resource, responseCode);
    }

}
