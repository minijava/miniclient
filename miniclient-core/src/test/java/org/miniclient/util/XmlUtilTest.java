package org.miniclient.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class XmlUtilTest
{

    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testParseXml()
    {
        String v1 = "\"abc\"";
        Object obj1 = XmlUtil.parseXml(v1);
        System.out.println("obj1 = " + obj1);

        String v2 = "1020";
        Object obj2 = XmlUtil.parseXml(v2);
        System.out.println("obj2 = " + obj2);

        String v3 = "\"" + (new Date()).toString() + "\"";
        Object obj3 = XmlUtil.parseXml(v3);
        System.out.println("obj3 = " + obj3);

        
        String j1 = "<a></a>";
        Object list1 = XmlUtil.parseXml(j1);
        System.out.println("list1 = " + list1);

        String j2 = "<a>1</a><a>2</a>";
        Object list2 = XmlUtil.parseXml(j2);
        System.out.println("list2 = " + list2);

        String j3 = "<a><b>1</b></a><a><c>2</c></a>";
        Object list3 = XmlUtil.parseXml(j3);
        System.out.println("list3 = " + list3);

        
        String xml1 = "<a></a>";
        Object map1 = XmlUtil.parseXml(xml1);
        System.out.println("map1 = " + map1);

        String xml2 = "<a x=\"1\"></a>";
        Object map2 = XmlUtil.parseXml(xml2);
        System.out.println("map2 = " + map2);

        String xml3 = "<a x=\"1\"></a><a y=\"2\"></a>";
        Object map3 = XmlUtil.parseXml(xml3);
        System.out.println("map3 = " + map3);
        
    }

    @Test
    public void testXmlToJson()
    {
        String xmlStr1 = "<a><b>1</b><c>2</c></a>";
        System.out.println("xmlStr1 = " + xmlStr1);
        String jsonStr1 = XmlUtil.XmlToJson(xmlStr1);
        System.out.println("jsonStr1 = " + jsonStr1);
        
        String xmlStr2 = "<a><b>2</b><b>2</b></a>";
        System.out.println("xmlStr2 = " + xmlStr2);
        String jsonStr2 = XmlUtil.XmlToJson(xmlStr2);
        System.out.println("jsonStr2 = " + jsonStr2);
    }

    @Test
    public void testBuildXml()
    {
        int obj1 = 86400;
        String j1 = XmlUtil.buildXml(obj1);
        System.out.println("j1 = " + j1);

        String obj2 = "Test value";
        String j2 = XmlUtil.buildXml(obj2);
        System.out.println("j2 = " + j2);

        Date obj3 = new Date();
        String j3 = XmlUtil.buildXml(obj3);
        System.out.println("j3 = " + j3);


        List<Object> list1 = new ArrayList<Object>();
        String xmlStr1 = XmlUtil.buildXml(list1);
        System.out.println("xmlStr1 = " + xmlStr1);

        List<Object> list2 = new ArrayList<Object>();
        list2.add("a");
        String xmlStr2 = XmlUtil.buildXml(list2);
        System.out.println("xmlStr2 = " + xmlStr2);

        List<Object> list3 = new ArrayList<Object>();
        Map<String,Object> list3map = new LinkedHashMap<String,Object>();
        list3map.put("x1", "y1");
        list3map.put("x2", 111.01);
        list3map.put("x3", false);
        list3map.put("x4", 'c');
        list3map.put("x5", new Date());
        list3.add("b");
        list3.add(list3map);
        String xmlStr3 = XmlUtil.buildXml(list3);
        System.out.println("xmlStr3 = " + xmlStr3);

        List<Object> list4 = new ArrayList<Object>();
        List<Object> list4list = new ArrayList<Object>();
        list4list.add("z");
        list4list.add(true);
        list4list.add(333);
        list4list.add('d');
        list4list.add(new Date());
        list4.add("b");
        list4.add(list4list);
        String xmlStr4 = XmlUtil.buildXml(list4);
        System.out.println("xmlStr4 = " + xmlStr4);

        
        Map<String,Object> map1 = new LinkedHashMap<String,Object>();
        String xml1 = XmlUtil.buildXml(map1);
        System.out.println("xml1 = " + xml1);

        Map<String,Object> map2 = new LinkedHashMap<String,Object>();
        map2.put("a", "b");
        String xml2 = XmlUtil.buildXml(map2);
        System.out.println("xml2 = " + xml2);

        Map<String,Object> map3 = new LinkedHashMap<String,Object>();
        Map<String,Object> map3map = new LinkedHashMap<String,Object>();
        map3map.put("x1", "y1");
        map3map.put("x2", 111.01);
        map3map.put("x3", false);
        map3map.put("x4", 'c');
        map3map.put("x5", new Date());
        map3.put("a", "b");
        map3.put("c", map3map);
        String xml3 = XmlUtil.buildXml(map3);
        System.out.println("xml3 = " + xml3);

        Map<String,Object> map4 = new LinkedHashMap<String,Object>();
        List<Object> map4list = new ArrayList<Object>();
        map4list.add("z");
        map4list.add(true);
        map4list.add(333);
        map4list.add('d');
        map4list.add(new Date());
        map4.put("a", "b");
        map4.put("a", map4list);
        String xml4 = XmlUtil.buildXml(map4);
        System.out.println("xml4 = " + xml4);
        
    }

    @Test
    public void testJsonToXml()
    {
        String jsonStr1 = "{\"a\":1,\"b\":2}";
        System.out.println("jsonStr1 = " + jsonStr1);
        String xmlStr1 = XmlUtil.JsonToXml(jsonStr1);
        System.out.println("xmlStr1 = " + xmlStr1);

        String jsonStr2 = "[{\"a\":2,\"b\":2},\"c\"]";
        System.out.println("jsonStr2 = " + jsonStr2);
        String xmlStr2 = XmlUtil.JsonToXml(jsonStr2);
        System.out.println("xmlStr2 = " + xmlStr2);

        String jsonStr3 = "{\"x\":[55,100]}";
        System.out.println("jsonStr3 = " + jsonStr3);
        String xmlStr3 = XmlUtil.JsonToXml(jsonStr3);
        System.out.println("xmlStr3 = " + xmlStr3);
    }

}
