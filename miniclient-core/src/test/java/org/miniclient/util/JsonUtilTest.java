/**
 * 
 */
package org.miniclient.util;

import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;


/**
 *
 */
public class JsonUtilTest
{

    /**
     * @throws java.lang.Exception
     */
    @Before
    public void setUp() throws Exception
    {
    }

    /**
     * @throws java.lang.Exception
     */
    @After
    public void tearDown() throws Exception
    {
    }

    /**
     * Test method for {@link org.miniclient.util.JsonUtil#parseJson(java.lang.String)}.
     */
    @Test
    public void testParseJson()
    {
        try {
//        String v1 = "\"abc\"";
//        Object obj1 = JsonUtil.parseJson(v1);
//        System.out.println("obj1 = " + obj1);
//
//        String v2 = "1020";
//        Object obj2 = JsonUtil.parseJson(v2);
//        System.out.println("obj2 = " + obj2);
//
//        String v3 = "\"" + (new Date()).toString() + "\"";
//        Object obj3 = JsonUtil.parseJson(v3);
//        System.out.println("obj3 = " + obj3);

        
            String j1 = "[]";
            Object list1 = JsonUtil.parseJson(j1);
            System.out.println("list1 = " + list1);
    
            String j2 = "[\"a\", 1]";
            Object list2 = JsonUtil.parseJson(j2);
            System.out.println("list2 = " + list2);
    
            String j3 = "[50,60,{\"a\": 100}]";
            Object list3 = JsonUtil.parseJson(j3);
            System.out.println("list3 = " + list3);
    
            String j4 = "[{\"a\": [3,5,7,99.99]}, {\"b\": {\"c\": \"d\"}}]";
            Object list4 = JsonUtil.parseJson(j4);
            System.out.println("list4 = " + list4);
    
            
            String json1 = "{}";
            Object map1 = JsonUtil.parseJson(json1);
            System.out.println("map1 = " + map1);
    
            String json2 = "{\"a\": 1}";
            Object map2 = JsonUtil.parseJson(json2);
            System.out.println("map2 = " + map2);
    
            String json3 = "{\"a\": [3,5,7]}";
            Object map3 = JsonUtil.parseJson(json3);
            System.out.println("map3 = " + map3);
    
            String json4 = "{\"a\": [3,5,7,99.99, false], \"b\": {\"c\": \"d\"}}";
            Object map4 = JsonUtil.parseJson(json4);
            System.out.println("map4 = " + map4);
        
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Test method for {@link org.miniclient.util.JsonUtil#buildJson(java.util.Map)}.
     */
    @Test
    public void testBuildJson()
    {
        try {

//        int obj1 = 86400;
//        String j1 = JsonUtil.buildJson(obj1);
//        System.out.println("j1 = " + j1);
//
//        String obj2 = "Test value";
//        String j2 = JsonUtil.buildJson(obj2);
//        System.out.println("j2 = " + j2);
//
//        Date obj3 = new Date();
//        String j3 = JsonUtil.buildJson(obj3);
//        System.out.println("j3 = " + j3);


            List<Object> list1 = new ArrayList<Object>();
            String jsonStr1 = JsonUtil.buildJson(list1);
            System.out.println("jsonStr1 = " + jsonStr1);
    
            List<Object> list2 = new ArrayList<Object>();
            list2.add("a");
            String jsonStr2 = JsonUtil.buildJson(list2);
            System.out.println("jsonStr2 = " + jsonStr2);
    
            List<Object> list3 = new ArrayList<Object>();
            Map<String,Object> list3map = new LinkedHashMap<String,Object>();
            list3map.put("x1", "y1");
            list3map.put("x2", 111.01);
            list3map.put("x3", false);
            list3map.put("x4", 'c');
            list3map.put("x5", new Date());
            list3.add("b");
            list3.add(list3map);
            String jsonStr3 = JsonUtil.buildJson(list3);
            System.out.println("jsonStr3 = " + jsonStr3);
    
            List<Object> list4 = new ArrayList<Object>();
            List<Object> list4list = new ArrayList<Object>();
            list4list.add("z");
            list4list.add(true);
            list4list.add(333);
            list4list.add('d');
            list4list.add(new Date());
            list4.add("b");
            list4.add(list4list);
            String jsonStr4 = JsonUtil.buildJson(list4);
            System.out.println("jsonStr4 = " + jsonStr4);
    
            
            Map<String,Object> map1 = new LinkedHashMap<String,Object>();
            String json1 = JsonUtil.buildJson(map1);
            System.out.println("json1 = " + json1);
    
            Map<String,Object> map2 = new LinkedHashMap<String,Object>();
            map2.put("a", "b");
            String json2 = JsonUtil.buildJson(map2);
            System.out.println("json2 = " + json2);
    
            Map<String,Object> map3 = new LinkedHashMap<String,Object>();
            Map<String,Object> map3map = new LinkedHashMap<String,Object>();
            map3map.put("x1", "y1");
            map3map.put("x2", 111.01);
            map3map.put("x3", false);
            map3map.put("x4", 'c');
            map3map.put("x5", new Date());
            map3.put("a", "b");
            map3.put("c", map3map);
            String json3 = JsonUtil.buildJson(map3);
            System.out.println("json3 = " + json3);
    
            Map<String,Object> map4 = new LinkedHashMap<String,Object>();
            List<Object> map4list = new ArrayList<Object>();
            map4list.add("z");
            map4list.add(true);
            map4list.add(333);
            map4list.add('d');
            map4list.add(new Date());
            map4.put("a", "b");
            map4.put("a", map4list);
            String json4 = JsonUtil.buildJson(map4);
            System.out.println("json4 = " + json4);
        
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

}
