package org.miniclient.impl;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ AbstractApiServiceClientTest.class,
        AbstractRestServiceClientTest.class })
public class AllTests
{

}
