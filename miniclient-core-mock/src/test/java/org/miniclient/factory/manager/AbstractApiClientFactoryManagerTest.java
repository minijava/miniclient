package org.miniclient.factory.manager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.miniclient.factory.ApiServiceClientFactory;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.impl.AbstractApiServiceClientFactory;
import org.miniclient.factory.mock.MockApiServiceClientFactory;


public class AbstractApiClientFactoryManagerTest
{

    private AbstractApiClientFactoryManager abstractApiClientFactoryManager;
    
    @Before
    public void setUp() throws Exception
    {
        abstractApiClientFactoryManager = TestApiClientFactoryManager.getInstance();
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGetApiServiceClientFactory()
    {
        ApiServiceClientFactory apiServiceClientFactory1 = MockApiServiceClientFactory.getInstance();
        abstractApiClientFactoryManager.setApiClientFactories(apiServiceClientFactory1);
        
        ApiServiceClientFactory apiServiceClientFactory2 = abstractApiClientFactoryManager.getApiServiceClientFactory();
        System.out.println("apiServiceClientFactory2 = " + apiServiceClientFactory2);
        ApiUserClientFactory apiUserClientFactory2 = abstractApiClientFactoryManager.getApiUserClientFactory();
        System.out.println("apiUserClientFactory2 = " + apiUserClientFactory2);

    }

    @Test
    public void testGetApiUserClientFactory()
    {
        ApiServiceClientFactory apiServiceClientFactory1 = AbstractApiServiceClientFactory.getInstance();
        abstractApiClientFactoryManager.setApiClientFactories(apiServiceClientFactory1);
        
        ApiServiceClientFactory apiServiceClientFactory2 = abstractApiClientFactoryManager.getApiServiceClientFactory();
        System.out.println("apiServiceClientFactory2 = " + apiServiceClientFactory2);
        ApiUserClientFactory apiUserClientFactory2 = abstractApiClientFactoryManager.getApiUserClientFactory();
        System.out.println("apiUserClientFactory2 = " + apiUserClientFactory2);

    }

}
