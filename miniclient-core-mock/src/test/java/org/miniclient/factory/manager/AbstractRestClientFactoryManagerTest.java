package org.miniclient.factory.manager;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.miniclient.factory.RestServiceClientFactory;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.factory.impl.AbstractRestServiceClientFactory;
import org.miniclient.factory.mock.MockRestServiceClientFactory;

public class AbstractRestClientFactoryManagerTest
{

    private AbstractRestClientFactoryManager abstractRestClientFactoryManager;
    
    @Before
    public void setUp() throws Exception
    {
        abstractRestClientFactoryManager = TestRestClientFactoryManager.getInstance();
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGetRestServiceClientFactory()
    {
        RestServiceClientFactory restServiceClientFactory1 = MockRestServiceClientFactory.getInstance();
        abstractRestClientFactoryManager.setRestClientFactories(restServiceClientFactory1);
        
        RestServiceClientFactory restServiceClientFactory2 = abstractRestClientFactoryManager.getRestServiceClientFactory();
        System.out.println("restServiceClientFactory2 = " + restServiceClientFactory2);
        RestUserClientFactory restUserClientFactory2 = abstractRestClientFactoryManager.getRestUserClientFactory();
        System.out.println("restUserClientFactory2 = " + restUserClientFactory2);

    }

    @Test
    public void testGetRestUserClientFactory()
    {
        RestServiceClientFactory restServiceClientFactory1 = AbstractRestServiceClientFactory.getInstance();
        abstractRestClientFactoryManager.setRestClientFactories(restServiceClientFactory1);
        
        RestServiceClientFactory restServiceClientFactory2 = abstractRestClientFactoryManager.getRestServiceClientFactory();
        System.out.println("restServiceClientFactory2 = " + restServiceClientFactory2);
        RestUserClientFactory restUserClientFactory2 = abstractRestClientFactoryManager.getRestUserClientFactory();
        System.out.println("restUserClientFactory2 = " + restUserClientFactory2);

    }

}
