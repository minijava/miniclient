package org.miniclient;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ org.miniclient.factory.manager.AllTests.class })
public class AllTests
{

}
