package org.miniclient.maker.mock;

import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.mock.MockAuthRefreshPolicy;
import org.miniclient.common.mock.MockAutoRedirectPolicy;
import org.miniclient.common.mock.MockCacheControlPolicy;
import org.miniclient.common.mock.MockClientCachePolicy;
import org.miniclient.common.mock.MockCrudMethodFilter;
import org.miniclient.common.mock.MockDataAccessClient;
import org.miniclient.common.mock.MockRequestRetryPolicy;
import org.miniclient.maker.ApiServiceClientMaker;
import org.miniclient.mock.MockRestServiceClient;


// Mock factory.
public class MockApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockApiServiceClientMaker.class.getName());


    protected MockApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class MockApiServiceClientMakerHolder
    {
        private static final MockApiServiceClientMaker INSTANCE = new MockApiServiceClientMaker();
    }

    // Singleton method
    public static MockApiServiceClientMaker getInstance()
    {
        return MockApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        if(log.isLoggable(Level.INFO)) log.info("makeRestClient() called!!! with  resourceBaseUrl = " + resourceBaseUrl);
        return new MockRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new MockCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockApiServiceClientMaker []";
    }

    
}
