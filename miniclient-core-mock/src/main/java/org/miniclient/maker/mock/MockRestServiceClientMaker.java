package org.miniclient.maker.mock;

import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.common.mock.MockAuthRefreshPolicy;
import org.miniclient.common.mock.MockAutoRedirectPolicy;
import org.miniclient.common.mock.MockCacheControlPolicy;
import org.miniclient.common.mock.MockClientCachePolicy;
import org.miniclient.common.mock.MockDataAccessClient;
import org.miniclient.common.mock.MockHttpMethodFilter;
import org.miniclient.common.mock.MockRequestRetryPolicy;
import org.miniclient.common.mock.MockResourceUrlBuilder;
import org.miniclient.maker.RestServiceClientMaker;


// Mock factory.
public class MockRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockRestServiceClientMaker.class.getName());


    protected MockRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class MockRestServiceClientMakerHolder
    {
        private static final MockRestServiceClientMaker INSTANCE = new MockRestServiceClientMaker();
    }

    // Singleton method
    public static MockRestServiceClientMaker getInstance()
    {
        return MockRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new MockResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new MockHttpMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockAutoRedirectPolicy();
    }


    
    @Override
    public String toString()
    {
        return "MockRestServiceClientMaker []";
    }


}
