package org.miniclient.maker.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.maker.ApiUserClientMaker;
import org.miniclient.mock.MockApiServiceClient;


// Mock factory.
public class MockApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(MockApiUserClientMaker.class.getName());


    protected MockApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static class MockApiUserClientMakerHolder
    {
        private static final MockApiUserClientMaker INSTANCE = new MockApiUserClientMaker();
    }

    // Singleton method
    public static MockApiUserClientMaker getInstance()
    {
        return MockApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new MockApiServiceClient(resourceBaseUrl);
    }


    
    @Override
    public String toString()
    {
        return "MockApiUserClientMaker []";
    }

    
}
