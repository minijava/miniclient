package org.miniclient.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.ApiUserClient;
import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.credential.UserCredential;
import org.miniclient.impl.AbstractRestUserClient;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.maker.mock.MockRestUserClientMaker;
import org.miniclient.proxy.DecoratedRestUserClient;


// "Mock" object
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockRestUserClient extends AbstractRestUserClient implements DecoratedRestUserClient, ResourceUrlBuilder, Serializable
{
    private static final Logger log = Logger.getLogger(MockRestUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final RestUserClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockRestUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public MockRestUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
        this.decoratedClient = null;
    }

    public MockRestUserClient(RestServiceClient restServiceClient)
    {
        this(restServiceClient, null);
    }
    public MockRestUserClient(RestServiceClient restServiceClient,
            UserCredential userCredential)
    {
        super(restServiceClient, userCredential);
        this.decoratedClient = null;
    }

    public MockRestUserClient(RestUserClient decoratedClient)
    {
        super((String) null);
        this.decoratedClient = decoratedClient;
    }

    

    // Factory methods

    @Override
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return MockRestUserClientMaker.getInstance();
    }


    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Map<String, Object> get(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestUserClient.get(): id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.get(id, params);
        } else {
            return super.get(id, params);
        }
    }

    @Override
    public Map<String, Object> post(Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestUserClient.post(): inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.post(inputData);
        } else {
            return super.post(inputData);
        }
    }

    @Override
    public Map<String, Object> put(Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestUserClient.put(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.put(inputData, id);
        } else {
            return super.put(inputData, id);
        }
    }

    @Override
    public Map<String, Object> patch(Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestUserClient.patch(): partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.patch(partialData, id);
        } else {
            return super.patch(partialData, id);
        }
    }

    @Override
    public Map<String, Object> delete(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockRestUserClient.delete(): id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(id, params);
        } else {
            return super.delete(id, params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockRestUserClient [decoratedClient=" + decoratedClient
                + ", getRestServiceClient()=" + getRestServiceClient()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getResourcePostUrl()=" + getResourcePostUrl()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy()
                + ", getUserCredential()=" + getUserCredential()
                + ", isAccessAllowed()=" + isAccessAllowed() + "]";
    }


}
