package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.credential.impl.AbstractClientCredential;


public class MockClientCredential extends AbstractClientCredential
{
    private static final Logger log = Logger.getLogger(MockClientCredential.class.getName());
    private static final long serialVersionUID = 1L;

    public MockClientCredential(String clientKey, String clientSecret)
    {
        super(clientKey, clientSecret);
    }


    
    @Override
    public String toString()
    {
        return "MockClientCredential [getClientKey()=" + getClientKey()
                + ", getClientSecret()=" + getClientSecret() + "]";
    }

}
