package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractClientCachePolicy;


public class MockClientCachePolicy extends AbstractClientCachePolicy
{
    private static final Logger log = Logger.getLogger(MockClientCachePolicy.class.getName());
    private static final long serialVersionUID = 1L;

    
    
    @Override
    public String toString()
    {
        return "MockClientCachePolicy [isCacheEnabled()=" + isCacheEnabled()
                + ", getCacheLifetime()=" + getCacheLifetime() + "]";
    }

    
}
