package org.miniclient.common.mock;

import java.io.Serializable;
import java.util.logging.Logger;

import org.miniclient.impl.AbstractDataAccessClient;


public class MockDataAccessClient extends AbstractDataAccessClient implements Serializable
{
    private static final Logger log = Logger.getLogger(MockDataAccessClient.class.getName());
    private static final long serialVersionUID = 1L;

    public MockDataAccessClient()
    {
        super();
    }


    
    @Override
    public String toString()
    {
        return "MockDataAccessClient [getRequiredScopes()="
                + getRequiredScopes() + "]";
    }


}
