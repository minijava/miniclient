package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractRequestRetryPolicy;


public class MockRequestRetryPolicy extends AbstractRequestRetryPolicy
{
    private static final Logger log = Logger.getLogger(MockRequestRetryPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    public MockRequestRetryPolicy()
    {
        // TODO Auto-generated constructor stub
    }


    
    @Override
    public String toString()
    {
        return "MockRequestRetryPolicy [isRetryIfFails()=" + isRetryIfFails()
                + ", getMaxRetryCount()=" + getMaxRetryCount() + "]";
    }


}
