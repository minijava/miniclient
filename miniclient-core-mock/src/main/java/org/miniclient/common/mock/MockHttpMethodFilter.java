package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractHttpMethodFilter;


public class MockHttpMethodFilter extends AbstractHttpMethodFilter
{
    private static final Logger log = Logger.getLogger(MockHttpMethodFilter.class.getName());
    private static final long serialVersionUID = 1L;

    
    
    @Override
    public String toString()
    {
        return "MockHttpMethodFilter [getMethodSet()=" + getMethodSet() + "]";
    }

    
}
