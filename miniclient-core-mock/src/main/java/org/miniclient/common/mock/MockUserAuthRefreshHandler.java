package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractUserAuthRefreshHandler;


public class MockUserAuthRefreshHandler extends AbstractUserAuthRefreshHandler
{
    private static final Logger log = Logger.getLogger(MockUserAuthRefreshHandler.class.getName());
    private static final long serialVersionUID = 1L;


    public MockUserAuthRefreshHandler()
    {
    }



    @Override
    public String toString()
    {
        return "MockUserAuthRefreshHandler [isImeplemented()="
                + isImeplemented() + "]";
    }

    
}
