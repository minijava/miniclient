package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.UserAuthRefreshHandler;
import org.miniclient.common.impl.AbstractAuthRefreshPolicy;


public class MockAuthRefreshPolicy extends AbstractAuthRefreshPolicy
{
    private static final Logger log = Logger.getLogger(MockAuthRefreshPolicy.class.getName());
    private static final long serialVersionUID = 1L;

    
    public MockAuthRefreshPolicy()
    {
        // TODO Auto-generated constructor stub
    }

    
    // Factory method.

    @Override
    protected UserAuthRefreshHandler makeUserAuthRefreshHandler()
    {
        return new MockUserAuthRefreshHandler();
    }



    @Override
    public String toString()
    {
        return "MockAuthRefreshPolicy [isRefreshBeforeRequest()="
                + isRefreshBeforeRequest() + ", isRefreshIfFails()="
                + isRefreshIfFails() + ", getFutureMarginSeconds()="
                + getFutureMarginSeconds() + ", getAuthRefreshHandler()="
                + getAuthRefreshHandler() + "]";
    }


}
