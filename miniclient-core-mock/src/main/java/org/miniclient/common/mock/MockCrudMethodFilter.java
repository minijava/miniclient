package org.miniclient.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractCrudMethodFilter;


public class MockCrudMethodFilter extends AbstractCrudMethodFilter
{
    private static final Logger log = Logger.getLogger(MockCrudMethodFilter.class.getName());
    private static final long serialVersionUID = 1L;

    
    
    @Override
    public String toString()
    {
        return "MockCrudMethodFilter [getMethodSet()=" + getMethodSet() + "]";
    }

    
}
