package org.miniclient.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.factory.ApiServiceClientFactory;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;
import org.miniclient.maker.ApiServiceClientMaker;
import org.miniclient.maker.mock.MockApiServiceClientMaker;
import org.miniclient.mock.MockApiServiceClient;


public class MockApiServiceClientFactory implements ApiServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockApiServiceClientFactory.class.getName());

    // Mock factory.
    private ApiServiceClientMaker apiServiceClientMaker;

    protected MockApiServiceClientFactory()
    {
        apiServiceClientMaker = makeApiServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockApiServiceClientFactoryHolder
    {
        private static final MockApiServiceClientFactory INSTANCE = new MockApiServiceClientFactory() {};
    }

    // Singleton method
    public static MockApiServiceClientFactory getInstance()
    {
        return MockApiServiceClientFactoryHolder.INSTANCE;
    }

    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiServiceClient(resourceBaseUrl);
    }

    @Override
    public ApiUserClientFactory createApiUserClientFactory()
    {
        return makeApiUserClientFactory();
    }


    // Factory methods

    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new MockApiServiceClient(resourceBaseUrl);
    }
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MockApiServiceClientMaker.getInstance();
    }
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return MockApiUserClientFactory.getInstance();
    }


    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return apiServiceClientMaker.makeRestClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return apiServiceClientMaker.makeCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return apiServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return apiServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return apiServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return apiServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return apiServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return apiServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockApiServiceClientFactory [apiServiceClientMaker="
                + apiServiceClientMaker + "]";
    }


}
