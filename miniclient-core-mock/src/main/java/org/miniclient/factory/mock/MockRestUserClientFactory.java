package org.miniclient.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;
import org.miniclient.factory.ClientFactory;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.maker.mock.MockRestUserClientMaker;
import org.miniclient.mock.MockRestUserClient;


public class MockRestUserClientFactory implements RestUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockRestUserClientFactory.class.getName());


    // Mock factory.
    private RestUserClientMaker restUserClientMaker;

    protected MockRestUserClientFactory()
    {
        restUserClientMaker = makeRestUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockRestUserClientFactoryHolder
    {
        private static final MockRestUserClientFactory INSTANCE = new MockRestUserClientFactory() {};
    }

    // Singleton method
    public static MockRestUserClientFactory getInstance()
    {
        return MockRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new MockRestUserClient(resourceBaseUrl);
    }
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return MockRestUserClientMaker.getInstance();
    }


    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestUserClient(resourceBaseUrl);
    }

    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return restUserClientMaker.makeServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockRestUserClientFactory [restUserClientMaker="
                + restUserClientMaker + "]";
    }



}
