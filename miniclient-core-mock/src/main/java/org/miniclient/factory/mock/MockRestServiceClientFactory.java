package org.miniclient.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ResourceClient;
import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.factory.ClientFactory;
import org.miniclient.factory.RestServiceClientFactory;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.maker.RestServiceClientMaker;
import org.miniclient.maker.mock.MockRestServiceClientMaker;
import org.miniclient.mock.MockRestServiceClient;


public class MockRestServiceClientFactory implements RestServiceClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockRestServiceClientFactory.class.getName());

    // Mock factory.
    private RestServiceClientMaker restServiceClientMaker;

    protected MockRestServiceClientFactory()
    {
        restServiceClientMaker = makeRestServiceClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockRestServiceClientFactoryHolder
    {
        private static final MockRestServiceClientFactory INSTANCE = new MockRestServiceClientFactory() {};
    }

    // Singleton method
    public static MockRestServiceClientFactory getInstance()
    {
        return MockRestServiceClientFactoryHolder.INSTANCE;
    }

    
    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeRestServiceClient(resourceBaseUrl);
    }

    @Override
    public RestUserClientFactory createRestUserClientFactory()
    {
        return makeRestUserClientFactory();
    }


    // Factory methods

    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new MockRestServiceClient(resourceBaseUrl);
    }
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return MockRestServiceClientMaker.getInstance();
    }
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return MockRestUserClientFactory.getInstance();
    }


    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return restServiceClientMaker.makeResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return restServiceClientMaker.makeHttpMethodFilter();
    }


    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return restServiceClientMaker.makeDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return restServiceClientMaker.makeAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return restServiceClientMaker.makeRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return restServiceClientMaker.makeClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return restServiceClientMaker.makeCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return restServiceClientMaker.makeAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockRestServiceClientFactory [restServiceClientMaker="
                + restServiceClientMaker + "]";
    }

    
}
