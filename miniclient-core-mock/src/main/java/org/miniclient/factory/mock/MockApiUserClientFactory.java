package org.miniclient.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ApiUserClient;
import org.miniclient.ResourceClient;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.ClientFactory;
import org.miniclient.maker.ApiUserClientMaker;
import org.miniclient.maker.mock.MockApiUserClientMaker;
import org.miniclient.mock.MockApiUserClient;


public class MockApiUserClientFactory implements ApiUserClientFactory, ClientFactory
{
    private static final Logger log = Logger.getLogger(MockApiUserClientFactory.class.getName());


    // Mock factory.
    private ApiUserClientMaker apiUserClientMaker;

    protected MockApiUserClientFactory()
    {
        apiUserClientMaker = makeApiUserClientMaker();
    }

    // Initialization-on-demand holder.
    private static final class MockApiUserClientFactoryHolder
    {
        private static final MockApiUserClientFactory INSTANCE = new MockApiUserClientFactory() {};
    }

    // Singleton method
    public static MockApiUserClientFactory getInstance()
    {
        return MockApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new MockApiUserClient(resourceBaseUrl) {};
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockApiUserClientMaker.getInstance();
    }



    @Override
    public ResourceClient createClient(String resourceBaseUrl)
    {
        return makeApiUserClient(resourceBaseUrl);
    }

    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return apiUserClientMaker.makeServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockApiUserClientFactory [apiUserClientMaker="
                + apiUserClientMaker + "]";
    }


}
