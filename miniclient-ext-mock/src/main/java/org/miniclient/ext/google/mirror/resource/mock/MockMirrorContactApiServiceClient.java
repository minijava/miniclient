package org.miniclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import org.miniclient.ext.google.mirror.mock.MockMirrorApiServiceClient;
import org.miniclient.ext.google.mirror.proxy.DecoratedMirrorApiServiceClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorContactApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorContactApiServiceClient extends MockMirrorApiServiceClient implements DecoratedMirrorApiServiceClient
{
    private static final Logger log = Logger.getLogger(MockMirrorContactApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    public MockMirrorContactApiServiceClient()
    {
        super(BaseMirrorContactApiServiceClient.CONTACT_RESOURCE_BASE_URL);
    }


    
    @Override
    public String toString()
    {
        return "MockMirrorContactApiServiceClient [getRestServiceClient()="
                + getRestServiceClient() + ", getCrudMethodFilter()="
                + getCrudMethodFilter() + ", getListResponseType()="
                + getListResponseType() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getClientCredential()="
                + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }
    
    
}
