package org.miniclient.ext.google.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ext.google.GoogleApiServiceClient;
import org.miniclient.ext.google.factory.GoogleApiServiceClientFactory;
import org.miniclient.ext.google.factory.GoogleApiUserClientFactory;
import org.miniclient.ext.google.maker.mock.MockGoogleApiServiceClientMaker;
import org.miniclient.ext.google.mock.MockGoogleApiServiceClient;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.factory.mock.MockApiServiceClientFactory;
import org.miniclient.maker.ApiServiceClientMaker;


public class MockGoogleApiServiceClientFactory extends MockApiServiceClientFactory implements GoogleApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleMockApiServiceClientFactoryHolder
    {
        private static final MockGoogleApiServiceClientFactory INSTANCE = new MockGoogleApiServiceClientFactory();
    }

    // Singleton method
    public static MockGoogleApiServiceClientFactory getInstance()
    {
        return GoogleMockApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleApiServiceClient(resourceBaseUrl);
    }
    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MockGoogleApiServiceClientMaker.getInstance();
    }
    @Override
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return makeGoogleApiUserClientFactory();
    }
    protected GoogleApiUserClientFactory makeGoogleApiUserClientFactory()
    {
        return MockGoogleApiUserClientFactory.getInstance();
    }


    @Override
    public GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleApiServiceClient(resourceBaseUrl);
    }


//    @Override
//    public GoogleApiUserClientFactory createGoogleApiUserClientFactory()
//    {
//        return makeGoogleApiUserClientFactory();
//    }


    @Override
    public String toString()
    {
        return "MockGoogleApiServiceClientFactory []";
    }


}
