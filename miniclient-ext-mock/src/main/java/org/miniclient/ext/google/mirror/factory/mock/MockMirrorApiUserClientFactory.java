package org.miniclient.ext.google.mirror.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ApiUserClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.factory.mock.MockGoogleApiUserClientFactory;
import org.miniclient.ext.google.mirror.MirrorApiServiceClient;
import org.miniclient.ext.google.mirror.MirrorApiUserClient;
import org.miniclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import org.miniclient.ext.google.mirror.maker.mock.MockMirrorApiUserClientMaker;
import org.miniclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorContactApiUserClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorLocationApiUserClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorSubscriptionApiUserClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorTimelineApiUserClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorTimelineAttachmentApiUserClient;
import org.miniclient.maker.ApiUserClientMaker;


public class MockMirrorApiUserClientFactory extends MockGoogleApiUserClientFactory implements MirrorApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(MockMirrorApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class MirrorMockApiUserClientFactoryHolder
    {
        private static final MockMirrorApiUserClientFactory INSTANCE = new MockMirrorApiUserClientFactory();
    }

    // Singleton method
    public static MockMirrorApiUserClientFactory getInstance()
    {
        return MirrorMockApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new MockMirrorApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockMirrorApiUserClientMaker.getInstance();
    }

    
    // Resource create methods

    @Override
    public MirrorApiUserClient createMirrorContactApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorContactApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorLocationApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorLocationApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorSubscriptionApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorSubscriptionApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineApiUserClient(UserCredential userCredential)
    {
        return new MockMirrorTimelineApiUserClient(userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(String timelineItemId, UserCredential userCredential)
    {
        return new MockMirrorTimelineAttachmentApiUserClient(timelineItemId, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorContactApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorContactApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorLocationApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorLocationApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorSubscriptionApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorSubscriptionApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorTimelineApiUserClient(serviceClient, userCredential);
    }

    @Override
    public MirrorApiUserClient createMirrorTimelineAttachmentApiUserClient(
            MirrorApiServiceClient serviceClient, UserCredential userCredential)
    {
        return new MockMirrorTimelineAttachmentApiUserClient(serviceClient, userCredential);
    }



    @Override
    public String toString()
    {
        return "MockMirrorApiUserClientFactory []";
    }

    
}
