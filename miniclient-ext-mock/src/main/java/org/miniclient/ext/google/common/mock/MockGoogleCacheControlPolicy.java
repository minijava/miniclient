package org.miniclient.ext.google.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractCacheControlPolicy;


public class MockGoogleCacheControlPolicy extends AbstractCacheControlPolicy
{
    private static final long serialVersionUID = 1L;
    private static final Logger log = Logger.getLogger(MockGoogleCacheControlPolicy.class.getName());

    
    public MockGoogleCacheControlPolicy()
    {
        // ...
    }


    @Override
    public String toString()
    {
        return "MockGoogleCacheControlPolicy [getClass()=" + getClass()
                + ", hashCode()=" + hashCode() + "]";
    }



 

}
