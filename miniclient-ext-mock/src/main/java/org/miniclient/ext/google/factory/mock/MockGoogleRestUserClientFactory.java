package org.miniclient.ext.google.factory.mock;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.RestUserClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleRestUserClient;
import org.miniclient.ext.google.factory.GoogleRestUserClientFactory;
import org.miniclient.ext.google.maker.GoogleRestUserClientMaker;
import org.miniclient.ext.google.mock.MockGoogleRestUserClient;
import org.miniclient.factory.impl.AbstractRestUserClientFactory;
import org.miniclient.maker.RestUserClientMaker;


public class MockGoogleRestUserClientFactory extends AbstractRestUserClientFactory implements GoogleRestUserClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleRestUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestUserClientFactoryHolder
    {
        private static final MockGoogleRestUserClientFactory INSTANCE = new MockGoogleRestUserClientFactory();
    }

    // Singleton method
    public static MockGoogleRestUserClientFactory getInstance()
    {
        return GoogleRestUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestUserClient makeRestUserClient(String resourceBaseUrl)
    {
        return new MockGoogleRestUserClient(resourceBaseUrl);
    }
    @Override
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return GoogleRestUserClientMaker.getInstance();
    }


    @Override
    public GoogleRestUserClient createGoogleRestUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new MockGoogleRestUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleRestUserClient createGoogleRestUserClient(RestServiceClient restServiceClient, UserCredential userCredential)
    {
        return new MockGoogleRestUserClient(restServiceClient, userCredential);
    }



    @Override
    public String toString()
    {
        return "MockGoogleRestUserClientFactory []";
    }

    
}
