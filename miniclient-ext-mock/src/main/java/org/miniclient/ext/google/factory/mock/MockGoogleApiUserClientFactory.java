package org.miniclient.ext.google.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ApiUserClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiUserClient;
import org.miniclient.ext.google.factory.GoogleApiUserClientFactory;
import org.miniclient.ext.google.maker.mock.MockGoogleApiUserClientMaker;
import org.miniclient.ext.google.mock.MockGoogleApiUserClient;
import org.miniclient.factory.mock.MockApiUserClientFactory;
import org.miniclient.maker.ApiUserClientMaker;


public class MockGoogleApiUserClientFactory extends MockApiUserClientFactory implements GoogleApiUserClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleApiUserClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleMockApiUserClientFactoryHolder
    {
        private static final MockGoogleApiUserClientFactory INSTANCE = new MockGoogleApiUserClientFactory();
    }

    // Singleton method
    public static MockGoogleApiUserClientFactory getInstance()
    {
        return GoogleMockApiUserClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    protected ApiUserClient makeApiUserClient(String resourceBaseUrl)
    {
        return new MockGoogleApiUserClient(resourceBaseUrl);
    }
    protected ApiUserClientMaker makeApiUserClientMaker()
    {
        return MockGoogleApiUserClientMaker.getInstance();
    }


    @Override
    public GoogleApiUserClient createGoogleApiUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        return new MockGoogleApiUserClient(resourceBaseUrl, userCredential);
    }

    @Override
    public GoogleApiUserClient createGoogleApiUserClient(ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        return new MockGoogleApiUserClient(apiServiceClient, userCredential);
    }


    @Override
    public String toString()
    {
        return "MockGoogleApiUserClientFactory []";
    }

    
}
