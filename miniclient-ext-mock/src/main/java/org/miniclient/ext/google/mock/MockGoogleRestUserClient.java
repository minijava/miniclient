package org.miniclient.ext.google.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.ApiUserClient;
import org.miniclient.RestServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.maker.mock.MockGoogleRestUserClientMaker;
import org.miniclient.ext.google.proxy.DecoratedGoogleRestUserClient;
import org.miniclient.maker.RestUserClientMaker;
import org.miniclient.mock.MockRestUserClient;


// "Mock" object.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockGoogleRestUserClient extends MockRestUserClient implements DecoratedGoogleRestUserClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockGoogleRestUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final MockRestUserClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockGoogleRestUserClient(String resourceBaseUrl)
    {
        this(resourceBaseUrl, null);
    }
    public MockGoogleRestUserClient(String resourceBaseUrl, UserCredential userCredential)
    {
        super(resourceBaseUrl, userCredential);
        this.decoratedClient = null;
    }

    public MockGoogleRestUserClient(RestServiceClient restServiceClient)
    {
        this(restServiceClient, null);
    }
    public MockGoogleRestUserClient(RestServiceClient restServiceClient,
            UserCredential userCredential)
    {
        super(restServiceClient, userCredential);
        this.decoratedClient = null;
    }

    public MockGoogleRestUserClient(MockRestUserClient decoratedClient)
    {
        super((String) null);
        this.decoratedClient = decoratedClient;
    }


    // Factory methods

    @Override
    protected RestUserClientMaker makeRestUserClientMaker()
    {
        return MockGoogleRestUserClientMaker.getInstance();
    }

    
    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Map<String, Object> get(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestUserClient.get(): id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.get(id, params);
        } else {
            return super.get(id, params);
        }
    }

    @Override
    public Map<String, Object> post(Object inputData)
            throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestUserClient.post(): inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.post(inputData);
        } else {
            return super.post(inputData);
        }
    }

    @Override
    public Map<String, Object> put(Object inputData,
            String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestUserClient.put(): inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.put(inputData, id);
        } else {
            return super.put(inputData, id);
        }
    }

    @Override
    public Map<String, Object> patch(Object partialData, String id) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestUserClient.patch(): partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.patch(partialData, id);
        } else {
            return super.patch(partialData, id);
        }
    }

    @Override
    public Map<String, Object> delete(String id,
            Map<String, Object> params) throws IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleRestUserClient.delete(): id = " + id + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(id, params);
        } else {
            return super.delete(id, params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockGoogleRestUserClient [decoratedClient=" + decoratedClient
                + ", getRestServiceClient()=" + getRestServiceClient()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getResourcePostUrl()=" + getResourcePostUrl()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy()
                + ", getUserCredential()=" + getUserCredential()
                + ", isAccessAllowed()=" + isAccessAllowed() + "]";
    }

    
}
