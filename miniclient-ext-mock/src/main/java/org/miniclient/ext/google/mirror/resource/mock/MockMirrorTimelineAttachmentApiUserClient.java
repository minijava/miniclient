package org.miniclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import org.miniclient.ext.google.mirror.proxy.DecoratedMirrorApiUserClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorTimelineAttachmentApiUserClient extends MockMirrorApiUserClient implements DecoratedMirrorApiUserClient
{
    private static final Logger log = Logger.getLogger(MockMirrorTimelineAttachmentApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;


    public MockMirrorTimelineAttachmentApiUserClient(String timelineItemId)
    {
        this(timelineItemId, null);
    }
    public MockMirrorTimelineAttachmentApiUserClient(String timelineItemId, UserCredential userCredential)
    {
        super(timelineItemId, userCredential);
    }

    public MockMirrorTimelineAttachmentApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public MockMirrorTimelineAttachmentApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "MockMirrorTimelineAttachmentApiUserClient [getApiServiceClient()="
                + getApiServiceClient()
                + ", getUserCredential()="
                + getUserCredential()
                + ", isAccessAllowed()="
                + isAccessAllowed()
                + ", getResourceBaseUrl()="
                + getResourceBaseUrl()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy()
                + ", getClientCachePolicy()="
                + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
