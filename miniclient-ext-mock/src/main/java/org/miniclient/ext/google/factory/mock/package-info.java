// Copyright (c) 2013 Harry Y.
// Permission is hereby granted to any person obtaining a copy of this software 
// to deal in the software without restriction subject to the following conditions: 
// "The software shall be used for good, not evil."
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED. 

/**
 * MiniClient ext/google module factory mock classes.
 * These are primarily intended for testing.
 */
package org.miniclient.ext.google.factory.mock;
