package org.miniclient.ext.google.maker.mock;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.CrudMethodFilter;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.mock.MockCrudMethodFilter;
import org.miniclient.common.mock.MockDataAccessClient;
import org.miniclient.ext.google.common.mock.MockGoogleAuthRefreshPolicy;
import org.miniclient.ext.google.common.mock.MockGoogleAutoRedirectPolicy;
import org.miniclient.ext.google.common.mock.MockGoogleCacheControlPolicy;
import org.miniclient.ext.google.common.mock.MockGoogleClientCachePolicy;
import org.miniclient.ext.google.common.mock.MockGoogleRequestRetryPolicy;
import org.miniclient.ext.google.mock.MockGoogleRestServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;


// Mock factory.
public class MockGoogleApiServiceClientMaker implements ApiServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleApiServiceClientMaker.class.getName());


    protected MockGoogleApiServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static final class MockGoogleMockApiServiceClientMakerHolder
    {
        private static final MockGoogleApiServiceClientMaker INSTANCE = new MockGoogleApiServiceClientMaker();
    }

    // Singleton method
    public static MockGoogleApiServiceClientMaker getInstance()
    {
        return MockGoogleMockApiServiceClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeRestClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }

    @Override
    public CrudMethodFilter makeCrudMethodFilter()
    {
        return new MockCrudMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }
    
    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockGoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockGoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockGoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockGoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockGoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockGoogleApiServiceClientMaker []";
    }

    
}
