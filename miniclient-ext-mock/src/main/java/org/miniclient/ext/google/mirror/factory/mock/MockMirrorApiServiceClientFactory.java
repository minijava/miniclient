package org.miniclient.ext.google.mirror.factory.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ext.google.factory.GoogleApiUserClientFactory;
import org.miniclient.ext.google.factory.mock.MockGoogleApiServiceClientFactory;
import org.miniclient.ext.google.mirror.MirrorApiServiceClient;
import org.miniclient.ext.google.mirror.factory.MirrorApiServiceClientFactory;
import org.miniclient.ext.google.mirror.factory.MirrorApiUserClientFactory;
import org.miniclient.ext.google.mirror.maker.mock.MockMirrorApiServiceClientMaker;
import org.miniclient.ext.google.mirror.mock.MockMirrorApiServiceClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorContactApiServiceClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorLocationApiServiceClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorSubscriptionApiServiceClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorTimelineApiServiceClient;
import org.miniclient.ext.google.mirror.resource.mock.MockMirrorTimelineAttachmentApiServiceClient;
import org.miniclient.factory.ApiUserClientFactory;
import org.miniclient.maker.ApiServiceClientMaker;


public class MockMirrorApiServiceClientFactory extends MockGoogleApiServiceClientFactory implements MirrorApiServiceClientFactory
{
    private static final Logger log = Logger.getLogger(MockMirrorApiServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class MirrorMockApiServiceClientFactoryHolder
    {
        private static final MockMirrorApiServiceClientFactory INSTANCE = new MockMirrorApiServiceClientFactory();
    }

    // Singleton method
    public static MockMirrorApiServiceClientFactory getInstance()
    {
        return MirrorMockApiServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected ApiServiceClient makeApiServiceClient(String resourceBaseUrl)
    {
        return new MockMirrorApiServiceClient(resourceBaseUrl);
    }
    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MockMirrorApiServiceClientMaker.getInstance();
    }
    @Override
    protected ApiUserClientFactory makeApiUserClientFactory()
    {
        return makeGoogleApiUserClientFactory();
    }
    @Override
    protected GoogleApiUserClientFactory makeGoogleApiUserClientFactory()
    {
        return makeMirrorApiUserClientFactory();
    }
    protected MirrorApiUserClientFactory makeMirrorApiUserClientFactory()
    {
        return MockMirrorApiUserClientFactory.getInstance();
    }

    
    // Resource create methods

    @Override
    public MirrorApiServiceClient createMirrorContactApiServiceClient()
    {
        return new MockMirrorContactApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorLocationApiServiceClient()
    {
        return new MockMirrorLocationApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorSubscriptionApiServiceClient()
    {
        return new MockMirrorSubscriptionApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorTimelineApiServiceClient()
    {
        return new MockMirrorTimelineApiServiceClient();
    }

    @Override
    public MirrorApiServiceClient createMirrorTimelineAttachmentApiServiceClient(String timelineItemId)
    {
        return new MockMirrorTimelineAttachmentApiServiceClient(timelineItemId);
    }


//    @Override
//    public MirrorApiUserClientFactory createMirrorApiUserClientFactory()
//    {
//        return makeMirrorApiUserClientFactory();
//    }


    
    @Override
    public String toString()
    {
        return "MockMirrorApiServiceClientFactory []";
    }


}
