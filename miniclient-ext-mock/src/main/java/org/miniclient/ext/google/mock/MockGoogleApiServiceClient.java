package org.miniclient.ext.google.mock;

import java.io.IOException;
import java.io.Serializable;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.miniclient.RestApiException;
import org.miniclient.core.ListResponseType;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.maker.mock.MockGoogleApiServiceClientMaker;
import org.miniclient.ext.google.proxy.DecoratedGoogleApiServiceClient;
import org.miniclient.maker.ApiServiceClientMaker;
import org.miniclient.mock.MockApiServiceClient;


// Base class for all GoogleMock API resources.
// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockGoogleApiServiceClient extends MockApiServiceClient implements DecoratedGoogleApiServiceClient, Serializable
{
    private static final Logger log = Logger.getLogger(MockGoogleApiServiceClient.class.getName());
    private static final long serialVersionUID = 1L;

    private final MockApiServiceClient decoratedClient;


    // Based on the use of a particular ctor,
    // we use either inheritance or decoration. 

    public MockGoogleApiServiceClient(MockApiServiceClient decoratedClient)
    {
        this(decoratedClient, (decoratedClient != null) ? decoratedClient.getResourceBaseUrl() : null);
    }
    public MockGoogleApiServiceClient(String resourceBaseUrl)
    {
        this(null, resourceBaseUrl);
    }
    private MockGoogleApiServiceClient(MockApiServiceClient decoratedClient, String resourceBaseUrl)
    {
        super(resourceBaseUrl);
        this.decoratedClient = decoratedClient;
    }


    @Override
    protected void init()
    {
        super.init();
        
        // TBD: Need to check this...
        setListResponseType(ListResponseType.MAP_ITEMS);
        // ...

    }


    // Factory methods

    @Override
    protected ApiServiceClientMaker makeApiServiceClientMaker()
    {
        return MockGoogleApiServiceClientMaker.getInstance();
    }


    // Override methods.
    //   Note the unusual "dual" delegation.

    @Override
    public Object get(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.get(): credential = " + credential + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.get(credential, id);
        } else {
            return super.get(credential, id);
        }
    }

    @Override
    public List<Object> list(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.list(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.list(credential, params);
        } else {
            return super.list(credential, params);
        }
    }

    @Override
    public List<String> keys(UserCredential credential,
            Map<String, Object> params) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.keys(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.keys(credential, params);
        } else {
            return super.keys(credential, params);
        }
    }

    @Override
    public Object create(UserCredential credential, Object inputData)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData);
        if(decoratedClient != null) {
            return decoratedClient.create(credential, inputData);
        } else {
            return super.create(credential, inputData);
        }
    }

    @Override
    public Object create(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.create(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.create(credential, inputData, id);
        } else {
            return super.create(credential, inputData, id);
        }
    }

    @Override
    public Object update(UserCredential credential, Object inputData, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.update(): credential = " + credential + "; inputData = " + inputData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.update(credential, inputData, id);
        } else {
            return super.update(credential, inputData, id);
        }
    }

    @Override
    public Object modify(UserCredential credential, Object partialData,
            String id) throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.modify(): credential = " + credential + "; partialData = " + partialData + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.modify(credential, partialData, id);
        } else {
            return super.modify(credential, partialData, id);
        }
    }

    @Override
    public boolean delete(UserCredential credential, String id)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.delete(): credential = " + credential + "; id = " + id);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, id);
        } else {
            return super.delete(credential, id);
        }
    }

    @Override
    public int delete(UserCredential credential, Map<String, Object> params)
            throws RestApiException, IOException
    {
        if(log.isLoggable(Level.FINE)) log.fine("MockGoogleApiServiceClient.delete(): credential = " + credential + "; params = " + params);
        if(decoratedClient != null) {
            return decoratedClient.delete(credential, params);
        } else {
            return super.delete(credential, params);
        }
    }

    
    
    @Override
    public String toString()
    {
        return "MockGoogleApiServiceClient [decoratedClient=" + decoratedClient
                + ", toString()=" + super.toString()
                + ", getRestServiceClient()=" + getRestServiceClient()
                + ", getCrudMethodFilter()=" + getCrudMethodFilter()
                + ", getListResponseType()=" + getListResponseType()
                + ", getResourceBaseUrl()=" + getResourceBaseUrl()
                + ", getClientCredential()=" + getClientCredential()
                + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy() + ", getRequiredScopes()="
                + getRequiredScopes() + ", getAutoRedirectPolicy()="
                + getAutoRedirectPolicy() + ", getAuthRefreshPolicy()="
                + getAuthRefreshPolicy() + ", getRequestRetryPolicy()="
                + getRequestRetryPolicy() + ", getClientCachePolicy()="
                + getClientCachePolicy() + "]";
    }


}
