package org.miniclient.ext.google.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractClientCachePolicy;


public class MockGoogleClientCachePolicy extends AbstractClientCachePolicy
{
    private static final Logger log = Logger.getLogger(MockGoogleClientCachePolicy.class.getName());


    
    @Override
    public String toString()
    {
        return "MockGoogleClientCachePolicy [isCacheEnabled()=" + isCacheEnabled()
                + ", getCacheLifetime()=" + getCacheLifetime() + "]";
    }


    
}
