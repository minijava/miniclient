package org.miniclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import org.miniclient.ext.google.mirror.proxy.DecoratedMirrorApiUserClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorLocationApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorLocationApiUserClient extends MockMirrorApiUserClient implements DecoratedMirrorApiUserClient
{
    private static final Logger log = Logger.getLogger(MockMirrorLocationApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    
    public MockMirrorLocationApiUserClient()
    {
        this((UserCredential) null);
    }
    public MockMirrorLocationApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorLocationApiServiceClient.LOCATION_RESOURCE_BASE_URL, userCredential);
    }

    public MockMirrorLocationApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public MockMirrorLocationApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "MockMirrorLocationApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }

    
    
}
