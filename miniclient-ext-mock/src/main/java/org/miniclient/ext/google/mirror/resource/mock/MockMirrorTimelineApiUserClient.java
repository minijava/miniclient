package org.miniclient.ext.google.mirror.resource.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.mirror.mock.MockMirrorApiUserClient;
import org.miniclient.ext.google.mirror.proxy.DecoratedMirrorApiUserClient;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorTimelineApiServiceClient;


// We have a very strange "dual" implementation.
// We use either inheritance or decoration, based on how the object is constructed.
public class MockMirrorTimelineApiUserClient extends MockMirrorApiUserClient implements DecoratedMirrorApiUserClient
{
    private static final Logger log = Logger.getLogger(MockMirrorTimelineApiUserClient.class.getName());
    private static final long serialVersionUID = 1L;

    
    public MockMirrorTimelineApiUserClient()
    {
        this((UserCredential) null);
    }
    public MockMirrorTimelineApiUserClient(UserCredential userCredential)
    {
        super(BaseMirrorTimelineApiServiceClient.TIMELINE_RESOURCE_BASE_URL, userCredential);
    }

    public MockMirrorTimelineApiUserClient(ApiServiceClient apiServiceClient)
    {
        this(apiServiceClient, null);
    }
    public MockMirrorTimelineApiUserClient(ApiServiceClient apiServiceClient,
            UserCredential userCredential)
    {
        super(apiServiceClient, userCredential);
    }

    
    
    @Override
    public String toString()
    {
        return "MockMirrorTimelineApiUserClient [getApiServiceClient()="
                + getApiServiceClient() + ", getUserCredential()="
                + getUserCredential() + ", isAccessAllowed()="
                + isAccessAllowed() + ", getResourceBaseUrl()="
                + getResourceBaseUrl() + ", getRestServiceAuthRefreshPolicy()="
                + getRestServiceAuthRefreshPolicy()
                + ", getRestServiceRequestRetryPolicy()="
                + getRestServiceRequestRetryPolicy()
                + ", getRestServiceClientCachePolicy()="
                + getRestServiceClientCachePolicy()
                + ", getRestServiceAutoRedirectPolicy()="
                + getRestServiceAutoRedirectPolicy()
                + ", getAuthRefreshPolicy()=" + getAuthRefreshPolicy()
                + ", getRequestRetryPolicy()=" + getRequestRetryPolicy()
                + ", getClientCachePolicy()=" + getClientCachePolicy()
                + ", getAutoRedirectPolicy()=" + getAutoRedirectPolicy() + "]";
    }


}
