package org.miniclient.ext.google.maker.mock;

import java.util.logging.Logger;

import org.miniclient.common.AuthRefreshPolicy;
import org.miniclient.common.AutoRedirectPolicy;
import org.miniclient.common.CacheControlPolicy;
import org.miniclient.common.ClientCachePolicy;
import org.miniclient.common.DataAccessClient;
import org.miniclient.common.HttpMethodFilter;
import org.miniclient.common.RequestRetryPolicy;
import org.miniclient.common.ResourceUrlBuilder;
import org.miniclient.common.mock.MockDataAccessClient;
import org.miniclient.common.mock.MockHttpMethodFilter;
import org.miniclient.ext.google.common.GoogleResourceUrlBuilder;
import org.miniclient.ext.google.common.mock.MockGoogleAuthRefreshPolicy;
import org.miniclient.ext.google.common.mock.MockGoogleAutoRedirectPolicy;
import org.miniclient.ext.google.common.mock.MockGoogleCacheControlPolicy;
import org.miniclient.ext.google.common.mock.MockGoogleClientCachePolicy;
import org.miniclient.ext.google.common.mock.MockGoogleRequestRetryPolicy;
import org.miniclient.maker.RestServiceClientMaker;


// GoogleMock factory.
public class MockGoogleRestServiceClientMaker implements RestServiceClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleRestServiceClientMaker.class.getName());


    protected MockGoogleRestServiceClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleMockRestServiceClientMakerHolder
    {
        private static final MockGoogleRestServiceClientMaker INSTANCE = new MockGoogleRestServiceClientMaker();
    }

    // Singleton method
    public static MockGoogleRestServiceClientMaker getInstance()
    {
        return GoogleMockRestServiceClientMakerHolder.INSTANCE;
    }

    @Override
    public ResourceUrlBuilder makeResourceUrlBuilder(String resourceBaseUrl)
    {
        return new GoogleResourceUrlBuilder(resourceBaseUrl);
    }

    @Override
    public HttpMethodFilter makeHttpMethodFilter()
    {
        return new MockHttpMethodFilter();
    }

    @Override
    public DataAccessClient makeDataAccessClient()
    {
        return new MockDataAccessClient();
    }

    @Override
    public AuthRefreshPolicy makeAuthRefreshPolicy()
    {
        return new MockGoogleAuthRefreshPolicy();
    }

    @Override
    public RequestRetryPolicy makeRequestRetryPolicy()
    {
        return new MockGoogleRequestRetryPolicy();
    }

    @Override
    public ClientCachePolicy makeClientCachePolicy()
    {
        return new MockGoogleClientCachePolicy();
    }

    @Override
    public CacheControlPolicy makeCacheControlPolicy()
    {
        return new MockGoogleCacheControlPolicy();
    }

    @Override
    public AutoRedirectPolicy makeAutoRedirectPolicy()
    {
        return new MockGoogleAutoRedirectPolicy();
    }


    @Override
    public String toString()
    {
        return "MockGoogleRestServiceClientMaker []";
    }


}
