package org.miniclient.ext.google.mirror.maker.mock;

import java.util.logging.Logger;

import org.miniclient.ApiServiceClient;
import org.miniclient.ext.google.mirror.mock.MockMirrorApiServiceClient;
import org.miniclient.maker.ApiUserClientMaker;


// Abstract factory.
public class MockMirrorApiUserClientMaker implements ApiUserClientMaker
{
    private static final Logger log = Logger.getLogger(MockMirrorApiUserClientMaker.class.getName());


    protected MockMirrorApiUserClientMaker()
    {
    }


    // Initialization-on-demand holder.
    private static final class MirrorMockApiUserClientMakerHolder
    {
        private static final MockMirrorApiUserClientMaker INSTANCE = new MockMirrorApiUserClientMaker();
    }

    // Singleton method
    public static MockMirrorApiUserClientMaker getInstance()
    {
        return MirrorMockApiUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public ApiServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new MockMirrorApiServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockMirrorApiUserClientMaker []";
    }

    
}
