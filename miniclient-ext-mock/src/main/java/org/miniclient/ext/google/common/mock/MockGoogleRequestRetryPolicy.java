package org.miniclient.ext.google.common.mock;

import java.util.logging.Logger;

import org.miniclient.common.impl.AbstractRequestRetryPolicy;


public class MockGoogleRequestRetryPolicy extends AbstractRequestRetryPolicy
{
    private static final Logger log = Logger.getLogger(MockGoogleRequestRetryPolicy.class.getName());

    public MockGoogleRequestRetryPolicy()
    {
        // TODO Auto-generated constructor stub
    }


    
    
    @Override
    public String toString()
    {
        return "MockGoogleRequestRetryPolicy [isRetryIfFails()=" + isRetryIfFails()
                + ", getMaxRetryCount()=" + getMaxRetryCount() + "]";
    }
    


}
