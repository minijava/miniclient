package org.miniclient.ext.google.factory.mock;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.ext.google.GoogleRestServiceClient;
import org.miniclient.ext.google.factory.GoogleRestServiceClientFactory;
import org.miniclient.ext.google.factory.GoogleRestUserClientFactory;
import org.miniclient.ext.google.maker.GoogleRestServiceClientMaker;
import org.miniclient.ext.google.mock.MockGoogleRestServiceClient;
import org.miniclient.factory.RestUserClientFactory;
import org.miniclient.factory.impl.AbstractRestServiceClientFactory;
import org.miniclient.maker.RestServiceClientMaker;


public class MockGoogleRestServiceClientFactory extends AbstractRestServiceClientFactory implements GoogleRestServiceClientFactory
{
    private static final Logger log = Logger.getLogger(MockGoogleRestServiceClientFactory.class.getName());

    // Initialization-on-demand holder.
    private static final class GoogleRestServiceClientFactoryHolder
    {
        private static final MockGoogleRestServiceClientFactory INSTANCE = new MockGoogleRestServiceClientFactory();
    }

    // Singleton method
    public static MockGoogleRestServiceClientFactory getInstance()
    {
        return GoogleRestServiceClientFactoryHolder.INSTANCE;
    }


    // Factory methods

    @Override
    protected RestServiceClient makeRestServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }
    @Override
    protected RestServiceClientMaker makeRestServiceClientMaker()
    {
        return GoogleRestServiceClientMaker.getInstance();
    }
    @Override
    protected RestUserClientFactory makeRestUserClientFactory()
    {
        return makeGoogleRestUserClientFactory();
    }
    protected GoogleRestUserClientFactory makeGoogleRestUserClientFactory()
    {
        return MockGoogleRestUserClientFactory.getInstance();
    }

 
    @Override
    public GoogleRestServiceClient createGoogleRestServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }


//    @Override
//    public GoogleRestUserClientFactory createGoogleRestUserClientFactory()
//    {
//        return makeGoogleRestUserClientFactory();
//    }


    @Override
    public String toString()
    {
        return "MockGoogleRestServiceClientFactory []";
    }


}
