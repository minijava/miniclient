package org.miniclient.ext.google.maker.mock;

import java.util.logging.Logger;

import org.miniclient.RestServiceClient;
import org.miniclient.ext.google.maker.GoogleRestUserClientMaker;
import org.miniclient.ext.google.mock.MockGoogleRestServiceClient;
import org.miniclient.maker.RestUserClientMaker;


// GoogleMock factory.
public class MockGoogleRestUserClientMaker implements RestUserClientMaker
{
    private static final Logger log = Logger.getLogger(MockGoogleRestUserClientMaker.class.getName());

    
    protected MockGoogleRestUserClientMaker()
    {
    }

    // Initialization-on-demand holder.
    private static class GoogleMockRestUserClientMakerHolder
    {
        private static final MockGoogleRestUserClientMaker INSTANCE = new MockGoogleRestUserClientMaker();
    }

    // Singleton method
    public static MockGoogleRestUserClientMaker getInstance()
    {
        return GoogleMockRestUserClientMakerHolder.INSTANCE;
    }

    
    @Override
    public RestServiceClient makeServiceClient(String resourceBaseUrl)
    {
        return new MockGoogleRestServiceClient(resourceBaseUrl);
    }



    @Override
    public String toString()
    {
        return "MockGoogleRestUserClientMaker []";
    }

}
