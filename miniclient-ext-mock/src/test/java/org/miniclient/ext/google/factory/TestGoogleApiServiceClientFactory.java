package org.miniclient.ext.google.factory;

import java.io.IOException;

import org.miniclient.ResourceClient;
import org.miniclient.RestApiException;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiServiceClient;
import org.miniclient.ext.google.factory.mock.MockGoogleApiServiceClientFactory;
import org.miniclient.ext.google.mock.MockGoogleApiServiceClient;
import org.miniclient.mock.MockApiServiceClient;



public class TestGoogleApiServiceClientFactory extends
        MockGoogleApiServiceClientFactory implements
        GoogleApiServiceClientFactory
{

    public TestGoogleApiServiceClientFactory()
    {
    }

    @Override
    public GoogleApiServiceClient createGoogleApiServiceClient(String resourceBaseUrl)
    {

        GoogleApiServiceClient googleApiServiceClient = super.createGoogleApiServiceClient(resourceBaseUrl);
        
        MockGoogleApiServiceClient mockGoogleApiServiceClient = new MockGoogleApiServiceClient((MockApiServiceClient) googleApiServiceClient) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object get(UserCredential credential, String id)
                    throws RestApiException, IOException
            {
                // TBD: ...
                return super.get(credential, id);
            }
            
        };
        
        return mockGoogleApiServiceClient;
    }
    
    
}
