package org.miniclient.ext.google.mirror.resource;

import static org.junit.Assert.fail;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.logging.Logger;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.miniclient.RestApiException;
import org.miniclient.core.AuthMethod;
import org.miniclient.credential.UserCredential;
import org.miniclient.credential.impl.AbstractUserCredential;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorTimelineApiUserClient;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope: https://www.googleapis.com/auth/glass.timeline
//        https://www.googleapis.com/auth/glass.location
// https://developers.google.com/glass/v1/reference/timeline#resource
public class MirrorTimelineApiUserClientTest
{
    private static final Logger log = Logger.getLogger(MirrorTimelineApiUserClientTest.class.getName());

    
    private BaseMirrorTimelineApiUserClient timelineApiUserClient;
    

    @Before
    public void setUp() throws Exception
    {
        timelineApiUserClient = new BaseMirrorTimelineApiUserClient();
        
        String user = "ab";
        String userId = "123";
        String authToken = "ya29.AHES6ZT2CM8ueqvjE847KxsygvnGOYUWe4y28k47-Hv55JCSkvknIA";
        Set<String> dataScopes = null;
        Long expirationTime = null;
        UserCredential authCredential = new AbstractUserCredential(true, user, userId, AuthMethod.BEARER, authToken, null, dataScopes, expirationTime, null) {};
        timelineApiUserClient.setUserCredential(authCredential);
        
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGet()
    {
        Object timelineItem = null;
        String id = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6NDQ0NTMwMTUyOjA";
        try {
            timelineItem = timelineApiUserClient.get(id);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("timelineItem = " + timelineItem);
    }

    @Test
    public void testList()
    {
        List<Object> timelineItems = null;
        try {
            Map<String,Object> params = null;
            timelineItems = timelineApiUserClient.list(params);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("timelineItems = " + timelineItems);
    }

    @Test
    public void testKeys()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObject()
    {
        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("kind", "mirror#timelineItem");
        inputData.put("title", "test timeline item 1");
        inputData.put("text", "Timeline item with text message");
        // inputData.put("html", "<p>Timeline item with <b>html message</b></p>");
        // etc...
        
        Object timelineItem = null;
        try {
            timelineItem = timelineApiUserClient.create(inputData);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("timelineItem = " + timelineItem);
    }

    @Test
    public void testCreateObjectString()
    {
        // fail("Not yet implemented");
    }

    // @Test
    public void testUpdate()
    {
        String id = "MTIxOTAxOTk1MTYwMDQ0MjA2NTY6NDQ0NTMwMTUyOjA";

        Map<String,Object> inputData = new HashMap<String,Object>();
        inputData.put("kind", "mirror#timelineItem");
        inputData.put("id", id);
        inputData.put("title", "test timeline item 1 modified - " + System.currentTimeMillis());
        inputData.put("text", "Timeline item with text message. Modified");
        // inputData.put("html", "<p>Timeline item with <b>html message</b>. Modified.</p>");
        // etc...
        
        Object timelineItem = null;
        try {
            timelineItem = timelineApiUserClient.update(inputData, id);
        // } catch (RestApiException | IOException e) {
        } catch (Exception e) {
            e.printStackTrace();
        }
        System.out.println("timelineItem = " + timelineItem);
    }

    @Test
    public void testDeleteString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
        // fail("Not yet implemented");
    }

}
