package org.miniclient.ext.google.mirror.resource;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorSubscriptionApiUserClient;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope:
// https://developers.google.com/glass/v1/reference/subscriptions#resource
public class MirrorSubscriptionApiUserClientTest
{
    private static final Logger log = Logger.getLogger(MirrorSubscriptionApiUserClientTest.class.getName());
    
    private BaseMirrorSubscriptionApiUserClient subscriptionApiUserClient;


    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGet()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testList()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testKeys()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObject()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testCreateObjectString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testUpdate()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteString()
    {
        // fail("Not yet implemented");
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
        // fail("Not yet implemented");
    }

}
