package org.miniclient.ext.google.factory;

import java.io.IOException;

import org.miniclient.ApiServiceClient;
import org.miniclient.RestApiException;
import org.miniclient.credential.UserCredential;
import org.miniclient.ext.google.GoogleApiUserClient;
import org.miniclient.ext.google.factory.mock.MockGoogleApiUserClientFactory;
import org.miniclient.ext.google.mock.MockGoogleApiUserClient;
import org.miniclient.mock.MockApiUserClient;


public class TestGoogleApiUserClientFactory extends
        MockGoogleApiUserClientFactory implements GoogleApiUserClientFactory
{

    public TestGoogleApiUserClientFactory()
    {
    }

    
    @Override
    public GoogleApiUserClient createGoogleApiUserClient(
            String resourceBaseUrl, UserCredential userCredential)
    {
        GoogleApiUserClient googleApiUserClient = super.createGoogleApiUserClient(resourceBaseUrl, userCredential);
        
        MockGoogleApiUserClient mockGoogleApiUserClient = new MockGoogleApiUserClient((MockApiUserClient) googleApiUserClient) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object get(String id) throws RestApiException, IOException
            {
                // TBD: ...
                return super.get(id);
            }
            
        };
        
        return mockGoogleApiUserClient;
    }

    @Override
    public GoogleApiUserClient createGoogleApiUserClient(
            ApiServiceClient apiServiceClient, UserCredential userCredential)
    {
        // TODO Auto-generated method stub
        GoogleApiUserClient googleApiUserClient = super.createGoogleApiUserClient(apiServiceClient, userCredential);
        
        MockGoogleApiUserClient mockGoogleApiUserClient = new MockGoogleApiUserClient((MockApiUserClient) googleApiUserClient) {

            private static final long serialVersionUID = 1L;

            @Override
            public Object get(String id) throws RestApiException, IOException
            {
                // TBD: ...
                return super.get(id);
            }
            
        };
        
        return mockGoogleApiUserClient;
    }

    
}
