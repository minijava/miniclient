package org.miniclient.ext.google.mirror.resource;

import static org.junit.Assert.*;

import java.util.logging.Logger;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.miniclient.ext.google.mirror.resource.impl.BaseMirrorLocationApiUserClient;


// Get OAuth2 access token from here: https://developers.google.com/oauthplayground/
// Scope:
// https://developers.google.com/glass/v1/reference/locations#resource
public class MirrorLocationApiUserClientTest
{
    private static final Logger log = Logger.getLogger(MirrorLocationApiUserClientTest.class.getName());
    
    private BaseMirrorLocationApiUserClient locationApiUserClient;


    @Before
    public void setUp() throws Exception
    {
    }

    @After
    public void tearDown() throws Exception
    {
    }

    @Test
    public void testGet()
    {
    }

    @Test
    public void testList()
    {
    }

    @Test
    public void testKeys()
    {
    }

    @Test
    public void testCreateObject()
    {
    }

    @Test
    public void testCreateObjectString()
    {
    }

    @Test
    public void testUpdate()
    {
    }

    @Test
    public void testDeleteString()
    {
    }

    @Test
    public void testDeleteMapOfStringObject()
    {
    }

}
