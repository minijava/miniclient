package org.miniclient.ext;

import org.junit.runner.RunWith;
import org.junit.runners.Suite;
import org.junit.runners.Suite.SuiteClasses;

@RunWith(Suite.class)
@SuiteClasses({ org.miniclient.ext.google.factory.AllTests.class, org.miniclient.ext.google.mirror.factory.manager.AllTests.class, org.miniclient.ext.google.mirror.resource.AllTests.class })
public class AllTests
{

}
